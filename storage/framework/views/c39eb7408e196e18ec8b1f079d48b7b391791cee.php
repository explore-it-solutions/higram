<?php $__env->startSection('title','Manage Staff Permission'); ?>
<?php $__env->startSection('content'); ?>
<style>.ion-ios7-eye{position: absolute;right: 20px; top: 28px;font-size: 25px;}</style>

    <div class="col-sm-12">
        
        <div class="card-box table-responsive" style="padding: 8px 10px;" >
              <div class="box master-add-field-box" >
            <div class="col-md-6 no-pad-left">
                <h3>STAFF PERMISSION</h3>
            </div>         
            </div>
            <div class="filter_text_box_row">
                      
                        <?php echo e(Form::hidden('s_id',$id, array ('id'=>'s_id','name'=>'s_id'))); ?>

                       <?php if(count($rows)>0)
                        { ?>
                        <div class="main_inner_class_track" style="width: 25%;">
                            <div class="group">
                               <div style="position: relative">
                                  <label>User Name</label>
                                   <?php echo e(Form::text('username',$rows[0]->name, array ('id'=>'username','name'=>'username','required','class'=>'form-control'))); ?>

                               </div>
                            </div>
                          </div>
                         <div class="main_inner_class_track" style="width: 25%;">
                          <div class="group">
                             <div style="position: relative">
                                 <label>Password</label>
                                 <input style="padding-right:25px;" class="form-control" id="userpassword" name="userpassword" type="password" value="<?php echo e($password); ?>">
                                 <div class="ion-ios7-eye" onmouseover="mouseoverPass();" onmouseout="mouseoutPass();" />
                             </div>
                              
                           </div>
                        </div>
                      <?php  }
                        else
                        { ?>
                          <div class="main_inner_class_track" style="width: 25%;">
                            <div class="group">
                               <div style="position: relative">
                                  <label>User Name</label>
                                  <input class="form-control" id="username" name="username" type="text">
                               </div>
                            </div>
                          </div>
                        <div class="main_inner_class_track" style="width: 25%;">
                          <div class="group">
                             <div style="position: relative">
                                 <label>Password</label>
                                <?php echo e(Form::password('userpassword',array('id'=>'userpassword','name'=>'userpassword','required','class'=>'form-control'))); ?>

                                <div class="ion-ios7-eye" onmouseover="mouseoverPass();" onmouseout="mouseoutPass();" />
                             </div>
                           </div>
                        </div>
                      <?php  } ?>
                       
            </div>
                           
                         <div class="col-md-1 no-pad-left">
                <div class="table-filter" style="margin-top: 22px;">
                  <div class="table-filter-cc">
                    <a> <button type="submit" style="margin-top: px; border-radius: 4px;margin-left: 0;" class="on-default followups-popup-btn btn btn-primary ad-work-clear-btn" onclick="savepassword();" >UPDATE</button></a>
                </div>
                   
                 </div>
            </div>  
               

                   </div>
            
        </div>
        <?php if(count($rows)>0): ?>
        <div class="card-box table-responsive" style="padding: 8px 10px;margin-top:10px;">
            <?php if(count($rows)==0): ?>
            <div class="overlay_staff_permision">
                Web Login Not Activated 
            </div> 
             <?php endif; ?>
              <div class="col-md-8 no-pad-left">
                <h3>PERMISSION</h3>
                <div class="table_secion_permision">
                  <div class="col-md-12">
                        <table class="table">
                               <tbody>
                                   
                                <?php if(count($pages)!=0): ?>
                                   <?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                   <?php if($p->sub_module!=''): ?>
                                        <tr>
                                          <td><?php echo e($p->sub_module); ?></td>
                                           <td>
                                                <div class="status_chck1<?php echo e($p->module_id); ?>">
                                                     <div class="onoffswitch">
                                                         <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch1<?php echo e($p->module_id); ?>" <?php if($p->active == 'Y'): ?> checked <?php endif; ?>>
                                                         <label class="onoffswitch-label" for="myonoffswitch1<?php echo e($p->module_id); ?>">
                                                             <span class="onoffswitch-inner" onclick="return permissionchange('<?php echo e($p->user_id); ?>','<?php echo e($p->module_id); ?>','<?php echo e($id); ?>')"></span>
                                                             <span class="onoffswitch-switch" onclick="return  permissionchange('<?php echo e($p->user_id); ?>','<?php echo e($p->module_id); ?>','<?php echo e($id); ?>')"></span>
                                                         </label>
                                                     </div>
                                                 </div>
                                              </td>
                                        </tr>
                                        <?php endif; ?>
                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>

                                </tbody>
                          </table>
                    </div>
                    
                </div>
            </div>
        </div>    
        <?php endif; ?>
    </div>


    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->

    <script>
        function savepassword()
        {
            $('.notifyjs-wrapper').remove();
            $('input').removeClass('input_focus');
            $('select').removeClass('input_focus');
            var name = $("#username").val();
            var password = $("#userpassword").val();
            var s_id = $("#s_id").val();
            if(name == '') 
            {
              $("#username").focus();
               $.Notification.autoHideNotify('error', 'bottom right','Enter Username');
            return false;
            }
            
            if(password == '') 
            {
              $("#userpassword").focus();
              $.Notification.autoHideNotify('error', 'bottom right','Enter Password');
            return false;
            }
            
        if(true)
        {
            var data= {"name":name,"password":password,"s_id":s_id};
            $.ajax({
                method: "get",
                url : "../api/savepassword",
                data : data,
                cache : false,
                crossDomain : true,
                async : false,
                dataType :'text',
                success : function(result)
                {
                    var json_x= JSON.parse(result);
                    if((json_x.msg)=='insert')
                    {
                         location.reload();
                         swal({
							
                            title: "",
                            text: "Added Successfully",
                            timer: 4000,
                            showConfirmButton: false
                        });

                    }
                    else if((json_x.msg)=='update')
                    {
                        location.reload();
                        swal({
							
                            title: "",
                            text: "Updated Successfully",
                            timer: 4000,
                            showConfirmButton: false
                        });
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#urls").text(jqxhr.responseText); //@text  = response error, it is will be errors: 324, 500, 404 or anythings else
                }
            });
        }
        }
    
    function permissionchange(userid,m_id,id)
        {
            var data= {"userid":userid,"m_id":m_id,"id":id};
            $.ajax({
                method: "get",
                url : "../api/savepermission",
                data : data,
                cache : false,
                crossDomain : true,
                async : false,
                dataType :'text',
                success : function(result)
                {
//                    alert (result);
//                   location.reload();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#urls").text(jqxhr.responseText); //@text  = response error, it is will be errors: 324, 500, 404 or anythings else
                }
            });
        }
    </script>
    <script>
  function mouseoverPass(obj) {
  var obj = document.getElementById('userpassword');
  obj.type = "text";
}
function mouseoutPass(obj) {
  var obj = document.getElementById('userpassword');
  obj.type = "password";
}
    </script>
<?php $__env->startSection('jquery'); ?> 
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>