
<?php $__env->startSection('content'); ?>

 <style>
        .filter_text_box_row{margin-bottom: 6px}
     .rating{pointer-events: none}
     #example1 tbody{min-height: 400px}
    </style>
<div class="col-sm-12">
        <div class="col-sm-12">
                <ol class="breadcrumb">
						<li>
							<a href="<?php echo e(url('index')); ?>">Dashboard</a>
						</li>
						
						<li class="active ms-hover">
							<a <?php if($user_type!='V'): ?> href="<?php echo e(url('manage_restaurant')); ?>" <?php else: ?> href="" <?php endif; ?>><?php echo e($restaurant_name[0]->name); ?></a>
						</li>
                    <li class="active ms-hover">
							Customer Review
						</li>
					</ol>
				</div>
        
        <div class="col-sm-12">
            <a href="<?php echo e(url('restaurant_edit/'.$resid)); ?>"><div class="potafo_top_menu_sec">About</div></a>
            <a href="<?php echo e(url('menu/list/'.$resid)); ?>"><div class="potafo_top_menu_sec">Menu</div></a>
            <a href="<?php echo e(url('category/list/'.$resid)); ?>"><div class="potafo_top_menu_sec">Category</div></a>
            <a ><div class="potafo_top_menu_sec potafo_top_menu_act">Review</div></a>
            <a href="<?php echo e(url('menu/tax/'.$resid)); ?>"><div class="potafo_top_menu_sec">Tax %</div></a>
          </div>  
     <div class="card-box table-responsive" style="padding: 8px 10px;">
             
              
                 <div class="col-md-6 no-pad-left">
                <h3>Customer Review</h3>
            </div> 
            
         <div class=" pull-right" style="display:none">
                <div class="table-filter" style="margin-top: 4px;">
                  <div class="table-filter-cc">
                    <a title="Filter" href="#" onclick="filter_view()"> <button type="submit"  style="margin-right: 10px;" class="on-default followups-popup-btn btn btn-primary filter_sec_btn" ><i class="fa fa-filter" aria-hidden="true"></i></button></a>
                </div>
                   
                 </div>
            </div>
                  
            
            
            <div class="filter_box_section_cc diply_tgl">
<!--                <div class="filter_box_section">FILTER</div>-->
                   <div class="filter_text_box_row">
                       <?php echo Form::open(['url'=>'filter/review', 'name'=>'frm_filter', 'id'=>'frm_filter','method'=>'post']); ?>

                        <?php echo e(Form::hidden('resid',$resid, array ('id'=>'resid','name'=>'resid'))); ?>

                       <div class="main_inner_class_track" style="width: 25%;">
                          <div class="group">
                             <div style="position: relative">
                                  <label>From</label>
                                  <input id="flt_from" data-date-format='dd-mm-yyyy' onchange="return filter_change(this.value)" name="flt_from" class="form-control" type="text" >
                              </div>
                           </div>
                        </div>
                       <div class="main_inner_class_track" style="width: 25%;">
                          <div class="group">
                             <div style="position: relative">
                                  <label>To</label>
                                 <input id="flt_to" name="flt_to" data-date-format='dd-mm-yyyy' onchange="return filter_change(this.value)" class="form-control" type="text">
                              </div>
                           </div>
                        </div>
                       <div class="main_inner_class_track" style="width: 25%;">
                          <div class="group">
                             <div style="position: relative">
                                  <label>Name</label>
                                 <input class="form-control" type="text" onkeyup="return filter_change(this.value)" id="flt_name" name="flt_name">
                              </div>
                           </div>
                        </div>
                       

                       <?php echo e(Form::close()); ?>


                   </div>  
            </div>
            <div class="table_section_scroll">  
            <div class="table-responsive">
                  <table class="table table-hover mails m-0 table table-actions-bar" id="example1">
                       <thead>
						<tr>
							<th style="min-width:50px;">#</th>
							<th style="min-width:100px;">Name</th>
							<th style="min-width:10px;">Rating</th>
							<th style="min-width:300px;">Review</th>
							<th style="min-width:80px;">Date</th>
                                                        <th style="min-width:10px;">Status</th>
							</tr>
		        </thead>
											
                        <tbody>
                   <?php if(isset($details)): ?>
                   <?php if(count($details)>0): ?>
                    <?php $__currentLoopData = $details; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php if($item->star>0): ?>
                               <tr class="active">
                                   <td style="min-width:50px;"><?php echo e($key+1); ?></td>
                                   <td style="min-width:100px;"><?php if(isset($item->name)): ?> <?php echo e(title_case($item->name)); ?><?php endif; ?></td>
                                   <td style="min-width:10px;"><?php if(isset($item->star)): ?> <?php echo e($item->star); ?><?php endif; ?></td>
                                   <td><?php if(isset($item->review)): ?> <?php echo e(title_case($item->review)); ?><?php endif; ?></td>
                                   <td style="min-width:80px;"><?php if(isset($item->entry_date)): ?> <?php echo e($item->entry_date); ?><?php endif; ?></td>
<!--                                    <td style="min-width:10px;"></td>-->
                                   <td>
                                       <div class="status_chck<?php echo e($item->id); ?>">
                                            <div class="onoffswitch">
                                                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch<?php echo e($item->id); ?>" <?php if( $item->status == 'Y'): ?> checked <?php endif; ?>>
                                                <label class="onoffswitch-label" for="myonoffswitch<?php echo e($item->id); ?>">
                                                    <span class="onoffswitch-inner" onclick="return  statuschange('<?php echo e($item->id); ?>','<?php echo e($item->status); ?>')"></span>
                                                    <span class="onoffswitch-switch" onclick="return  statuschange('<?php echo e($item->id); ?>','<?php echo e($item->status); ?>')"></span>
                                                </label>
                                            </div>
                                       </div>
                                   </td>
                                 </tr>     
                    <?php endif; ?>                           
                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                   <?php endif; ?>
                   <?php endif; ?>
                                            </tbody>
                                        </table>
                                    </div>
                
           </div>  
          </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->

    


<?php $__env->startSection('jquery'); ?>

     
<script>
    function filter_change(val)
  {
      var frm = $('#frm_filter');
      var table = $('#example1').DataTable();
      $.ajax({
          method: "post",
          url   : "../../api/filter/review",
          data  : frm.serialize(),
          cache : false,
          crossDomain : true,
          async : false,
          dataType :'text',
          success : function(result)
          {
            //  $("#urls").text(result);
              var rows = table.rows().remove().draw();
              var json_x= JSON.parse(result);
              if(parseInt(json_x.length) > 0) {
                  $.each(json_x, function (i, val)
                  {
                      var count = i + 1;
                      var fltname = toTitleCase(val.name);
                      var rating = val.star;
                      var id = val.id;
                      var review = val.review;
                      var edate =  val.entry_date;

                      var myDate = new Date(edate);
                      console.log(myDate);
                      var d = myDate.getDate();
                      var m =  myDate.getMonth();
                      m += 1;  
                      var y = myDate.getFullYear();
                      var newdate=(d+ "-" + m + "-" + y);
                      
                      if(val.status == 'Y')
                        {
                            var status = 'checked';
                        }
                        
                      var newRow = '<tr><td style="min-width:50px;">'+count+'</td>'+
                          '<td style="min-width:100px;">'+fltname+'</td>'+
                          '<td style="min-width:10px;">'+rating+'</td>'+
                          '<td>'+review+'</td>'+
                          '<td style="min-width:80px;">'+newdate+'</td>'+
                          '<td><div class="status_chck'+id+'"><div class="onoffswitch"> <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch'+id+'"  '+status+'> <label class="onoffswitch-label" for="myonoffswitch'+id+'"> <span class="onoffswitch-inner" onclick="return  statuschange('+id+')"></span><span class="onoffswitch-switch" onclick="return  statuschange('+id+')"></span> </label></div></div></td>'+
                          '<td  style="min-width:90px";><a href="" class="table-action-btn button_table" ><i class="md md-delete"></i></a><a href="" class="table-action-btn button_table" ><i class="md md-check"></i></a></td>'+'</tr>';
                      var rowNode = table.row.add($(newRow)).draw().node();
                  });
              }
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              $("#urls").text(jqXHR.responseText); //@text  = response error, it is will be errors: 324, 500, 404 or anythings else
          }
      });
      return true;
  }
  
  function statuschange(id,status) {
            var ids = id;
            var data = {"ids": ids,"status": status,"resid" : $("#resid").val()};
            $.ajax({
                method: "get",
                url: "../../review_status",
                data: data,
                cache: false,
                crossDomain: true,
                async: false,
                dataType: 'text',
                success: function (result)
                {
//                    location.reload();
                },
                error: function (jqXHR, textStatus, errorThrown) {
//                    alert(errorThrown);
                    $("#errbox").text(jqxhr.responseText);
                }
            });
        }

    </script>

    <script type="text/javascript">
        $(document).ready(function()
        {
        var t = $('#example1').DataTable({
            scrollX: false,
            scrollCollapse: true,
            "searching": false,
            "ordering": false,
            "info": false,
             columnDefs: [
            { width: '20%', targets: 0 }
        ],
            "deferLoading": 0,
              "lengthChange": false,
           "columnDefs": [{
                paging: false
            } ],
        });
            
            
      $('#flt_from').datepicker({
                       autoclose: true,
                       todayHighlight: true,
                       
orientation: "bottom left"
                       
    });
             $('#flt_to').datepicker({
                        autoclose: true,
                	todayHighlight: true,
                        
orientation: "bottom left"
                       
    });
            
            
    } );

     $('.filter_sec_btn').on('click', function(e)
    {
        $('.filter_box_section_cc').toggleClass("diply_tgl");
        $("#restaurant_name").focus();
    });
</script>

<?php $__env->stopSection(); ?>



   

<?php $__env->stopSection(); ?>





<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>