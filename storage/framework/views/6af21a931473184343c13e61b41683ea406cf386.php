
<?php $__env->startSection('title','Business Category'); ?>
<?php $__env->startSection('content'); ?>
    <?php
    $pg=app('request')->input('page') ;
    if($pg==''){
        $pg=1;
    }
    $sl=($pg * 25)-24;
    $p=1;
    ?>

    <style>
        .not-active {
            pointer-events: none;
            cursor: default;opacity: 0.5;
            font-weight: bold;
        }
        .add-work-done-poppup-textbox-box label{font-weight:lighter;}
        .inner-textbox-cc input:focus ~ label, input:valid ~ label{font-size:13px;top: -10px;}
        .group{margin-bottom: 14px}
        .sweet-alert{width:300px !important;left: 0 !important;right: 0;margin: auto !important;}

        .bootstrap-select.btn-group .dropdown-menu.inner{max-height:  300px !important;}
        .staff_master_tbl_tbody{
            width: 100%;
            height: 150px;
            margin-bottom: 2px;
            float: left;
            overflow: auto;

        }
        .main_inner_class_track .bootstrap-select{border: solid 1px #ccc;}
        .table_staff_scr_scr thead{ display: inline-block;width: 100%;}
        .table_staff_scr_scr tbody{ display: inline-block;width: 100%;max-height:  390px;overflow: auto   }
        .table_staff_scr_scr tr{ display: inline-block;width: 100%;}
        .table_staff_scr_scr td{ width: 100px;}
        .table_staff_scr_scr th{ width: 100px;}
        .pagination_total_showing{float: left;width: auto;padding-top: 12px;padding-left: 10px;color: #000000;}
        .add-work-done-poppup-textbox-box label{font-weight:lighter;}.inner-textbox-cc input:focus ~ label, input:valid ~ label{font-size:13px;top: -10px;}.group{margin-bottom: 14px}.add-work-done-poppup{height: auto;} div.dataTables_wrapper div.dataTables_filter{float: right;top: 4px;position: relative;}.dataTables_length{top: 7px;position: relative;float: left}
        .dataTables_scrollHeadInner{width: 100% !important}.dataTables_scrollHeadInner table{width: 100% !important}.dataTables_scrollBody table{width: 100% !important} .dataTables_scrollBody {  height: 350px;}

    </style>

    <link href="<?php echo e(asset('public/assets/plugins/datatables/dataTables.bootstrap.min.css')); ?>" rel="stylesheet">


    <input type='hidden' id='url' value='<?php echo e($url); ?>' />

    <div class="col-sm-12">

        <div class="card-box table-responsive" style="padding: 8px 10px;">
            <div class="box master-add-field-box" >
                <div class="col-md-6 no-pad-left">
                    <h3>MANAGE BUSINESS CATEGORY</h3>

                </div>
                <div class="col-md-1 no-pad-left pull-right" style='display:none'>
                    <div class="table-filter" style="margin-top: 4px;">
                        <div class="table-filter-cc">
                            <a href="#"> <button type="submit" style="margin-top: px; border-radius: 4px;margin-left: 0;" class="on-default followups-popup-btn btn btn-primary ad-work-clear-btn" >Add New</button></a>

                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-12">

            </div>

            <table id="example1" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th style="min-width:30px">Slno</th>
                    <th style="min-width:150px">Category</th>
                    <th style="min-width:150px">Status</th>
                    <th style="min-width:50px">Action</th>
                </tr>
                </thead>
                <tbody id="arealisting" style="height:390px">
                <?php if(count($rows)>0): ?>
                    <?php $__currentLoopData = $rows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td style="text-align: left;width:7%"><?php echo e($sl++); ?></td>
                            <td style="text-align: left;width:10%" id="category_<?php echo e($value->id); ?>"><?php echo e($value->category); ?></td>
                            <td style="text-align: left;width:7%">
                               <?php if( $value->active == 'Y'): ?> Active  <?php else: ?>  Inactive <?php endif; ?>
                            </td>
                            <td style="text-align: left;width:10%">
                                <a onclick="return edit_record('<?php echo e($value->id); ?>','<?php echo e($value->active); ?>','<?php echo e($value->pre_scheduled); ?>','<?php echo e($value->cat_image); ?>','<?php echo e($value->disp_order); ?>')" class="btn button_table clear_edit" >
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
                </tbody>

            </table>
        </div>
    </div>


    <div id="url"></div>

    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->

    <div id="add_user" class="add-work-done-poppup-cc" style="display: none;">
        <div class="add-work-done-poppup">
            <div class="add-work-done-poppup-head">Update Category Details
                <a href="#"><div class="close-pop-ad-work-cc ad-work-close-btn"><img src="<?php echo e(asset ('public/assets/images/black_cross.png')); ?>"></div></a>
            </div>
            <div style="text-align:center;" id="branchtimezone"></div>
            <div class="add-work-done-poppup-contant">
                <div class="add-work-done-poppup-textbox-cc">
                    <div class="add-work-done-poppup-textbox-box">

                        <div class="main_container_track_order_list inner-textbox-cc" style="margin-top:10px;margin-bottom:0">
                <?php echo Form::open(['enctype'=>'multipart/form-data','name'=>'frm_data', 'id'=>'frm_data','method'=>'POST']); ?>


                            <input type='hidden' id='url' value='<?php echo e($url); ?>' />
                            <input type='hidden' id='catid' name="catid" />
                            <input type='hidden' id='exist_image' name="exist_image" />
                            <div class="col-xs-3 main_inner_class_track ">
                                <div class="group">
                                    <div style="position: relative">
                                        <label>Category Title</label>
                                        <?php echo Form::text('cat_name',null, ['class'=>'form-control not-active','id'=>'cat_name','name'=>'cat_name','maxlength'=>'100','onkeypress' => 'return charonly(event);','required','style'=>"background-color:transparent;",'autofocus' => "true"]); ?>

                                    </div>
                                </div>
                            </div> 
							<div class="main_inner_class_track ">
                                <div class="group">
                                    <div style="position: relative">
                                        <label>Category Image</label>
										 <input type="file" id="cate_img" name="cate_img" class="save_data form-control">

                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-3 main_inner_class_track">
                                <div class="form-group" >
                                    <label for="status"><span style="color:black">&nbsp;</span></label>
                                    <p style="color: red;margin:0 0 5px"></p>
                                    <?php echo e(Form::select('scheduled_status',['N' => 'Delivery','Y' => 'Pre Schedule'],null,['id' => 'scheduled_status','name'=>'scheduled_status', 'class'=>"form-control"])); ?>

                                </div>
                            </div>
							<div class="col-xs-3 main_inner_class_track">
                                <div class="form-group" >
                                    <label for="status"><span style="color:black">Display Order *</span></label>
                                    <p style="color: red;margin:0 0 5px"></p>
                                        <?php echo Form::text('disp_order',null, ['class'=>'form-control','id'=>'disp_order','name'=>'disp_order','maxlength'=>'100','onkeypress' => 'return numonly(event);','required','style'=>"background-color:transparent;",'autofocus' => "true"]); ?>

                                </div>
                            </div> 
							<div class="col-xs-3 main_inner_class_track">
                                <div class="form-group" id="status_p" style="display: none">
                                    <label for="status"><span style="color:black">&nbsp;</span></label>
                                    <p style="color: red;margin:0 0 5px"></p>
                                    <?php echo e(Form::select('status',['Y' => 'Active','N' => 'Inactive'],null,['id' => 'status','name'=>'status', 'class'=>"form-control"])); ?>

                                </div>
                            </div>

                            <div class="main_inner_class_track" style="width:20%">
                                <b><p class="" style="color: #000;float:right;cursor:pointer;display: none;background-color: burlywood;margin-top: 6px;padding: 2px 13px;line-height: 30px;" id="getimage" data-toggle="modal"  data-target="#myModal" data-title=""><a style="color: #000;">View image</a></p></b>

                            </div>

                            <div class="box-footer">
                                <input type="hidden" name="type" id="type" />
                                <a id="inserting" name="inserting"  class="staff-add-pop-btn staff-add-pop-btn-new" onclick="update_record();">Update</a>
                            </div>
						<?php echo e(Form::close()); ?>	
                        </div>


                    </div>
                </div>
            </div><!--add-work-done-poppup-textbox-cc-->
        </div>
        <div class="add-work-list-cc">
            <!--<div class="add-work-list-head">LIST</div>-->


        </div><!--add-work-done-poppup-->

    </div>
    <div id="edit_load">
    </div>



    <style>#datatable-fixed-col_filter{display:none}table.dataTable thead th{white-space:nowrap;padding-right: 20px;}
        .on-default	{margin-left:10px;}div.dataTables_info {padding-top:13px;}
        .height_align{
            margin-top: 12px;
        }
    </style>
    <link href="<?php echo e(asset('public/assets/plugins/bootstrap-sweetalert/sweet-alert.css')); ?>" rel="stylesheet" type="text/css">
    <script src="<?php echo e(asset('public/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/assets/pages/jquery.sweet-alert.init.js')); ?>"></script>
    <style>#datatable-fixed-col_filter{display:none}table.dataTable thead th{white-space:nowrap;padding-right: 20px;}
        .on-default	{margin-left:10px;}div.dataTables_info {padding-top:13px;}
    </style>

<?php $__env->startSection('jquery'); ?>
    <script>
        $.fn.dataTable.ext.errMode = 'none';
    </script>
    <script>

        $(document).ready(function() {
            var t = $('#example1').DataTable( {
                scrollY: "380px",
                scrollX: true,
                scrollCollapse: true,
                "columnDefs": [ {

                    paging: false


                } ],
                "searching": true,
                "ordering": false,
                "iDisplayLength": 5
            } );
        } );
        $(document).ready(function(){
            $(".alert").delay(600).slideUp(300);
        });
    </script>
    <script>
        $(document).ready(function()
        {
            $(".input-sm").focus();
        });
    </script>
    <script>
        $(document).ready(function() {

            $('#myModal').on("show.bs.modal", function (e) {
                $("#fav-title").attr("src",$(e.relatedTarget).data('title'));
            });
        });
        function hasExtension(inputID, exts) {
            var fileName = document.getElementById(inputID).value;
            return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
        }
        $(".followups-popup-btn").click(function(){

            $("#add_user").show();
             $("#cat_name").val('');
            $("#catid").val('');
			 $("#scheduled_status").val('');
			 $("#disp_order").val('');
            $("#exist_image").val('');
        });
        $(".ad-work-close-btn").click(function(){
            $("#reset_pasword").css("display",'none');
            $("#add_user").hide();
			 $("#cat_name").val('');
            $("#catid").val('');
            $("#disp_order").val('');
           
            $("#scheduled_status").val('');
            $("#exist_image").val(''); 
        });
        
        $(".ad-work-clear-btn").click(function(){
            $("#cat_name").val('');
			 $("#scheduled_status").val('');
            $("#exist_image").val('');
            $("#cat_name").focus();
            $("#status").hide();
        });
        function update_record()
        {
            var table ;
            $('.notifyjs-wrapper').remove();
            var cat_name = $("#cat_name").val();
            var catid =$("#catid").val();
            var status =$("#status").val();
            var disp_order =$("#disp_order").val();
            if(cat_name.trim() == '') {
                $("#cat_name").focus();
                $.Notification.autoHideNotify('warning', 'top right','Enter Category Name.');
                return false;
            }
            if(disp_order.trim() == '' || disp_order<=0) {
                $("#disp_order").focus();
                $.Notification.autoHideNotify('warning', 'top right','Enter Display Order.');
                return false;
            }

            if(true)
            {
				//
              //  var data= {"cat_name":cat_name,"status":status,"catid":catid};
			   var formdata = new FormData($('#frm_data')[0]);
                $.ajax({
                    method: "post",
                    url : "api/update_business_cat",
                    data : formdata,
					async:false,
                    type:'post',
                    processData: false,
                    contentType: false,
                    success : function(result)
                    {
                        

                        if(result=='success')
                        {
                            swal({

                                title: "",
                                text: "Updated Successfully",
                                timer: 4000,
                                showConfirmButton: false
                            });
							location.reload();
                        }
                        else 
                        {
                            swal({

                                title: "",
                                text: "Already Exist",
                                timer: 4000,
                                showConfirmButton: false
                            });
                        }


                       

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(jqXHR.responseText);
                        $("#urls").text(jqXHR.responseText); //@text  = response error, it is will be errors: 324, 500, 404 or anythings else
                    }
                });

            }

        }

        function edit_record(id,status,scheduled,image,disp_order)
        {
            $("#catid").val(id);
            $("#status").val(status);
			var cat_name = $("#category_"+id).html();
            $("#cat_name").val(cat_name);
            $("#scheduled_status").val(scheduled);
            $("#exist_image").val(image);
            $("#disp_order").val(disp_order);
            
            $("#add_user").css("display",'block');
            $("#status_p").css("display",'block');

        }
          </script>
    <script>
        function numonly(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                    && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function charonly(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && keyCode != 32)
                return false;
            return true;
        }
    </script>
  

<?php $__env->stopSection(); ?>

<script>

    function numonly(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    function charonly(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (!(charCode >= 65 && charCode <= 120) && (charCode != 32 && charCode != 0))
            return false;
        return true;
    }
</script>
<script>
    $(document).ready(function() {
        $('#example1').DataTable( {
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
        } );
    } );

</script>

<?php $__env->stopSection(); ?>





<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>