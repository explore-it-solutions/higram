
<?php $__env->startSection('title','Potafo - Admin Dashboard'); ?>
<?php $__env->startSection('content'); ?>
<?php
        $pg=app('request')->input('page') ;
        if($pg==''){
            $pg=1;
        }
        $sl=($pg * 25)-24;
        $p=1;
?>
                   
<style>
  .most_sale text[text-anchor="middle"]{display:none}
    .content-page > .content{padding: 20px;}.table-responsive{float: left;width: 100%;}.portlet{float: left;width: 100%;box-shadow: 0px 1px 20px rgba(0, 0, 0, 0.10);}.portlet .portlet-heading .portlet-title{font-size: 19px; margin-bottom: 17px;}
    .table td{font-size: 14px;padding: 10px 8px !important;}
     .card:before {
    position: absolute;
    bottom: 0;
    left: -55px;
    z-index: 1;
    display: block;
    width: 60px;
    height: 75px;
    background-color: rgba(0, 0, 0, 0.10);
    content: "";
    -webkit-transform: skewX(40deg);
    -moz-transform: skewX(40deg);
    -ms-transform: skewX(40deg);
    -o-transform: skewX(40deg);
    transform: skewX(40deg);
}
    .top_sm_anylt_sec{display: none}
    .grph_1 h4{color:#242424}.grph_1 h5{color:#242424}
    .portlet .portlet-heading .portlet-title{margin-bottom:10px}
    .chart-detail-list li{margin: 0px 0px;float: left;}
    .das_btn_1{width:auto;float:left;padding:5px 10px;color:#242424;box-shadow:0px 3px 15px #c5c5c5;margin-right:10px;margin-bottom:15px;border: 1px #ccc solid;opacity: 0.5;}.das_btn_1_act{}
</style>
<div class="dashboard_main_container">
    
    <div class="row">
    <div class="col-md-3 stretch-card ">
              <div class="card bg-gradient-anth card-img-holder text-white">
                <div class="card-body">
                  <img src="public/assets/images/circle.svg" class="card-img-absolute" alt="circle-image">
                    <h6 class="card-text">Today</h6>
                  <h4 class="font-weight-normal mb-3">Total Order
                    <i class="fa fa-signal mdi-24px float-right"></i>
                  </h4>
                  <h2 class="mb-5"><?=$total_orders[0]->total?></h2>
                  
                </div>
              </div>
         </div>
         <div class="col-md-3 stretch-card ">
              <div class="card bg-gradient-success  card-img-holder text-white">
                <div class="card-body">
                  <img src="public/assets/images/circle.svg" class="card-img-absolute" alt="circle-image">
                    <h6 class="card-text">Today</h6>
                  <h4 class="font-weight-normal mb-3">Unassigned orders
                    <i class="fa fa-pie-chart  mdi-24px float-right"></i>
                  </h4>
                  <h2 class="mb-5"><?=$cancelled_orders[0]->total?></h2>
                  
                </div>
              </div>
         </div>
        <div class="col-md-3 stretch-card ">
              <div class="card bg-gradient-danger  card-img-holder text-white">
                <div class="card-body">
                  <img src="public/assets/images/circle.svg" class="card-img-absolute" alt="circle-image">
                    <h6 class="card-text">Today</h6>
                  <h4 class="font-weight-normal mb-3">Delivery Pending
                    <i class="fa fa-line-chart mdi-24px float-right"></i>
                  </h4>
                  <h2 class="mb-5"><?=$pending_orders[0]->total?></h2>
                  
                </div>
              </div>
         </div>
        <div class="col-md-3 stretch-card">
              <div class="card bg-gradient-info card-img-holder text-white">
                <div class="card-body">
                  <img src="public/assets/images/circle.svg" class="card-img-absolute" alt="circle-image">
                    <h6 class="card-text">Today</h6>
                  <h4 class="font-weight-normal mb-3">Completed Order
                    <i class="fa fa-bar-chart  mdi-24px float-right"></i>
                  </h4>
                  <h2 class="mb-5"><?=$completed_orders[0]->total?></h2>
                  
                </div>
              </div>
         </div>
        
        
        
    </div>


    <div class="row">

<div class="col-lg-12 grid-margin">
  
    <div class="portlet"><!-- /primary heading -->
		                            <div class="portlet-heading">
		                                <h3 class="portlet-title text-dark">
		                                  Hourly Sales
		                                </h3>
		                                <div class="portlet-widgets">
		                                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
		                                    <span class="divider"></span>
		                                    <a data-toggle="collapse" data-parent="#accordion1" href="#portlet5"><i class="ion-minus-round"></i></a>
		                                    <span class="divider"></span>
		                                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
		                                </div>
		                                <div class="clearfix"></div>
		                            </div>
		                            <div id="portlet5" class="panel-collapse collapse in">
		                                <div class="portlet-body">
		                                    <div id="combine-chart">
	                                            <div id="combine-chart-container" class="flot-chart" style="height: 320px;">
	                                            </div>
	                                        </div>
		                                </div>
		                            </div>
		                        </div>
    

</div>

<div class="row">
                        	<div class="col-lg-6">
		                        <div class="portlet"><!-- /primary heading -->
		                            <div class="portlet-heading">
		                                <h3 class="portlet-title text-dark">
                                    Vendor Sales
		                                </h3>
		                                <div class="portlet-widgets">
		                                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
		                                    <span class="divider"></span>
		                                    <a data-toggle="collapse" data-parent="#accordion1" href="#portlet1"><i class="ion-minus-round"></i></a>
		                                    <span class="divider"></span>
		                                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
		                                </div>
		                                <div class="clearfix"></div>
		                            </div>
		                            <div id="portlet1" class="panel-collapse collapse in">
		                                <div class="portlet-body">
		                                    <div id="website-stats" style="height: 320px;" class="flot-chart"></div>
		                                </div>
		                            </div>
		                        </div>
                        </div>
                      
                        	<div class="col-lg-6">
		                        <div class="portlet"><!-- /primary heading -->
		                            <div class="portlet-heading">
		                                <h3 class="portlet-title text-dark">
                                    MOST SELLING
		                                </h3>
		                                <div class="portlet-widgets">
		                                    <a href="javascript:;" data-toggle="reload"><i class="ion-refresh"></i></a>
		                                    <span class="divider"></span>
		                                    <a data-toggle="collapse" data-parent="#accordion1" href="#portlet3"><i class="ion-minus-round"></i></a>
		                                    <span class="divider"></span>
		                                    <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a>
		                                </div>
		                                <div class="clearfix"></div>
		                            </div>
		                            <div id="portlet3" class="panel-collapse collapse in">
		                                <div class="portlet-body">
		                                    <div id="donut-chart">
	                                            <div id="donut-chart-container" class="flot-chart" style="height: 320px;">
	                                            </div>
	                                        </div>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                    <div class="col-lg-6" style="height:0;overflow:hidden">
                        <div id="portlet4" class="panel-collapse collapse in" >
		                                <div class="portlet-body">
		                                    <div id="pie-chart">
	                                            <div id="pie-chart-container" class="flot-chart" style="height: 320px;">
	                                            </div>
	                                        </div>
		                                </div>
                                </div>
                                </div>
		                    
		                    <div class="col-lg-6" style="height:0;overflow:hidden">
                             <div id="portlet2" class="panel-collapse collapse in">
		                                <div class="portlet-body">
		                                    <div id="flotRealTime" style="height: 320px;" class="flot-chart"></div>
		                                </div>
		                            </div>
                        </div>
  </div>





</div>







    
    <div class="row">

                    <div class="col-lg-6">

                        <div class="portlet"><!-- /primary heading -->
                            <div class="portlet-heading">
                                <h3 class="portlet-title text-dark text-uppercase">
                                    Today's Delivery Staffs
                              
                                </h3>
                                
                                
                            </div>
                            <div id="portlet2" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Pending odr</th>
                                                    <th>Total odrs</th>
                                                    <th>Number</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if(count($all)>0): ?>
                                                <?php $i = 0; ?>
                                                  <?php $__currentLoopData = $all; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                               <?php $i++;  ?>   
                                                        <tr>
                                                            <td><?=$i?></td>
                                                            <td><?=$value['name']?></td>
                                                            <td><span class="label label-purple"><?=$value['pending']?></span></td>
                                                            <td><?=$value['all_order']?></td>
                                                            <td><?=$value['mobile']?></td>
                                                        </tr>
                                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
        
        <div class="col-lg-6">

                        <div class="portlet"><!-- /primary heading -->
                            <div class="portlet-heading">
                                <h3 class="portlet-title text-dark text-uppercase">
                                Today's Orders
                                </h3>
                            </div>
                            <div id="portlet2" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>ID</th>
                                                    <th>Vendor</th>
                                                    <th>Place</th>
                                                    <th>Amount</th>
                                                    <th>Name</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>ID254878</td>
                                                    <td>2nd Gate</td>
                                                    <td>Nadakav</td>
                                                    <td>300</td>
                                                    <td>Abcd Name</td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>ID254878</td>
                                                    <td>Bun Club</td>
                                                    <td>Mavoor...</td>
                                                    <td>28</td>
                                                    <td>9876543210</td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>3</td>
                                                    <td>ID254878</td>
                                                    <td>Adaminte...</td>
                                                    <td>Beach</td>
                                                    <td>25</td>
                                                    <td>9876543210</td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>4</td>
                                                    <td>ID254878</td>
                                                    <td>Ojin</td>
                                                    <td>Nadakav</td>
                                                    <td>20</td>
                                                    <td>9876543210</td>
                                                </tr>
                                                <tr>
                                                    <td>5</td>
                                                    <td>ID254878</td>
                                                    <td>Biriyani...</td>
                                                    <td>Nadakav</td>
                                                    <td>18</td>
                                                    <td>9876543210</td>
                                                </tr>
                                                <tr>
                                                    <td>1</td>
                                                    <td>ID254878</td>
                                                    <td>2nd Gate</td>
                                                    <td>Nadakav</td>
                                                    <td>300</td>
                                                    <td>Abcd Name</td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>ID254878</td>
                                                    <td>Bun Club</td>
                                                    <td>Mavoor...</td>
                                                    <td>28</td>
                                                    <td>9876543210</td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>3</td>
                                                    <td>ID254878</td>
                                                    <td>Adaminte...</td>
                                                    <td>Beach</td>
                                                    <td>25</td>
                                                    <td>9876543210</td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>4</td>
                                                    <td>ID254878</td>
                                                    <td>Ojin</td>
                                                    <td>Nadakav</td>
                                                    <td>20</td>
                                                    <td>9876543210</td>
                                                </tr>
                                                <tr>
                                                    <td>5</td>
                                                    <td>ID254878</td>
                                                    <td>Biriyani...</td>
                                                    <td>Nadakav</td>
                                                    <td>18</td>
                                                    <td>9876543210</td>
                                                </tr>
                                                <tr>
                                                    <td>1</td>
                                                    <td>ID254878</td>
                                                    <td>2nd Gate</td>
                                                    <td>Nadakav</td>
                                                    <td>300</td>
                                                    <td>Abcd Name</td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>ID254878</td>
                                                    <td>Bun Club</td>
                                                    <td>Mavoor...</td>
                                                    <td>28</td>
                                                    <td>9876543210</td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>3</td>
                                                    <td>ID254878</td>
                                                    <td>Adaminte...</td>
                                                    <td>Beach</td>
                                                    <td>25</td>
                                                    <td>9876543210</td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>4</td>
                                                    <td>ID254878</td>
                                                    <td>Ojin</td>
                                                    <td>Nadakav</td>
                                                    <td>20</td>
                                                    <td>9876543210</td>
                                                </tr>
                                                <tr>
                                                    <td>5</td>
                                                    <td>ID254878</td>
                                                    <td>Biriyani...</td>
                                                    <td>Nadakav</td>
                                                    <td>18</td>
                                                    <td>9876543210</td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
        
        


                </div>
    
    
    

</div><!--dashboard_main_container-->

  

 
    




    
<?php $__env->startSection('jquery'); ?>


      <script src="public/assets/plugins/flot-chart/jquery.flot.min.js"></script>
        <script src="public/assets/plugins/flot-chart/jquery.flot.time.js"></script>
        <script src="public/assets/plugins/flot-chart/jquery.flot.tooltip.min.js"></script>
        <script src="public/assets/plugins/flot-chart/jquery.flot.resize.js"></script>
        <script src="public/assets/plugins/flot-chart/jquery.flot.pie.js"></script>
        <script src="public/assets/plugins/flot-chart/jquery.flot.selection.js"></script>
        <script src="public/assets/plugins/flot-chart/jquery.flot.stack.js"></script>
        <script src="public/assets/plugins/flot-chart/jquery.flot.orderBars.min.js"></script>
        <script src="public/assets/plugins/flot-chart/jquery.flot.crosshair.js"></script>
        <script src="public/assets/pages/jquery.flot.init.js"></script>
    

 
<?php $__env->stopSection(); ?>



<?php $__env->stopSection(); ?>






<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>