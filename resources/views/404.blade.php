<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="{{ app()->getLocale() }}">
<head>
    <?php echo
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: text/html');?>
    <meta http-equiv='cache-control' content='no-cache'>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Deliveru Home | 404</title>

    <style>
        .loader_img{
            position: absolute;
            margin: auto;
            top: 100px;
            bottom: 0;
            width: 40px;
            height: 40px;
            right: 0;
            left: 0;
            display:none;
            z-index:1;
        }
        .sweet-alert .btn-lg {
            font-size: 15px !important;
        }
        .btn.focus, .btn:focus, .btn:hover {
            color: #333;
            text-decoration: none;
        }
        .btn-primary, .btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .btn-primary.focus, .btn-primary:active, .btn-primary:focus, .btn-primary:hover, .open > .dropdown-toggle.btn-primary {
            background-color: #5d9cec !important;
            border: 1px solid #5d9cec !important
        }
        .btn-group-lg>.btn, .btn-lg {
            padding: 10px 16px;
            font-size: 18px;
            line-height: 1.3333333;
            border-radius: 6px;
        }
        button {
            overflow: visible;
        }
        .h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6 {
            font-family: inherit;
            font-weight: 500;
            line-height: 1.1;
            color: inherit;
        }
        h1, h2, h3, h4, h5, h6 {
            color: #505458;
            font-family: "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
            margin: 10px 0;
        }
        button, select {
            text-transform: none;
        }
        .sweet-alert .btn-lg {
            font-size: 15px !important;
        }
        button, html input[type=button], input[type=reset], input[type=submit] {
            -webkit-appearance: button;
            cursor: pointer;
        }
        .btn-primary, .btn-success, .btn-default, .btn-info, .btn-warning, .btn-danger, .btn-inverse, .btn-purple, .btn-pink {
            color: #ffffff !important;
        }

        .btn
        {
            border-radius: 3px;
            outline: none !important;
        }
        .btn-group-lg>.btn, .btn-lg {
            padding: 10px 16px;
            font-size: 18px;
            line-height: 1.3333333;
            border-radius: 6px;
        }
    </style>
	<style>.frt_ft{width: 100%;height: auto;float: left;padding: 50px 0;text-align: center;font-family: 'RobotoRegular';}.frt_ft h1{font-size: 80px}.frt_ft p{font-size: 14px;color: #666}</style>

</head>
<link rel="stylesheet" type="text/css" href="{{ asset('public/assets/css/component.css') }}" />
<link href="{{ asset('public/assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('public/assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('public/assets/css/style.css') }}" rel="stylesheet" />
<script src="{{ asset('public/assets/script/jquery.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('public/assets/js/angular.min.js') }}"></script>
<script src="{{ asset('public/assets/js/modernizr.custom.js') }}"></script>
<script src="{{ asset('public/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
<script src="{{ asset('public/assets/pages/jquery.sweet-alert.init.js') }}"></script>

<body onload="load_ip()">

<div class="container" id="divID" ng-controller="myCtrl">

   <div class="login_top_header">
        <span><img src="{{ asset('public/assets/images/comp_logo.png') }}" /></span>
    </div>

    <div  >
    
<div class="container">
  <div class="frt_ft">
    <h1>404!</h1>
    <p>Sorry, the page you requested was not found.
      Please click <a routerLink="/"> here </a> to find what you are looking for.</p>
  </div>
</div>


    </div>
        <span class="final-message"></span>
</div>

</body>
</html>
