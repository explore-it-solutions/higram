@extends('layouts.app')
@section('title','Delivery Charge')
@section('content')
    <?php
    $pg=app('request')->input('page') ;
    if($pg==''){
        $pg=1;
    }
    $sl=($pg * 25)-24;
    $p=1;
    ?>

    <style>
        .not-active {
            pointer-events: none;
            cursor: default;opacity: 0.5;
            font-weight: bold;
        }
        .add-work-done-poppup-textbox-box label{font-weight:lighter;}
        .inner-textbox-cc input:focus ~ label, input:valid ~ label{font-size:13px;top: -10px;}
        .group{margin-bottom: 14px}
        .sweet-alert{width:300px !important;left: 0 !important;right: 0;margin: auto !important;}

        .bootstrap-select.btn-group .dropdown-menu.inner{max-height:  300px !important;}
        .staff_master_tbl_tbody{
            width: 100%;
            height: 150px;
            margin-bottom: 2px;
            float: left;
            overflow: auto;

        }
        .main_inner_class_track .bootstrap-select{border: solid 1px #ccc;}
        .table_staff_scr_scr thead{ display: inline-block;width: 100%;}
        .table_staff_scr_scr tbody{ display: inline-block;width: 100%;max-height:  390px;overflow: auto   }
        .table_staff_scr_scr tr{ display: inline-block;width: 100%;}
        .table_staff_scr_scr td{ width: 100px;}
        .table_staff_scr_scr th{ width: 100px;}
        .pagination_total_showing{float: left;width: auto;padding-top: 12px;padding-left: 10px;color: #000000;}
        .add-work-done-poppup-textbox-box label{font-weight:lighter;}.inner-textbox-cc input:focus ~ label, input:valid ~ label{font-size:13px;top: -10px;}.group{margin-bottom: 14px}.add-work-done-poppup{height: auto;} div.dataTables_wrapper div.dataTables_filter{float: right;top: 4px;position: relative;}.dataTables_length{top: 7px;position: relative;float: left}
        .dataTables_scrollHeadInner{width: 100% !important}.dataTables_scrollHeadInner table{width: 100% !important}.dataTables_scrollBody table{width: 100% !important} .dataTables_scrollBody {  height: 350px;}

    </style>

    <link href="{{ asset('public/assets/plugins/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet">


    <input type='hidden' id='url' value='{{$url}}' />

    <div class="col-sm-12">

        <div class="card-box table-responsive" style="padding: 8px 10px;">
            <div class="box master-add-field-box" >
                <div class="col-md-6 no-pad-left">
                    <h3>Manage Delivery Charge</h3>

                </div>
                <div class="col-md-1 no-pad-left pull-right">
                    <div class="table-filter" style="margin-top: 4px;">
                        <div class="table-filter-cc">
                            <a href="#"> <button type="submit" style="margin-top: px; border-radius: 4px;margin-left: 0;" class="on-default followups-popup-btn btn btn-primary ad-work-clear-btn" >Add New</button></a>

                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-12">

            </div>

            <table id="example1" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th style="min-width:30px">Slno</th>
                    <th style="min-width:150px">From Range</th>
                    <th style="min-width:150px">To Range</th>
                    <th style="min-width:150px">Charge</th>
                    <th style="min-width:150px">Status</th>
                    <th style="min-width:50px">Action</th>
                </tr>
                </thead>
                <tbody id="arealisting" style="height:390px">
                @if(count($rows)>0)
                    @foreach($rows as $value)
                        <tr>
                            <td style="text-align: left;width:7%">{{ $sl++ }}</td>
                            <td style="text-align: left;width:10%">{{ $value->from_range}}</td>
                            <td style="text-align: left;width:10%">{{ $value->to_range}}</td>
                            <td style="text-align: left;width:10%">{{ $value->charge}}</td>
                            <td style="text-align: left;width:7%">
                               @if( $value->active == 'Y') Active  @else  Inactive @endif
                            </td>
                            <td style="text-align: left;width:10%">
                                <a onclick="return edit_record('{{$value->id}}','{{$value->from_range}}','{{$value->to_range}}','{{$value->charge}}','{{$value->active}}')" class="btn button_table clear_edit" >
                                    <i class="fa fa-pencil"></i>
                                </a>
								<a onclick="return delete_record('{{$value->id}}')" class="btn button_table clear_edit" >
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>

            </table>
        </div>
    </div>


    <div id="url"></div>

    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->

    <div id="add_user" class="add-work-done-poppup-cc" style="display: none;">
        <div class="add-work-done-poppup">
            <div class="add-work-done-poppup-head"><span id="pop_title"></span>
                <a href="#"><div class="close-pop-ad-work-cc ad-work-close-btn"><img src="{{asset ('public/assets/images/black_cross.png') }}"></div></a>
            </div>
            <div style="text-align:center;" id="branchtimezone"></div>
            <div class="add-work-done-poppup-contant">
                <div class="add-work-done-poppup-textbox-cc">
                    <div class="add-work-done-poppup-textbox-box">

                        <div class="main_container_track_order_list inner-textbox-cc" style="margin-top:10px;margin-bottom:0">
                {!! Form::open(['enctype'=>'multipart/form-data','name'=>'frm_data', 'id'=>'frm_data','method'=>'POST']) !!}

                            <input type='hidden' id='url' value='{{$url}}' />
                            <input type='hidden' id='del_id' name="del_id" value='' />
                            <div class="col-xs-3 main_inner_class_track ">
                                <div class="group">
                                    <div style="position: relative">
                                        <label>From Range</label>
                                        {!! Form::text('from_range',null, ['class'=>'form-control','id'=>'from_range','name'=>'from_range','maxlength'=>'6','onkeypress' => 'return numonly(event);','required','style'=>"background-color:transparent;",'autofocus' => "true"]) !!}
                                    </div>
                                </div>
                            </div> 
							<div class="col-xs-3 main_inner_class_track ">
                                <div class="group">
                                    <div style="position: relative">
                                        <label>To Range</label>
                                        {!! Form::text('to_range',null, ['class'=>'form-control','id'=>'to_range','name'=>'to_range','maxlength'=>'6','onkeypress' => 'return numonly(event);','required','style'=>"background-color:transparent;",'autofocus' => "true"]) !!}

                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-3 main_inner_class_track">
                                <div class="form-group" >
                                    <label for="status"><span style="color:black">Charge</span></label>
                                    <p style="color: red;margin:0 0 5px"></p>
                                        {!! Form::text('del_charge',null, ['class'=>'form-control','id'=>'del_charge','name'=>'del_charge','maxlength'=>'15','onkeypress' => 'return numonly(event);','required','style'=>"background-color:transparent;",'autofocus' => "true"]) !!}
                                </div>
                            </div> 
							<div class="col-xs-3 main_inner_class_track">
                                <div class="form-group" id="status_p" style="display: none">
                                    <label for="status"><span style="color:black">&nbsp;</span></label>
                                    <p style="color: red;margin:0 0 5px"></p>
                                    {{ Form::select('status',['Y' => 'Active','N' => 'Inactive'],null,['id' => 'status','name'=>'status', 'class'=>"form-control"])}}
                                </div>
                            </div>

                            <div class="main_inner_class_track" style="width:20%">
                                <b><p class="" style="color: #000;float:right;cursor:pointer;display: none;background-color: burlywood;margin-top: 6px;padding: 2px 13px;line-height: 30px;" id="getimage" data-toggle="modal"  data-target="#myModal" data-title=""><a style="color: #000;">View image</a></p></b>

                            </div>

                            <div class="box-footer">
                                <input type="hidden" name="type" id="type" />
                                <a id="inserting" name="inserting"  class="staff-add-pop-btn staff-add-pop-btn-new" onclick="update_record();">Submit</a>
                            </div>
						{{ Form::close() }}	
                        </div>


                    </div>
                </div>
            </div><!--add-work-done-poppup-textbox-cc-->
        </div>
        <div class="add-work-list-cc">
            <!--<div class="add-work-list-head">LIST</div>-->


        </div><!--add-work-done-poppup-->

    </div>
    <div id="edit_load">
    </div>



    <style>#datatable-fixed-col_filter{display:none}table.dataTable thead th{white-space:nowrap;padding-right: 20px;}
        .on-default	{margin-left:10px;}div.dataTables_info {padding-top:13px;}
        .height_align{
            margin-top: 12px;
        }
    </style>
    <link href="{{ asset('public/assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css">
    <script src="{{ asset('public/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
    <script src="{{ asset('public/assets/pages/jquery.sweet-alert.init.js') }}"></script>
    <style>#datatable-fixed-col_filter{display:none}table.dataTable thead th{white-space:nowrap;padding-right: 20px;}
        .on-default	{margin-left:10px;}div.dataTables_info {padding-top:13px;}
    </style>

@section('jquery')
    <script>
        $.fn.dataTable.ext.errMode = 'none';
    </script>
    <script>

        $(document).ready(function() {
            var t = $('#example1').DataTable( {
                scrollY: "380px",
                scrollX: true,
                scrollCollapse: true,
                "columnDefs": [ {

                    paging: false


                } ],
                "searching": true,
                "ordering": false,
                "iDisplayLength": 10
            } );
        } );
        $(document).ready(function(){
            $(".alert").delay(600).slideUp(300);
        });
    </script>
    <script>
        $(document).ready(function()
        {
            $(".input-sm").focus();
        });
    </script>
    <script>
        $(document).ready(function() {

            $('#myModal').on("show.bs.modal", function (e) {
                $("#fav-title").attr("src",$(e.relatedTarget).data('title'));
            });
        });
        function hasExtension(inputID, exts) {
            var fileName = document.getElementById(inputID).value;
            return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
        }
       
        $(".ad-work-close-btn").click(function(){
            $("#reset_pasword").css("display",'none');
			$("#status_p").css("display",'none');
             $("#del_id").val('');
            $("#status").val('');
            $("#from_range").val('');
            $("#to_range").val('');
            $("#del_charge").val('');
            $("#add_user").hide();
            
        });
        
        $(".ad-work-clear-btn").click(function(){
			 $("#add_user").show();
             $("#pop_title").html("ADD");
             $("#inserting").html("Insert");
            $("#cat_name").val('');
			 $("#scheduled_status").val('');
            $("#exist_image").val('');
            $("#cat_name").focus();
            $("#status").hide();
        });
        function update_record()
        {
            var table ;
            $('.notifyjs-wrapper').remove();
            var from_range = $("#from_range").val();
            var to_range =$("#to_range").val();
            var del_charge =$("#del_charge").val();
            var status =$("#status").val();
            if(from_range.trim() == '' || isNaN(from_range)==true) {
                $("#from_range").focus();
                $.Notification.autoHideNotify('warning', 'top right','Enter Start Range.');
                return false;
            } else if(to_range.trim() == '' || isNaN(to_range)==true) {
                $("#to_range").focus();
                $.Notification.autoHideNotify('warning', 'top right','Enter End Range.');
                return false;
            }  else if(parseFloat(from_range)>parseFloat(to_range)) {
                $("#to_range").focus();
                $.Notification.autoHideNotify('warning', 'top right','From Range Must be Less Than End Range.');
                return false;
            } else if(del_charge.trim() == '' || isNaN(del_charge)==true) {
                $("#del_charge").focus();
                $.Notification.autoHideNotify('warning', 'top right','Enter Charge.');
                return false;
            }

            if(true)
            {
				//
              //  var data= {"cat_name":cat_name,"status":status,"catid":catid};
			   var formdata = new FormData($('#frm_data')[0]);
                $.ajax({
                    method: "post",
                    url : "api/update_delivery_charge",
                    data : formdata,
					async:false,
                    type:'post',
                    processData: false,
                    contentType: false,
                    success : function(result)
                    {
                        

                        if(result=='inserted')
                        {
                            swal({

                                title: "",
                                text: "Added Successfully",
                                timer: 2000,
                                showConfirmButton: false
                            });
							location.reload();
                        } else if(result=='updated')
                        {
                            swal({

                                title: "",
                                text: "Updated Successfully",
                                timer: 2000,
                                showConfirmButton: false
                            });
							location.reload();
                        }
                        else 
                        {
                            swal({

                                title: "",
                                text: "Already Exist",
                                timer: 2000,
                                showConfirmButton: false
                            });
                        }


                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(jqXHR.responseText);
                        $("#urls").text(jqXHR.responseText); //@text = response error, it is will be errors: 324, 500, 404 or anythings else
                    }
                });

            }

        }

        function edit_record(id,from,to,charge,active)
        {
            $("#del_id").val(id);
            $("#status").val(status);
            $("#from_range").val(from);
            $("#to_range").val(to);
            $("#del_charge").val(charge);
            $("#pop_title").html("EDIT");
             $("#inserting").html("Update");
            $("#add_user").css("display",'block');
            $("#status_p").css("display",'block');

        }
		function delete_record(delid){
			 swal({
                    title: "",
                    text: 'Are you sure you want to Remove this Record',
                    type: "info",
                    showCancelButton: true,
                    cancelButtonClass: 'btn-white btn-md waves-effect',
                    confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
                    confirmButtonText: 'CONFIRM',
                    closeOnConfirm: true
                    }, function (isConfirm)
                   {
                     if (isConfirm)
                     {
                        $.ajax({
                    method: "post",
                    url : "api/update_delivery_charge",
                    data : {"operation":"delete","del_id":delid},
                    success : function(result)
                    {
                     swal({

                                title: "",
                                text: "Deleted Successfully",
                                timer: 4000,
                                showConfirmButton: false
                            });
							location.reload();

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert(jqXHR.responseText);
                        $("#urls").text(jqXHR.responseText); //@text = response error, it is will be errors: 324, 500, 404 or anythings else
                    }
                });
                               
                     }
                   });
		}
          </script>
    <script>
        function numonly(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                    && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function charonly(evt)
        {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && keyCode != 32)
                return false;
            return true;
        }
    </script>
  

@stop

<script>

    function numonly(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    function charonly(evt)
    {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (!(charCode >= 65 && charCode <= 120) && (charCode != 32 && charCode != 0))
            return false;
        return true;
    }
</script>
<script>
    $(document).ready(function() {
        $('#example1').DataTable( {
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
        } );
    } );

</script>

@endsection




