@extends('layouts.app')
@section('title','Potafo - Manage Vendor')
@section('content')
<style>
     .not-active {
          pointer-events: none;
          cursor: default;opacity: 0.5;
          font-weight: bold;
       }
    .add-work-done-poppup-textbox-box label{font-weight:lighter;}
    .inner-textbox-cc input:focus ~ label, input:valid ~ label{font-size:13px;top: -10px;}
    .group{margin-bottom: 14px}
    .sweet-alert{width:300px !important;left: 0 !important;right: 0;margin: auto !important;}
 
.bootstrap-select.btn-group .dropdown-menu.inner{max-height:  300px !important;}
.staff_master_tbl_tbody{
    width: 100%;
    height: 150px;
    margin-bottom: 2px;
    float: left;
    overflow: auto;
        
}
.main_inner_class_track .bootstrap-select{border: solid 1px #ccc;}
.table_staff_scr_scr thead{ display: inline-block;width: 100%;}
.table_staff_scr_scr tbody{ display: inline-block;width: 100%;max-height:  390px;overflow: auto   }
.table_staff_scr_scr tr{ display: inline-block;width: 100%;}
.table_staff_scr_scr td{ width: 100px;}
.table_staff_scr_scr th{ width: 100px;}
.pagination_total_showing{float: left;width: auto;padding-top: 12px;padding-left: 10px;color: #000000;}
.add-work-done-poppup-textbox-box label{font-weight:lighter;}.inner-textbox-cc input:focus ~ label, input:valid ~ label{font-size:13px;top: -10px;}.group{margin-bottom: 14px}.add-work-done-poppup{height: auto;} div.dataTables_wrapper div.dataTables_filter{float: right;top: 4px;position: relative;}.dataTables_length{top: 7px;position: relative;float: left}
    .dataTables_scrollHeadInner{width: 100% !important}.dataTables_scrollHeadInner table{width: 100% !important}.dataTables_scrollBody table{width: 100% !important} .dataTables_scrollBody {  height: 350px;}
    .onoffswitch{width: 70px;}.onoffswitch-switch{right: 40px;}
</style>
<script src="{{asset('public/commonfiles/pagination.js') }}" type="text/javascript"></script>
<script src="{{asset('public/assets/script/common.js') }}" type="text/javascript"></script>
<div class="col-sm-12">
    
        <div class="col-sm-12">
                <ol class="breadcrumb">
						<li>
							<a href="{{ url('index') }}">Dashboard</a>
						</li>
						
						<li class="active ms-hover">
							Vendors
						</li>
					</ol>
				</div>
        <div class="card-box table-responsive" style="padding: 8px 10px;">
             
              <div class="box master-add-field-box" >
             <div class="col-md-6 no-pad-left">
                <h3>Manage Vendor</h3>Total(<span id="searchcount" ></span>/<span id="count"></span>)Vendors
                
            </div>    
                  
            <div class="col-md-1 no-pad-left pull-right">
                <div class="table-filter" style="margin-top: 4px;">
                  <div class="table-filter-cc">
                    <a href="restaurant_details"> <button type="submit" style="margin-left:0" class="on-default followups-popup-btn btn btn-primary" >Add New</button></a>
                </div>

                 </div>
            </div>
               <div class=" pull-right">
                <div class="table-filter" style="margin-top: 4px;">
                  <div class="table-filter-cc">
                    <a title="Filter" href="#" onclick="filter_view()"> <button type="submit"  style="margin-right: 10px;" class="on-default followups-popup-btn btn btn-primary filter_sec_btn" ><i class="fa fa-filter" aria-hidden="true"></i></button></a>
                </div>
                   
                 </div>
            </div>
                  
            </div>
            <div class="filter_box_section_cc diply_tgl" style='display:block'>
<!--                <div class="filter_box_section">FILTER</div>-->
                   <div class="filter_text_box_row">
                       {!! Form::open(['url'=>'filter/restaurant', 'name'=>'frm_filter', 'id'=>'frm_filter','method'=>'get']) !!}
                       <input type="hidden" id="staff_id" name="staff_id" value="{{ Session::get('staffid')}}"/>
                       <div class="main_inner_class_track" style="width: 25%;">
                          <div class="group">
                             <div style="position: relative">
                                  <label>Vendor Name</label>
                                  <input id="restaurant_name" onkeyup="return refresh_filter()" name="restaurant_name" class="form-control" type="text">
                              </div>
                           </div>
                        </div><div class="main_inner_class_track" style="width: 25%;">
                          <div class="group">
                             <div style="position: relative">
							  <label>Vendor Type</label>
									 <select id="flt_category" name="flt_category" class="form-control" onchange="return refresh_filter();">
									<option value='' >Select Category</option>
									 @if(count($business_cat)!=0)
									@foreach($business_cat as $cat)
										<option value='{{$cat->id}}' >{{$cat->category}}</option>
									@endforeach
									@endif
									
								</select>
                                 </div>
                           </div>
                        </div>
                       <div class="main_inner_class_track" style="width: 10%;display:none" id='diet_section'>
                          <div class="group">
                             <div style="position: relative">
                                  <label>Diet</label>
                                 <select id="diet" name ="diet" class="form-control" onchange="return refresh_filter();">
                                     <option value="">All</option>
                                     <option value="N">Non Veg</option>
                                     <option value="Y">Veg</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                       <div class="main_inner_class_track" style="width: 25%;">
                          <div class="group">
                             <div style="position: relative">
                                  <label>Phone</label>
                                  <input id="phone" name="phone"  onkeyup="return refresh_filter()" class="form-control" type="text">
                              </div>
                           </div>
                        </div>
						<div class="main_inner_class_track" style="width: 10%">
                          <div class="group">
                             <div style="position: relative">
                                  <label>Show Entries</label>
                                 <select id="limit_value" name ="limit_value" class="form-control" onchange="return refresh_filter();">
                                     <option value="20">20</option>
                                     <option value="25">25</option>
                                     <option value="50">50</option>
                                     <option value="100">100</option>
                                     <option value="500">500</option>
                                 </select>
                              </div>
                           </div>
                        </div>
                       {{ Form::close() }}

                   </div>  
            </div>
            
            <div class="table_section_scroll">  
            <table id="example1" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th style="min-width:3px">Slno</th>
                    <th style="min-width:100px">Vendor Name</th>
                    <th style="width:80px">Dis Ordr</th>
        <!--        <th style="min-width:50px">Point Of Contact</th>  -->
                    <th style="min-width:80px">Mobile</th>
                    <th style="min-width:10px">Star</th>
              <!--  <th style="min-width:7px">Extra Rate %</th>  -->
        <!--        <th style="min-width:20px">Min Cart Val</th>  -->
                    <th style="min-width:5px">Diet</th>
                    <th style="min-width:5px">Busy</th>
         <!--       <th style="min-width:25px">Landline</th>   -->
                    <th style="min-width:10px"></th>
                </tr>
                </thead>
                
                <tbody id='append_tbody'>
               
                </tbody>
                


            </table>
                
           </div>
              <input type="hidden" id="start_count"  />
            <input type="hidden" id="current_count" value="{{ app('request')->input('page') }}"  />
            <input type="hidden" id="end_count"  />
             <div class="pagination_container_sec">
                    <ul class="pagination" id="pagination">
                        <li class="paginate_button previous disabled" id="pagn_prev" ><a href="#">Previous</a></li>
                        <li class="paginate_button" id="pagn_start" ><a href="#">1</a></li>
                        <li class="paginate_button " id="pagn_midle" ><a href="#">2</a></li>
                        <li class="paginate_button " id="pagn_end" ><a href="#">3</a></li>
                        <li class="paginate_button next " id="pagn_next" ><a href="#">Next</a></li>
                     </ul>
                 <div class="table-filter-cc" style='display:none'>
                     <input type="text" id="goto_page" name="goto_page" onkeyup="remove_warning()" onkeypress="return numonly(event);"  />
                               <a  onclick="goto_specific_page()" style="margin-left:0;width: 80px " class="on-default followups-popup-btn btn btn-primary">Go To</a>
                            </div>
             </div>
        </div>
    </div>
  </div>
   <div id="rest_auth_sec" class="add-work-done-poppup-cc" style="display: none;">
        <div class="add-work-done-poppup">
            <div class="add-work-done-poppup-head">Login Details
                <a href="#" onclick="close_aut_log()"><div class="close-pop-ad-work-cc ad-work-close-btn"><img src="{{asset ('public/assets/images/black_cross.png') }}"></div></a>
            </div>

                <div style="text-align:center;" id="branchtimezone"></div>

            <div class="add-work-done-poppup-contant" >
              
                <div class="add-work-done-poppup-textbox-cc">
                    <div class="add-work-done-poppup-textbox-box">
                       
                        <div class="main_container_track_order_list inner-textbox-cc" style="margin-top:10px;margin-bottom:0">
                                          
                                            <input type='hidden' id='url' value='{{$url}}' />
                                             <input type='hidden' id='restid' name="restid" />
                            <div class="main_inner_class_track ">
                                <div class="group">
                                    <div style="position: relative">
                                        <label>Login Name</label>
                                       {!! Form::text('rest_name',null, ['class'=>'form-control','id'=>'rest_name','name'=>'rest_name','onkeypress' => 'return charonly(event);','required','style'=>"background-color:transparent;"]) !!}
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="main_inner_class_track ">
                                <div class="group">
                                    <div style="position: relative">
                                        <label>Password</label>
                                          <input style="padding-right:35px;" class="form-control" id="rest_pasw" name="rest_pasw" type="password">

                                         <div class="ion-ios7-eye pass_show" onmouseover="mouseoverPass();" onmouseout="mouseoutPass();" />
                                    </div>
                                </div>
                            </div>
                                    <div class="main_inner_class_track" style="width:20%">
                                    	 <b><p class="" style="color: #000;float:right;cursor:pointer;display: none;background-color: burlywood;margin-top: 6px;padding: 2px 13px;line-height: 30px;" id="getimage" data-toggle="modal"  data-target="#myModal" data-title=""><a style="color: #000;">View image</a></p></b>
                                       
                                    </div>
                        
                             <div class="box-footer">
                                 <input type="hidden" name="type" id="type" />
                               <a id="updating" name="updating"  class="staff-add-pop-btn" onclick="update_auth();" style="height:40px; bottom: 20px;">Update</a>
                              </div>
                        </div>
                         
                            
                        </div>
                    </div>
                </div><!--add-work-done-poppup-textbox-cc-->
            </div>
            </div>
            </div>
            <div class="add-work-list-cc">
                <!--<div class="add-work-list-head">LIST</div>-->
                
              
        </div><!--add-work-done-poppup-->
        
  
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->

    
 
    <div class="permission_popup_sec" id="vendor_privilege" style='display:none'>
            <div class="permission_popup">
			<input type='hidden' id='vendor_id' name='vendor_id' >
                <div class="permission_popup_head">Permission</div>
                <div class="permission_popup_contant">
                        <table id="permission_content">
                            
                        </table>
                </div>
                <div class="permission_pop_footer">
                     <a href="#" onclick='close_privilage()'><div class="permission_pop_save_btn">CLOSE</div></a>
                </div>
            </div>
    </div>
     
    



    <style>#datatable-fixed-col_filter{display:none}table.dataTable thead th{white-space:nowrap;padding-right: 20px;}
        .on-default	{margin-left:10px;}div.dataTables_info {padding-top:13px;}
        .height_align{
                    margin-top: 12px;
        }
    </style>
     <link href="{{ asset('public/assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css">
        <script src="{{ asset('public/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
        <script src="{{ asset('public/assets/pages/jquery.sweet-alert.init.js') }}"></script>
         <style>#datatable-fixed-col_filter{display:none}table.dataTable thead th{white-space:nowrap;padding-right: 20px;}
        .on-default	{margin-left:10px;}div.dataTables_info {padding-top:13px;}
    </style>
    
@section('jquery')

 
    <script type="text/javascript">
    $(document).ready(function()
    {
           var t = $('#example1').DataTable({
                scrollX: false,
                scrollCollapse: false,
                "searching": false,
                "ordering": false,
                "info": false,
               "bPaginate": false,
                columnDefs: [
                    { width: '20%', targets: 0 }
                ],
                "deferLoading": 0,
                "lengthChange": false,
                "columnDefs": [{
                    paging: false
                } ],
            } );
        });

    $('.filter_sec_btn').on('click', function(e)
    {
        $('.filter_box_section_cc').toggleClass("diply_tgl");
        $("#restaurant_name").focus();
    });
</script>
    <script>
        $(document).ready(function () {
            $('input').attr('autocomplete', 'false');
        });
    </script>
<script>
function mouseoverPass(obj) {
  var obj = document.getElementById('rest_pasw');
  obj.type = "text";
}
function mouseoutPass(obj) {
  var obj = document.getElementById('rest_pasw');
  obj.type = "password";
}
</script>
<script>
$(document).ready(function() {
        
        var current_count = $('#current_count').val();
        if(current_count==''){
            current_count = 1;
        }
         var start_count = current_count-1;
        if(current_count==1) {
            start_count=1;
        }
        var end_count = current_count+1;
            $("#current_count").val(current_count);
            $("#start_count").val(start_count);
            $("#end_count").val(end_count);
			
            if(localStorage.restaurant_name){
                    $("#restaurant_name").val( localStorage.restaurant_name);
                }
            if(localStorage.flt_prfid){
                    $("#flt_prfid").val( localStorage.flt_prfid);
                }
                if(localStorage.flt_category){
                    $("#flt_category").val( localStorage.flt_category);
                }
                if(localStorage.diet){
                    $("#diet").val( localStorage.diet);
                }
                if(localStorage.phone){
                    $("#phone").val( localStorage.phone);
                }
                
            filter_change();
   });
 function refresh_filter() {
                $("#current_count").val(1);
                $("#start_count").val(1);
                $("#end_count").val(1);
                
                filter_change();
    }
function filter_change(val)
{
      var flt_category_name =  $( "#flt_category option:selected" ).text();
	  if(flt_category_name=='restaurants'){
		  $("#diet_section").show();
		  
	  } else{
		  $("#diet_section").hide();
		  $("#diet").val('');
	  }
	  var restaurant_name = $("#restaurant_name").val();
	  var flt_category    = $("#flt_category").val();
	  var diet    = $("#diet").val();
	  var phone    = $("#phone").val();
	  var start_cnt = $("#start_count").val();
	  var staff_id = $("#staff_id").val();
      var current_cnt = $("#current_count").val();    
      var limit_value = $("#limit_value").val();    
	  var filter_data={'limit_value':limit_value,'staff_id':staff_id,'restaurant_name':restaurant_name,'flt_category':flt_category,'diet':diet,'phone':phone,"current_count":current_cnt};
      $.ajax({
          method: "post",
          url   : "api/filter/restaurant",
          data  : filter_data,
          success : function(result)
          {
			  
            $("#append_tbody").html(result['fltr_data']);
             pagination_link(result); 
			  localStorage.clear();
              localStorage.restaurant_name      = restaurant_name;
              localStorage.flt_category         = flt_category;
              localStorage.diet_section         = diet_section;
              localStorage.phone                = phone;
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              $("#urls").text(jqXHR.responseText); //@text = response error, it is will be errors: 324, 500, 404 or anythings else
          }
      });
      return true;
  }
function close_aut_log()
{
     $("#rest_auth_sec").css("display","none");
}
function update_auth() {
    var restname = $("#rest_name").val();
    var restpasw = $("#rest_pasw").val();
    var restid   = $("#restid").val();
    if(restname==''){
          swal({
							
                title: "",
                text: "Please Enter Name",
                timer: 2000,
                showConfirmButton: false
            });
    }
    else if(restpasw==''){
          swal({
							
                title: "",
                text: "Please Enter Password",
                timer: 2000,
                showConfirmButton: false
            });
    }
    else {
                 $.ajax({
                        method: "post",
                        url: "api/update_rest_auth",
                        data:{"restname":restname,"restpasw":restpasw,"restid":restid},
                        success: function (result)
                        {
                          if(result=='insert'){
                              
                            swal({

                                  title: "",
                                  text: "Login Added Succesfully",
                                  timer: 4000,
                                  showConfirmButton: false
                              });
                              location.reload();
                      }
                      else{
                            swal({

                                  title: "",
                                  text: "Login Updated Succesfully",
                                  timer: 4000,
                                  showConfirmButton: false
                              });
                              location.reload();
                      }  
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
        //                    alert(errorThrown);
                            $("#errbox").text(jqxhr.responseText);
                        }
                    });

            }
    }
 function viewlink(id,link)
    {
        if(link == 'menu')
        {
            window.location.href="menu/list/"+id;
        }
        else if(link == 'about')
        {
            window.location.href="restaurant_edit/"+id;
        }
        else if(link == 'review')
        {
            window.location.href="menu/review/"+id;
        }
        else if(link == 'tax')
        {
            window.location.href="menu/tax/"+id;
        }
         else if(link == 'offer')
        {
            window.location.href="restaurant/offer/"+id;
        }
         else if(link == 'login')
        {
            $("#rest_auth_sec").css("display","block");
            $("#restid").val(id);
            $.ajax({
                method: "get",
                url: "api/login_restaurant/"+id,
                success: function (result)
                {
                    $("#rest_name").val(result['name']);
                    $("#rest_pasw").val(result['password']);
                },
                error: function (jqXHR, textStatus, errorThrown) {
//                    alert(errorThrown);
                    $("#errbox").text(jqxhr.responseText);
                }
            });
        } else if(link=='privilege'){
			$("#vendor_privilege").show();
			$("#vendor_id").val(id);
			$("#permission_content").html('');
			 $.ajax({
                method: "get",
                url: "api/vendor_privileges/"+id,
                success: function (result)
                {
                    $("#permission_content").html(result);
                },
                error: function (jqXHR, textStatus, errorThrown) {
//                    alert(errorThrown);
                    $("#errbox").text(jqxhr.responseText);
                }
            });
		}
        return true;
    }
	function update_privilege(userid,moduleid){
		var status = $("#module_"+moduleid).prop('checked');
		 $.ajax({
                method: "post",
                url: "api/update_privilege",
				data:{'userid':userid,'moduleid':moduleid,'status':status},
                success: function (result)
                {
                },
                error: function (jqXHR, textStatus, errorThrown) {
//                    alert(errorThrown);
                    $("#errbox").text(jqxhr.responseText);
                }
            });
	}
    function close_privilage(){
		$("#vendor_privilege").hide();
		$("#permission_content").html('');
		$("#vendor_id").val('');
	}
     function statuschange(id) {
            var ids = id;
            var data = {"ids": ids};
            $.ajax({
                method: "get",
                url: "restaurant_status",
                data: data,
                cache: false,
                crossDomain: true,
                async: false,
                dataType: 'text',
                success: function (result)
                {
//                    alert (result);
//                    location.reload();
                },
                error: function (jqXHR, textStatus, errorThrown) {
//                    alert(errorThrown);
                    $("#errbox").text(jqxhr.responseText);
                }
            });
        }
        
  function changeorderno(id,val)
        {
            $.ajax({
                method: "get",
                url: "api/rest_order/" + id+ "/"+val,
                cache: false,
                crossDomain: true,
                async: false,
                dataType: 'text',
                success: function (result)
                {
                    var json_x = JSON.parse(result);
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    $("#urls").text(jqXHR.responseText); //@text = response error, it is will be errors: 324, 500, 404 or anythings else
                }
            });
            return true;
        }      
        
</script>

@stop



   

@endsection




