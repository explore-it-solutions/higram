$.ajaxSetup({
    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});

function pagination_link(result){
    
    var start_cnt = $("#start_count").val();
              var current_cnt = $("#current_count").val();
              var end_cnt = $("#end_count").val();
              var s='';
              var m ='';
              var e='';
              var prev='p';
              var next="n";
              var cust_count = result['count'];
              var cust_count = result['count'];
              $("#count").html(cust_count);
              $("#searchcount").html('&nbsp;<b>'+result['searchcount']+'</b>');
                if(result['data_count']==0)
                  {
                      end_cnt=1;
                  }
                  else
                  {
                     end_cnt =  result['data_count'];
                  }
                 $("#end_count").val(end_cnt);
                  if(current_cnt=='')
                  {
                      current_cnt=1;
                  }
                  if(start_cnt==''){
                      start_cnt=1;
                  }
				  if(start_cnt=='' || start_cnt<1){
                      start_cnt=1;
                  }
              $(".paginate_button").removeClass("active");
              $(".paginate_button").removeClass("disabled");
              var page_endlimit = "";
              var page_startlimit = "";
               
              if(current_cnt == start_cnt && end_cnt==1) {
                   $("#pagination").html(' <li class="paginate_button previous disabled" id="pagn_1" ><a href="#">Previous</a></li>'+
                        '<li class="paginate_button active" id="pagn_2" onclick="search_filter(1)"><a href="#">1</a></li>'+
                        '<li class="paginate_button disabled" id="pagn_3" ><a href="#">2</a></li>'+
                        '<li class="paginate_button disabled" id="pagn_4" ><a href="#">3</a></li>'+
                        '<li class="paginate_button next disabled" id="pagn_5" ><a href="#">Next</a></li>');
              } 
             else if(current_cnt == start_cnt && end_cnt==2) {
                   $("#pagination").html(' <li class="paginate_button previous disabled" id="pagn_1" ><a href="#">Previous</a></li>'+
                        '<li class="paginate_button active" id="pagn_2" onclick="search_filter(1)"><a href="#">1</a></li>'+
                        '<li class="paginate_button " id="pagn_3" onclick="search_filter(2)"><a href="#">2</a></li>'+
                        '<li class="paginate_button disabled" id="pagn_4" ><a href="#">3</a></li>'+
                        '<li class="paginate_button next " id="pagn_5" onclick="search_filter_btn(2)"><a href="#">Next</a></li>');
              } 
             else if(current_cnt == 2 && end_cnt==2) {
                   $("#pagination").html(' <li class="paginate_button previous " id="pagn_1" onclick="search_filter_btn(1)" ><a href="#">Previous</a></li>'+
                        '<li class="paginate_button " id="pagn_2" onclick="search_filter(1)"><a href="#">1</a></li>'+
                        '<li class="paginate_button active" id="pagn_3" onclick="search_filter(2)"><a href="#">2</a></li>'+
                        '<li class="paginate_button disabled" id="pagn_4" ><a href="#">3</a></li>'+
                        '<li class="paginate_button next disabled" id="pagn_5" ><a href="#">Next</a></li>');
              } 
              else if(current_cnt == start_cnt){
                  page_startlimit=""; 
                  
                  if(end_cnt!=3) {
                      page_startlimit ='<li class="paginate_button " id="pagn_4" onclick="search_filter(3)"><a href="#">3</a></li>';
                              if(end_cnt!=e+1) {
                                     page_endlimit ='<li class="paginate_button" id="pagn_4" ><a >...</a></li>';
                            }
                  }
                  
                  $("#pagination").html(' <li class="paginate_button previous disabled" id="pagn_1"  ><a href="#">Previous</a></li>'+
                        '<li class="paginate_button active" id="pagn_2" onclick="search_filter(1)" ><a href="#">1</a></li>'+
                        '<li class="paginate_button " id="pagn_3" onclick="search_filter(2)"><a href="#">2</a></li>'+page_startlimit+
                        '<li class="paginate_button " id="pagn_4" onclick="search_filter('+end_cnt+')"><a href="#">'+end_cnt+'</a></li>'+
                        '<li class="paginate_button next " id="pagn_5" onclick="search_filter_btn(2)"><a href="#">Next</a></li>');
              } else if(current_cnt == end_cnt)
              {
                  
                        s = parseInt(current_cnt)-2;
                        m=parseInt(current_cnt)-1;
                        e=current_cnt;
                        if(start_cnt!=s) {
                            page_startlimit = '<li class="paginate_button" id="pagn_2" onclick="search_filter(1)" ><a href="#">1</a></li>';
                            if(start_cnt+1!=s) {
                            page_startlimit +='<li class="paginate_button" id="pagn_4" ><a >...</a></li>';
                            }
                        }
                        
                   $("#pagination").html(' <li class="paginate_button previous " id="pagn_1" onclick="search_filter_btn(1)" ><a href="#">Previous</a></li>'+page_startlimit+
                        '<li class="paginate_button" id="pagn_2" onclick="search_filter('+s+')"><a href="#">'+s+'</a></li>'+
                        '<li class="paginate_button " id="pagn_3" onclick="search_filter('+m+')"><a href="#">'+m+'</a></li>'+
                        '<li class="paginate_button active" id="pagn_4" onclick="search_filter('+e+')"><a href="#">'+e+'</a></li>'+
                        '<li class="paginate_button next disabled" id="pagn_5" ><a href="#">Next</a></li>');
              } 
               else{
                        s = parseInt(current_cnt)-1;
                        m=parseInt(current_cnt);
                        e=parseInt(current_cnt)+1;
                       
                        var slimit = parseInt(start_cnt+1);
                        if(start_cnt!=s) {
                            page_startlimit = '<li class="paginate_button " id="pagn_2" onclick="search_filter(1)" ><a href="#">1</a></li>';
                            if(start_cnt!=s-1) {
                              page_startlimit +='<li class="paginate_button" id="pagn_4" ><a >...</a></li>';
                            }
                        }
                        
                        if(end_cnt!=e) {
                             if(end_cnt!=e+1) {
                                     page_endlimit ='<li class="paginate_button" id="pagn_4" ><a >...</a></li>';
                            }
                            page_endlimit += '<li class="paginate_button " id="pagn_4" onclick="search_filter('+end_cnt+')"><a href="#">'+end_cnt+'</a></li>';
                        }
                    $("#pagination").html(' <li class="paginate_button previous " id="pagn_1" onclick="search_filter_btn(1)"><a href="#">Previous</a></li>'+page_startlimit+
                        '<li class="paginate_button" id="pagn_2" onclick="search_filter('+s+')"><a href="#">'+s+'</a></li>'+
                        '<li class="paginate_button active" id="pagn_3" onclick="search_filter('+m+')"><a href="#">'+m+'</a></li>'+
                        '<li class="paginate_button " id="pagn_4" onclick="search_filter('+e+')"><a href="#">'+e+'</a></li>'+page_endlimit+
                        '<li class="paginate_button next " id="pagn_5" onclick="search_filter_btn(2)"><a href="#">Next</a></li>');
              }
}

function search_filter(cv){
                   $("#current_count").val(cv);
             
              filter_change();
            }
function search_filter_btn(cv){
    var str = $("#start_count").val();
    var crnt = $("#current_count").val();
    var end = $("#end_count").val();
    var new_crn = 0;
    if(cv==1){
        new_crn = parseInt(crnt)-1;
        var strt_crn = parseInt(new_crn)-1;
        $("#current_count").val(new_crn);
        $("#start_count").val(strt_crn);
    }
    else if(cv==2){
        new_crn = parseInt(crnt)+1;
        $("#current_count").val(new_crn);
    }
              
   filter_change();
}