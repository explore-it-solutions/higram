<?php

namespace App\Providers;

use App\GeneralSetting;
use App\MenuPortion;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;
use Helpers\Datasource;
use Helpers\Commonsource;
use Response;
use DB;
use App\Designation;
use App\City;
use App\Country;
use Session;
use App\UserMaster;
class ContentServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadheader();
        $this->loaddecimal();
        $this->siteurl();
        $this->loaddesignationdetails();
        $this->loadcity();
        $this->loadcountry();
        $this->loadcurrency();
        $this->loadmenuportion();
        $this->staffpermission();
        $this->superpermission();
        $this->notificationgroups();
        $this->googleapikey();
        $this->paymode();
        $this->module_list();
    }
    private function loadheader()
    {
        view()->composer(['*'], function($view)
        {
            $url = Datasource::geturl();

            $view->with('url', $url);
        });
    }
    private function googleapikey()
    {
        view()->composer(['*'], function($view)
        {
            $googlekey = Commonsource::googleapikey();

            $view->with('googlekey', $googlekey);
        });
    }
    private function paymode()
    {
        view()->composer(['*'], function($view)
        {
            $payment_method = DB::SELECT("select name FROM payment_methods WHERE active='Y'");
            $view->with('paymode', $payment_method);
        });
    }
    private function siteurl()
    {
        view()->composer(['*'], function($view)
        {
            $siteUrl = Datasource::getsiteurl();
            $view->with('siteUrl', $siteUrl);
        });
    }
   private function loaddecimal()
    {
        view()->composer(['*'], function($view)
        {
            $general= GeneralSetting::where('id','1')->select('decimal_digit')->first();
            $view->with('decimal_digit', $general['decimal_digit']);
        });
    }
     private function loaddesignationdetails() {
        view()->composer(['*'], function($view) {
			 $user_type  =Session::get('logingroup');
			 if($user_type=='V') {
				 $designationlist = Designation::select('designation', 'designation')
                            ->where('active','=','Y')->whereIn('designation',['Delivery staff'])
                            ->pluck('designation', 'designation')->all();
			 } else{
				$designationlist = Designation::select('designation', 'designation')
                            ->where('active','=','Y')
                            ->pluck('designation', 'designation')->all();
			 }
            $view->with('designationlist', $designationlist);
        });
    }
    private function loadcity() {
        view()->composer(['*'], function($view) {
            $citylist = City::select('name', 'id')
                ->where('active','Y')
                ->pluck('name', 'id')
                ->all();
            $view->with('citylist', $citylist);
        });
    }
    private function loadmenuportion()
    {
        view()->composer(['menu.menu_add','menu.menu_edit'], function($view)
        {
            $portion = MenuPortion::where('mp_status','Y')
                ->select('mp_portion')
                ->pluck('mp_portion', 'mp_portion')
                ->all();
            $view->with('portion', $portion);
        });
    }
    private function loadcountry() {
        view()->composer(['*'], function($view) {
            $countrylist = DB::SELECT("SELECT name->>'$.country' as country FROM country");
			$i=0;
			$list = array();
			foreach($countrylist as $list) {
				$country[$i] = $list->country;
				$i++;
			}
            $view->with('countrylist', $country);
     
        });
    }
    private function loadcurrency()
    {
        view()->composer(['*'], function($view) {
            $currencylist = DB::SELECT("SELECT name->>'$.currency ' as currency FROM country");
            $i=0;
            $currency_list = array();
            foreach($currencylist as $currency_list) {
                $currency[$i] = $currency_list->currency;
                $i++;
            }
            $view->with('currencylist', $currency);
        });
    }
     private function notificationgroups()
     {
           view()->composer(['*'],function($view)
           {
             $result = DB::SELECT("SELECT g_name,g_id from `notification_group` where g_active = 'Y'");
             $view->with('group',$result);
           });
     }
    private function staffpermission()
    {
        view()->composer(['*'], function($view)
        {
            $staffid = Session::get('staffid');
            $logingroup = Session::get('logingroup');
            if($logingroup=='H'){
                $permission = DB::SELECT("SELECT modules->>'$.RestauranrReports' as RestauranrReports,JSON_UNQUOTE(modules->'$.banner') as banner,JSON_UNQUOTE(modules->'$.restaurant') as restaurant,JSON_UNQUOTE(modules->'$.staff') as staff,JSON_UNQUOTE(modules->'$.offers') as offers,JSON_UNQUOTE(modules->'$.orders') as orders,JSON_UNQUOTE(modules->'$.reports') as reports,JSON_UNQUOTE(modules->'$.customer') as customer,JSON_UNQUOTE(modules->'$.designation') as designation,JSON_UNQUOTE(modules->'$.staff_report') as staff_report,JSON_UNQUOTE(modules->'$.general_report') as general_report,JSON_UNQUOTE(modules->'$.send_notification') as send_notification,JSON_UNQUOTE(modules->'$.notification_group') as notification_group,modules->>'$.order_history' as order_history from  users a LEFT JOIN internal_staffs b on a.id=b.id where a.restaurant_id= '".$staffid."'");
            }
            else{
//                $permission = DB::SELECT("select module_name,sub_module,active from module_master LEFT JOIN users_modules ON module_master.m_id=users_modules.module_id where user_id='$staffid'");
                 $permission = DB::SELECT("SELECT modules->>'$.RestauranrReports' as RestauranrReports,JSON_UNQUOTE(modules->'$.banner') as banner,JSON_UNQUOTE(modules->'$.restaurant') as restaurant,JSON_UNQUOTE(modules->'$.staff') as staff,JSON_UNQUOTE(modules->'$.offers') as offers,JSON_UNQUOTE(modules->'$.orders') as orders,JSON_UNQUOTE(modules->'$.reports') as reports,JSON_UNQUOTE(modules->'$.customer') as customer,JSON_UNQUOTE(modules->'$.designation') as designation,JSON_UNQUOTE(modules->'$.staff_report') as staff_report,JSON_UNQUOTE(modules->'$.general_report') as general_report,JSON_UNQUOTE(modules->'$.send_notification') as send_notification,JSON_UNQUOTE(modules->'$.notification_group') as notification_group,modules->>'$.order_history' as order_history from  users a LEFT JOIN internal_staffs b on a.id=b.id where a.staffid= '".$staffid."'");

            }
            $view->with('permission',$permission);
        });
    }
     private function superpermission()
    {
        view()->composer(['*'], function($view)
        {
            $staffid = Session::get('staffid');
            $superadmin = DB::SELECT("SELECT login_group,staffid from users");
            $view->with('superadmin',$superadmin);
        });
    }
    
     private function module_list()
    {
         view()->composer(['*'], function($view)
        {
			 $mainlink = array();$m=0;$test="";$user_type="";
            $staffid = Session::get('staffid');
            $setuserid = Session::get('setuserid');
 //           $module = DB::SELECT("SELECT DISTINCT(mm.module_name),(select `m_id` from module_master  mms where mms.module_name = mm.module_name limit 0,1) as mid,(select page_link from module_master mst where mm.module_name =  mst.module_name limit 0,1) as page_link,(select count(`sub_module`) from module_master ms where ms.module_name = mm.module_name) as count from module_master mm LEFT JOIN users_modules um ON mm.m_id=um.module_id where mm.module_for='C' and um.user_id = '$setuserid' and um.active='Y' order by mid asc");
             //$module = DB::SELECT("SELECT DISTINCT(mm.module_name),mm.sub_module,mm.display_order,(select page_link from module_master mst where mm.module_name =  mst.module_name limit 0,1) as page_link,(select count(`sub_module`) from module_master ms where ms.module_name = mm.module_name) as count from module_master mm LEFT JOIN users_modules um ON mm.m_id=um.module_id where mm.module_for='C' and um.user_id = '$setuserid' and um.active='Y' order by display_order asc");
            //$modulesublist = DB::SELECT("SELECT mm.module_name,mm.m_id,mm.sub_module,mm.page_link from module_master mm LEFT JOIN users_modules um ON mm.m_id=um.module_id where mm.module_for='C' and um.user_id = '$setuserid' and um.active='Y' AND mm.module_name=''");
  
		    $user_type  =Session::get('logingroup');
		   if($user_type){
		            			//$test ="SELECT a.module_id,a.active,b.* FROM users_modules a LEFT JOIN module_master b on a.module_id=b.m_id WHERE a.user_id=$setuserid AND b.module_for IN('".$user_type."','G') AND a.active='Y' AND b.active='Y' ORDER BY display_order ASC";

		   $permitted_pages = DB::SELECT("SELECT a.module_id,a.active,b.* FROM users_modules a LEFT JOIN module_master b on a.module_id=b.m_id WHERE a.user_id=$setuserid AND b.module_for LIKE '%".$user_type."%' AND a.active='Y' AND b.active='Y' ORDER BY display_order ASC");
                foreach($permitted_pages as $pages ){
                    $sublink_list = array();
                    if($pages->sub_link!=''){
                        $explode_sub = explode(',', $pages->sub_link);$i=0;
                       foreach($explode_sub as $sub){
                             $permitted_pages = DB::SELECT("SELECT a.module_id,a.active,b.* FROM users_modules a LEFT JOIN module_master b on a.module_id=b.m_id WHERE a.user_id=$setuserid AND b.m_id=$sub AND a.active='Y' AND b.active='Y' ");
                             if(count($permitted_pages)!=0){
                                 $sublink_list[$i]   = ['module_id'=>$permitted_pages[0]->module_id,
                                                     'active' => $permitted_pages[0]->active,
                                                     'm_id'=> $permitted_pages[0]->m_id,
                                                     'module_name'=>$permitted_pages[0]->module_name,
                                                     'sub_module'=> $permitted_pages[0]->sub_module,
                                                     'page_link'=>$permitted_pages[0]->page_link,
                                                     'sub_link'=>$permitted_pages[0]->sub_link,
                                                     'icon'=>$permitted_pages[0]->icon,
                                                     'module_for'=>$permitted_pages[0]->module_for,
                             ];
                            $i++;
                             }
                        }
                    }
                  $mainlink[$m]   = ['module_id'=>$pages->module_id,
                                                     'active' => $pages->active,
                                                     'm_id'=> $pages->m_id,
                                                     'module_name'=>$pages->module_name,
                                                     'sub_module'=> $pages->sub_module,
                                                     'page_link'=>$pages->page_link,
                                                     'sub_link'=>$sublink_list,
                                                     'icon'=>$pages->icon,
                                                     'module_for'=>$pages->module_for,
                          ];
                  $m++;
                }
                }
		   $view->with(['mainlink'=>$mainlink,'staffid'=>$staffid,'setuserid'=>$setuserid,'test'=>$test,'user_type'=>$user_type]);
        });
    }
  }
