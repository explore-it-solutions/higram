<?php
//Common functions


use Helpers\DataSource;


//Check Decimal

//      function timeadd($time)
//    {
//        $datetimeadd = $time->format('h:i A',strtotime($time . "+45 minutes"));
//        $datetimeadd = date('h:i A', strtotime($time . "+45 minutes"));
//        return $datetimeadd;
//    }

    function timediff($time)
    {
        $timezone = 'ASIA/KOLKATA';
        $totime = new DateTime('now', new DateTimeZone($timezone));
        $to_time = $totime->format('Y-m-d H:i:s');
        $fromtime = new DateTime(date('Y-m-d H:i:s' ,strtotime($time)), new DateTimeZone($timezone));
        $from_time = $fromtime->format('Y-m-d H:i:s');
        $t =  $fromtime->diff($totime);
        $hr = ($t->format('%a')*1440)+($t->format('%h')*60) +($t->format('%i'));
         if($hr >= 40)
         {
                return 'Y';
         }
        else
        {
                return 'N';
        }
    }

?>