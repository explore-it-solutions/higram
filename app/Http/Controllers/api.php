<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
   
});
    Route::group(['middleware' => ['api','cors']], function () {
     
   });

     



  //USER LOGIN SECTION
Route::get('emailcheck','UserLoginController@emailcheck');
Route::post('logintest','UserLoginController@logintest');

Route::post('add_designation', 'DesignationController@add_designation');//Add and Edit Designation
Route::post('add_staff', 'StaffController@add_staff');//Add and Edit Staff
Route::post('add_restaurant', 'RestaurantController@add_restaurant');//Add Restaurant
Route::post('filter/restaurant', 'RestaurantController@filter_restaurant');//Filter Restaurant
Route::post('filter/menu', 'MenuController@filter_menu');//Filter Restaurant
Route::post('menu/add', 'MenuController@submit_menu');//Add menu
Route::post('menu/edit', 'MenuController@menu_editsubmit');//Add menu
Route::post('category/add', 'MenuController@category_add');//Add menu
Route::post('subcategory/add', 'MenuController@subcategory_add');//Add menu
Route::get('category/{id}', 'MenuController@category');//List actegory per  Restaurant
Route::get('subcategory/{id}', 'MenuController@subcategory');//List actegory per  Restaurant
Route::post('edit_restaurant', 'RestaurantController@edit_restaurant');//Edit Restaurant
Route::post('openclose_time', 'RestaurantController@openclose_time');//Open Close Time Add and update
Route::post('get_taxvalue', 'MenuController@get_taxvalue');//get tax value for particular tax
Route::get('view_time', 'RestaurantController@view_time');//View Open Close Time in Restaurant
Route::get('delete_time', 'RestaurantController@delete_time');//Delete Open Close Time in Restaurant
//Route::get('rest_order/{id}/{val}','RestaurantController@rest_order');
Route::post('add_tax', 'TaxController@add_tax');//Add and Edit Tax
Route::post('menu/upload', 'MenuController@menu_upload');//get tax value for particular tax
Route::post('filter/review', 'ReviewController@filter_review');//Filter Review
Route::post('filter/customer_list', 'CustomerController@filter_customer_list');//Filter Customer List

//Banner add
Route::post('banner/add','BannerController@banner_submit');
Route::get('banner_delete/{id}','BannerController@banner_delete');
Route::get('banner_order/{id}/{val}','BannerController@banner_order');

// General Offers
Route::post('add_gen_offers', 'GeneralOfferController@add_gen_offers');//Add General Offers
Route::post('edit_gen_offers', 'GeneralOfferController@edit_gen_offers');//Edit General Offers
Route::post('filter/genoffer', 'GeneralOfferController@filter_genoffer');//Filter General Offers

//Restaurant
Route::post('restaurant/add_offer','RestaurantOfferController@restaurant_offer');


//API FOR FRONT END SERVICES
Route::get('restaurants/{id}/{category}','RestaurantController@restaurantlists'); //returns restaurants not busy nad open at the time in particular location
Route::get('mobile_reg/{no}','CustomerController@mobile_registration');
Route::get('verify_otp/{no}/{otp}','CustomerController@otp_verification');
Route::post('cutomer_reg/{frst}/{lst}/{eml}/{pswd}/{mbl}','CustomerController@customer_registration');
Route::get('customer_login/{phone}/{password}','CustomerController@customer_login');
Route::get('restaurant_offers','RestaurantOfferController@restaurant_offerslist');//returns the restaurant and general offer details
Route::get('bannerapp','BannerController@bannerapplist');//returns list of App Banner
Route::get('bannerweb','BannerController@bannerweblist'); //returns list of Web Banner
Route::get('restaurant_offers','RestaurantOfferController@restaurant_offerslist');
Route::get('popular/restaurants','RestaurantController@mostpopular_restaurants');
Route::get('restaurant/about/{id}','RestaurantController@about_restaurants');


 