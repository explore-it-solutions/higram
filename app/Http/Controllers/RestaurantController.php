<?php

namespace App\Http\Controllers;

use App\GeneralSetting;
use App\RestaurantOffer;
use Helpers\Commonsource;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use Image;
use Session;
use Response;
use DateTime;
use DateTimeZone;
use Illuminate\Support\Facades\Input;
use Helpers\Datasource;
use App\Group;
use App\Restaurant_Master;
class RestaurantController extends Controller
{
    //View Manage Restaurant Page
    public function view_restaurant(Request $request)
    {
          $staffid = Session::get('staffid');
          $filterarr = array();
          $details   = array();
		  $business_cat=DB::SELECT("SELECT id,URL_DECODE(category) as category,active FROM business_category WHERE 1");

       /* $details = DB::SELECT('SELECT name_tagline->>"$.name" as name,mobile->>"$.ind" as code,star_rating->>"$.value" as value,mobile->>"$.mobile" as mob,id,point_of_contact,star_rating,
          popular_display_order,busy,min_cart_value,extra_rate_percent,phone,pure_veg FROM `restaurant_master` WHERE `restaurant_master`.`id` != " " ORDER BY popular_display_order ASC');
       */
         /* $details = DB::SELECT('SELECT name_tagline->>"$.name" as name,mobile->>"$.ind" as code,star_rating->>"$.value" as value,mobile->>"$.mobile" as mob,id,point_of_contact,star_rating,
          popular_display_order,busy,min_cart_value,extra_rate_percent,phone,pure_veg from restaurant_master r where r.`id` != " " and r.city in (SELECT  a.area_id from users u, internal_staffs s, internal_staffs_area a where u.staffid =s.id and s.id = a.staff_id and a.staff_id = "'.$staffid .'")  ORDER BY popular_display_order ASC');
		  */
          return view('restaurant.manage_restaurant',compact('details','business_cat'));
    }

    public function view_restaurantdetails(Request $request)
    {
        $filterarr = array();
		$business_cat=DB::SELECT("SELECT id,URL_DECODE(category) as category,active,pre_scheduled FROM business_category WHERE 1");

        return view('restaurant.restaurant_details',compact('business_cat'));
    }

    // Group auto search
    public function groupautosearch(Request $request)
    {

        $searchterm = $request['searchterm'];
        $groups =Group::where('group_name', 'LIKE', '%'.$searchterm . '%')
            ->get();
        return $groups;
    }
    // Adding of Restaurants and Details
    public function add_restaurant(Request $request)
    {
        $logo = Input::file('logo');
        $banner = Input::file('banner');
        $url = Datasource::geturl();
        $timeDate = date("jmYhis") . rand(991, 9999);
        $date = date('Y-m-d');
        if($logo == "")
        {
            $image_url = null;
        }
        else
        {
            $uploadfile = $timeDate . '.' .strtolower($logo->getClientOriginalExtension());
            Image::make($logo)->resize(260, 200)->save(base_path() . '/uploads/logo/' . $uploadfile);
            $image_url = 'uploads/logo/' . $uploadfile;
        }
        if($banner == "")
        {
            $banner_url = null;
        }
        else
        {
            $uploadbanner = $timeDate . '.' .strtolower($banner->getClientOriginalExtension());
            Image::make($banner)->resize(250, 250)->save(base_path() . '/uploads/restaurant_banner/' . $uploadbanner);
            $banner_url = 'uploads/restaurant_banner/' . $uploadbanner;
        }

        if($request['phone']== '')
        {
            $phone = '0';
        }
        else
        {
            $phone = $request['phone'];
        }
        if($request['del_time']== '')
        {
            $del_time = '0';
        }
        else
        {
            $del_time = $request['del_time'];
        }
        if($request['cart_value']== '')
        {
            $cart_value = '0';
        }
        else
        {
            $cart_value = $request['cart_value'];
        }
        if($request['pre_deltime']== '')
        {
            $pre_deltime = '0';
        }
        else
        {
            $pre_deltime= $request['pre_deltime'];
        }
        if($request['extra_rate']== '')
        {
            $extra_rate = '0';
        }
        else
        {
            $extra_rate= $request['extra_rate'];
        }
        if($request['ind']== '')
        {
            $ind = '+91';
        }
        else
        {
            $ind= $request['ind'];
        }
        if($request['range']== '')
        {
            $range = '0';
        }
        else
        {
            $range= $request['range'];
        }
        if($request['del_charge']== '')
        {
            $del_charge = '0';
        }
        else
        {
            $del_charge= $request['del_charge'];
        }
        if($request['pack_charge']== '')
        {
            $pack_charge = '0';
        }
        else
        {
            $pack_charge= $request['pack_charge'];
        }
        $restaurant = DB::select("SELECT name_tagline->>'$.name' FROM restaurant_master where name_tagline->>'$.name' = '".$request['rname']."'");

        if(count($restaurant)>0)
        {
            $msg = 'already exist';
            return response::json(compact('msg'));
        }
        else
        {
            $dietsave = $request['dietsave'];
            if($dietsave == "false")
            {
                $diet = 'Y';
            }
            else
            {
                $diet = 'N';
            }

            $p_exclusivesave = $request['p_exclusive'];
            if($p_exclusivesave == "true")
            {
                $p_exclusive = 'Y';
            }
            else
            {
                $p_exclusive = 'N';
            }
            $latitude = str_replace(",","", $request['lat']);
            $longitude = str_replace(",","", $request['long']);
            $geocordinates = $latitude.','.$longitude;
			$delivery_type = $request['delivery_type'];
			$is_pickup = $request['is_pickup'];
			$del_befor_time = $request['del_befor_time'];
			$max_schedule_slot = $request['max_schedule_slot'];
			if($delivery_type=='delivery'){
				$is_presheduled='N';
			} else{
				$is_presheduled='Y';
			}
			if($is_pickup=='no_pickup'){
				$is_pickup='Y';
			} else{
				$is_pickup='N';
			}
            $group = DB::select("select * from restaurant_group where group_name =  '".$request['group']."'");
            $geo_location =  str_replace(array("'","\"","\\","/"),array("","","","_","_"), $request['geo_location']);
            if(count($group)==0)            
            {
                DB::INSERT("INSERT INTO `restaurant_group`(`group_name`)VALUES('".$request['group']."')");
                
			}
		//return "INSERT INTO `restaurant_master`(`group_id`, `name_tagline`, `pure_veg`,`p_exclusive`,`category`,`address`,`email`,`country`,`mobile`,`phone`,`point_of_contact`,`city`,`delivery_range_unit`,`min_delivery_time`,`min_cart_value`,`min_prepration_time`,`speical_message`,`cuisines`,`license_numbers`,`extra_rate_percent`,`google_location`,`logo`,`banner`,`registeration_date`,`delivery_charge`,`packing_charge`,`expensive_rating`,`geo_cordinates`,star_rating,is_scheduled,is_pickup,schedule_before_time,max_order_slot)
               //   VALUES((select id from restaurant_group where group_name = '".$request['group']."'),json_object('name','" . $request['rname'] . "','tag_line','" . $request['tagline'] . "','description','" . $request['description'] . "'),'$diet','$p_exclusive','" . $request['category'] . "','" . $request['address'] . "','" . $request['email'] . "',json_object('currency','" . $request['currency'] . "','country','" . $request['country'] . "'),json_object('ind','$ind','mobile','" . $request['mobile'] . "'),'$phone','" . $request['ptcontact'] . "','" . $request['city'] . "',json_object('unit','" . $request['unit'] . "','range','$range'),'$del_time','$cart_value','$pre_deltime','" . $request['message'] . "','" . $request['cuisine'] . "','" . $request['lic_cert'] . "','$extra_rate','" . $geo_location . "','$image_url','$banner_url','$date','$del_charge','$pack_charge','" . $request['exp_rating'] . "', '". $geocordinates ."',json_object('count','0','value','4'),'$is_presheduled','$is_pickup','$del_befor_time','$max_schedule_slot')";	
			DB::INSERT("INSERT INTO `restaurant_master`(`group_id`, `name_tagline`, `pure_veg`,`p_exclusive`,`category`,`address`,`email`,`country`,`mobile`,`phone`,`point_of_contact`,`city`,`delivery_range_unit`,`min_delivery_time`,`min_cart_value`,`min_prepration_time`,`speical_message`,`cuisines`,`license_numbers`,`extra_rate_percent`,`google_location`,`logo`,`banner`,`registeration_date`,`delivery_charge`,`packing_charge`,`expensive_rating`,`geo_cordinates`,star_rating,is_scheduled,is_pickup,schedule_before_time,max_order_slot)
                   VALUES((select id from restaurant_group where group_name = '".$request['group']."'),json_object('name','" . $request['rname'] . "','tag_line','" . $request['tagline'] . "','description','" . $request['description'] . "'),'$diet','$p_exclusive','" . $request['category'] . "','" . $request['address'] . "','" . $request['email'] . "',json_object('currency','" . $request['currency'] . "','country','" . $request['country'] . "'),json_object('ind','$ind','mobile','" . $request['mobile'] . "'),'$phone','" . $request['ptcontact'] . "','" . $request['city'] . "',json_object('unit','" . $request['unit'] . "','range','$range'),'$del_time','$cart_value','$pre_deltime','" . $request['message'] . "','" . $request['cuisine'] . "','" . $request['lic_cert'] . "','$extra_rate','" . $geo_location . "','$image_url','$banner_url','$date','$del_charge','$pack_charge','" . $request['exp_rating'] . "', '". $geocordinates ."',json_object('count','0','value','4'),'$is_presheduled','$is_pickup','$del_befor_time','$max_schedule_slot')");
            
            $msg = 'success';
            return response::json(compact('msg'));
        }
        return redirect('manage_restaurant');

    }

    //Filtering of Manage Restaurant
    public function filter_restaurant(Request $request)
    {
        $staffid = $request['staff_id'];
        $search = '';
        $restaurant_name = $request['restaurant_name'];
        $flt_category = $request['flt_category'];
        $diet = $request['diet'];
        $phone = $request['phone'];
        $user = $request['point_contact'];
		$search="";
		 $pointing        = $request['current_count'];//return $flt_subcategory;
		 $limit = $request['limit_value'];
            if($pointing=='')
            {
                $pointing=1;
            }
             $startlimit = ($pointing-1)*$limit;
             $endlimit = ($pointing)*$limit;
        if(isset($restaurant_name) && $restaurant_name != '')
        {
            $search.=" and  LOWER(name_tagline->>'$.name')   LIKE '%".strtolower($restaurant_name)."%'";
        }
		if($flt_category!=''){
			 $search.=" and  category ='$flt_category'";
		}
        if(isset($phone) && $phone != '')
        {
            $search.=" and  mobile->>'$.mobile'   LIKE '".$phone."%'";
        }
        if(isset($diet) && $diet != '')
        {
            $search.= " and pure_veg  = '".$diet."''";
        }
        if(isset($user) && $user != '')
        {
             $search.= " and point_of_contact  LIKE '%".$user."%''";
        }
		//return $search;
		$totaldetails = DB::SELECT("SELECT count(id) as total_list FROM  restaurant_master WHERE 1 ");
            $grant_total = $totaldetails[0]->total_list;
          $searchtotal_details = DB::SELECT('SELECT count(id) as total_list FROM `restaurant_master` WHERE `restaurant_master`.`id` != " " and restaurant_master.city in (SELECT  a.area_id from users u, internal_staffs s, internal_staffs_area a where u.staffid =s.id and s.id = a.staff_id and a.staff_id = "'.$staffid .'") '.$search.'  ORDER BY id');
    
		 $search_count = $searchtotal_details[0]->total_list;
             $info_res = round($search_count/$limit,0);
             $info_mode = ($search_count)%($limit);
              if( $info_mode!=0 ) {
                  $info_res=$info_res+1;
              }
            $data_count=$info_res;
            $append='';
            $slno=$startlimit;
		 $details = DB::SELECT('SELECT popular_display_order,name_tagline->>"$.name" as name,mobile->>"$.ind" as code,star_rating->>"$.value" as value,mobile->>"$.mobile" as mob,id,point_of_contact,star_rating,busy,id,
                               min_cart_value,extra_rate_percent,phone,pure_veg FROM `restaurant_master` WHERE `restaurant_master`.`id` != " " and restaurant_master.city in (SELECT  a.area_id from users u, internal_staffs s, internal_staffs_area a where u.staffid =s.id and s.id = a.staff_id and a.staff_id = "'.$staffid .'") '.$search.'  ORDER BY id LIMIT '.$startlimit.','.$limit);
       	if(count($details)!=0){
		foreach($details as $info){
			$slno++;
			$restaurantname = strtoupper($info->name);
                      $contact = strtoupper($info->point_of_contact);
                      $phoneno = $info->code.$info->mob;
                      $busy = $info->busy;
                      $id = $info->id;
                      $displayorder = $info->popular_display_order;
                      if($info->busy == 'Y')
                        {
                            $busy = 'checked';
                        }
                      if($info->pure_veg == 'Y')
                      {
                          $img = "public/assets/images/veg_ico.png";
                      }
                      else
                      {
                          $img = "public/assets/images/non_veg_ico.png";
                      }
			$append .= '<tr><td style="min-width:3px;">'.$slno.'</td><td style="min-width:160px;text-align: left;">'.$restaurantname.'</td>';
                          $append .= '<td style="width:70px;"><input style="width:70px;" class="form-control" type="textbox" onkeypress="return isNumberKey(event)" value='.$displayorder.' title="Edit Order" name="order_no" id="order_no" onkeyup="return changeorderno('.$id.',this.value)"></td>';
                          $append .= '<td style="min-width:80px;text-align: left;">'.$phoneno.'</td>';
                          $append .= '<td style="min-width:30px;text-align: center;">'.$info->value.'</td>';
                         $append .=  '<td style="min-width:10px;"><img width="15px" src="'.$img.'"></td>';
                          $append .= '<td><div class="status_chck'.$id.'"><div class="onoffswitch"> <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch'.$id.'"  '.$busy.'> <label class="onoffswitch-label" for="myonoffswitch'.$id.'"> <span class="onoffswitch-inner" onclick="return  statuschange('.$id.')"></span><span class="onoffswitch-switch" onclick="return  statuschange('.$id.')"></span> </label></div></div></td>';
                          $append .= '<td  style="min-width:10px"> <a href="#" class="btn button_table" >';
                         $append .=  '<i class="fa fa-cog"></i><div class="other_button_section">';
                          $append .= '<div class="oth_btn_1"  onclick="viewlink('.$id.',\'about\')">';
                          $append .= '<i class="fa fa-info"></i> <br>About</div>';
                          $append .= '<div class="oth_btn_1"  onclick="viewlink('.$id.',\'menu\')"><i class="fa fa-cutlery"></i><br>Items</div>';
                          $append .= '<div class="oth_btn_1"  onclick="viewlink('.$id.',\'review\')"><i class="fa fa-star-o"></i><br>Review</div>';
                          $append .= '<div class="oth_btn_1"  onclick="viewlink('.$id.',\'tax\')"><i class="fa">%</i><br>Tax</div>';
                          $append .= '<div class="oth_btn_1"  onclick="viewlink('.$id.',\'offer\')"><i class="fa fa-star-o "></i><br>Offer</div>';
                          $append .= '<div class="oth_btn_1"  onclick="viewlink('.$id.',\'login\')"><i class="fa fa-star-o "></i>  <br>Login</div>';
                          $append .= '<div class="oth_btn_1"  onclick="viewlink('.$id.',\'privilege\')"><i class="fa fa-star-o "></i>  <br>Permissions';
                          $append .= '</div></div></a></td></tr>';		  
		}
		} else{
			$append .= '<tr><td colspan="7" style="text-align:center"><strong >No Data</strong></td></tr>';
		}
		return ['fltr_data'=>$append,'data_count'=>$data_count,'count' =>$grant_total,'searchcount' =>$search_count,'current_count'=>$pointing];

    }
	public function vendor_privileges($vid){
		$content = "";
		$module_info = DB::SELECT("SELECT a.m_id,a.module_name,a.sub_module,b.active,b.user_id FROM module_master a LEFT JOIN users_modules b ON a.m_id=b.module_id LEFT JOIN users c ON c.id=b.user_id WHERE c.restaurant_id='$vid' ORDER BY a.display_order");
		if(count($module_info)!=0){
		foreach($module_info as $info){
			$content .= '<tr>
                                <td style="width:70%"><strong>'.$info->sub_module.'</strong></td>
                                <td style="width:20%"><div class="status_chck">
                                    <div class="onoffswitch">';
									if($info->active=='Y'){
                                        $content .= '<input type="checkbox" checked name="onoffswitch" class="onoffswitch-checkbox" id="module_'.$info->m_id.'">';
									} else {  
									   $content .= '<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="module_'.$info->m_id.'">';
									}  
									   $content .= '<label class="onoffswitch-label" for="module_'.$info->m_id.'">
                                            <span class="onoffswitch-inner" onclick=\'update_privilege("'.$info->user_id.'","'.$info->m_id.'")\'></span>
                                            <span class="onoffswitch-switch" onclick=\'update_privilege("'.$info->user_id.'","'.$info->m_id.'")\'></span>
                                        </label>
                                    </div>
                                    </div>
                                </td>
                            </tr>';
		}
		} else{
		$content .= '<tr><td colspan="4"><strong>The Vendor does not have login credentials<strong></td></tr>';	
		}
		return $content;
	}
	public function update_privilege(Request $request){
		$userid   = $request['userid'];
		$moduleid = $request['moduleid'];
		$status   = $request['status'];
		if($status==true){
			$status='Y';
		} else{
			$status = 'N';
		}
		DB::UPDATE("UPDATE users_modules SET active='$status' WHERE user_id='$userid' AND module_id='$moduleid' ");
	}
    //Edit View of Restaurant
    public function restaurant_edit($id)
    {
		$logingroup = Session::get('logingroup');
		 $staffid = Session::get('staffid');
		 if($logingroup=='V' && $staffid!=$id){
			 return view('404');
		 } else{
			$business_cat=DB::SELECT("SELECT id,URL_DECODE(category) as category,active FROM business_category WHERE 1");

			$restaurantdetail = DB::SELECT('SELECT is_scheduled,is_pickup,schedule_before_time,max_order_slot,name_tagline->>"$.name" as name,name_tagline->>"$.description" as description,name_tagline->>"$.tag_line" as tagline,address,email,mobile->>"$.ind" as code,mobile->>"$.mobile" as mob,phone,geo_cordinates,point_of_contact,min_delivery_time,min_cart_value,min_prepration_time,speical_message,cuisines,license_numbers,extra_rate_percent,category,google_location,city,country->>"$.country" as country,country->>"$.currency" as currency,delivery_range_unit->>"$.unit" as unit,star_rating->>"$.value" as value,delivery_range_unit->>"$.range" as ranges,restaurant_group.id as id,star_rating,
						   expensive_rating,delivery_charge,packing_charge,status,popular_display_order,pure_veg,p_exclusive,busy,operational_time,logo,banner,restaurant_master.id as rid,restaurant_group.group_name FROM `restaurant_master` LEFT JOIN restaurant_group ON restaurant_master.group_id = restaurant_group.id WHERE restaurant_master.id = "'.$id.'"');
			$abc =  array(json_decode($restaurantdetail[0]->operational_time));

			$siteurl = Datasource::getsiteurl();
			return view('restaurant.restaurant_edit',compact('restaurantdetail','id','siteurl','business_cat'));
		 }
    }

    //Updation of Restaurant Details
    public function edit_restaurant(Request $request)
    {
        $rid = $request['edrid'];
        $logo = Input::file('logo');
        $banner = Input::file('banner');
        $url = Datasource::geturl();
        $timeDate = date("jmYhis") . rand(991, 9999);
        $date = date('Y-m-d');
        if (Input::file('logo') != '') {
            $image = Input::file('logo');
            $uploadfile = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(260, 200)->save(base_path() . '/uploads/logo/' . $uploadfile);
            if ($request['oldlogo'] == '') {
                $logo = 'uploads/logo/' . $uploadfile;

//                $file_path = base_path() . '/' . $request['oldlogo'];
//                unlink($file_path);
            }
            $logo = 'uploads/logo/' . $uploadfile;
        } elseif($request['oldlogo'] != '')
        {
            $logo = $request['oldlogo'];
        }
        else
        {
            $logo = '';
        }


        if (Input::file('banner') != '') {
            $image = Input::file('banner');
            $uploadfile = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->resize(250, 250)->save(base_path() . '/uploads/restaurant_banner/' . $uploadfile);
            if ($request['oldbanner'] == '') {
                $banner = 'uploads/restaurant_banner/' . $uploadfile;
//                $file_path = base_path() . '/' . $request['oldbanner'];
//                unlink($file_path);
            }
            $banner = 'uploads/restaurant_banner/' . $uploadfile;
        } elseif($request['oldbanner'] != '')
        {
            $banner = $request['oldbanner'];
        }
        else
        {
            $banner = '';
        }


        if($request['edphone']== '')
        {
            $phone = '0';
        }
        else
        {
            $phone = $request['edphone'];
        }
        if($request['eddel_time']== '')
        {
            $del_time = '0';
        }
        else
        {
            $del_time = $request['eddel_time'];
        }
        if($request['edcart_value']== '')
        {
            $cart_value = '0';
        }
        else
        {
            $cart_value = $request['edcart_value'];
        }
        if($request['edpre_deltime']== '')
        {
            $pre_deltime = '0';
        }
        else
        {
            $pre_deltime= $request['edpre_deltime'];
        }
        if($request['edextra_rate']== '')
        {
            $extra_rate = '0';
        }
        else
        {
            $extra_rate= $request['edextra_rate'];
        }
        if($request['edcode']== '')
        {
            $ind = '+91';
        }
        else
        {
            $ind= $request['edcode'];
        }
        if($request['edrange']== '')
        {
            $range = '0';
        }
        else
        {
            $range= $request['edrange'];
        }
        if($request['edorder']== '')
        {
            $order = '0';
        }
        else
        {
            $order= $request['edorder'];
        }
        if($request['eddel_charge']== '')
        {
            $eddel_charge = '0';
        }
        else
        {
            $eddel_charge = $request['eddel_charge'];
        }
        if($request['edpack_charge']== '')
        {
            $edpack_charge = '0';
        }
        else
        {
            $edpack_charge = $request['edpack_charge'];
        }
        $restaurant = DB::select("SELECT name_tagline->>'$.name' FROM restaurant_master where name_tagline->>'$.name' = '".$request['edrname']."' and id != '".$rid."'");
        if(count($restaurant)>0)
        {
            $msg = 'already exist';
            return response::json(compact('msg'));
        }
        else
        {
            $dietsave = $request['eddietsave'];
            if($dietsave == "false")
            {
                $diet = 'Y';
            }
            else
            {
                $diet = 'N';
            }

            $p_exclusivesave = $request['edp_exclusive'];
            if($p_exclusivesave == "true")
            {
                $p_exclusive = 'Y';
            }
            else
            {
                $p_exclusive = 'N';
            }
            $latitude = str_replace(",","", $request['edlat']);
            $longitude = str_replace(",","", $request['edlong']);
            $geocordinates = $latitude.','.$longitude;
            $group = DB::select("select * from restaurant_group where group_name =  '".$request['edgroup']."'");
            $edgeo_location =  str_replace(array("'","\"","\\","/"),array("","","","_","_"), $request['edgeo_location']);
           $delivery_type = $request['delivery_type'];
			$is_pickup = $request['is_pickup'];
			$del_befor_time = $request['del_befor_time'];
			$max_schedule_slot = $request['max_schedule_slot'];
			if($delivery_type=='delivery'){
				$is_presheduled='N';
			} else{
				$is_presheduled='Y';
			}
			if($is_pickup=='no_pickup'){
				$is_pickup='Y';
			} else{
				$is_pickup='N';
			}
		   if(count($group)==0)
            {
                DB::INSERT("INSERT INTO `restaurant_group`(`group_name`)VALUES('".$request['edgroup']."')");
            }
          
			DB::SELECT("UPDATE `restaurant_master` SET `group_id`=(select id from restaurant_group where group_name = '".$request['edgroup']."'),`name_tagline`=json_object('name','" . $request['edrname'] . "','tag_line','" . $request['edtagline'] . "','description','" . $request['eddescription'] . "'),`pure_veg`='$diet',`p_exclusive`='$p_exclusive',`category`='" . $request['edcategory'] . "',`address`='" . $request['edaddress'] . "',`email`='" . $request['edemail'] . "',`phone`='$phone',`mobile`=json_object('ind','$ind','mobile','" . $request['edmobile'] . "'),`point_of_contact`='" . $request['edptcontact'] . "',`city`='" . $request['edcity'] . "',`country`=json_object('currency','" . $request['edcurrency'] . "','country','" . $request['edcountry'] . "'),`delivery_range_unit`=json_object('unit','" . $request['edunit'] . "','range','$range'),`min_delivery_time`='$del_time',`min_prepration_time`='$pre_deltime',`speical_message`='" . $request['edmessage'] . "',`min_cart_value`='$cart_value',`cuisines`='" . $request['edcuisine'] . "',`license_numbers`='" . $request['edlic_cert'] . "',`extra_rate_percent`='$extra_rate',`google_location`='" . $edgeo_location . "',`registeration_date`='$date',`logo`='$logo',`banner`='$banner',`busy`='" . $request['busy'] . "',`status`='".$request['edstatus']."',`popular_display_order`='$order',`delivery_charge`='$eddel_charge',`packing_charge`='$edpack_charge',`expensive_rating`='" . $request['edexp_rate'] . "',`geo_cordinates` =  '". $geocordinates ."',is_scheduled='$is_presheduled',is_pickup='$is_pickup',schedule_before_time='$del_befor_time',max_order_slot='$max_schedule_slot'  WHERE `id` = '$rid'");

			//DB::SELECT("UPDATE `restaurant_master` SET `group_id`=(select id from restaurant_group where group_name = '".$request['edgroup']."'),`name_tagline`=json_object('name','" . $request['edrname'] . "','tag_line','" . $request['edtagline'] . "','description','" . $request['eddescription'] . "'),`pure_veg`='$diet',`p_exclusive`='$p_exclusive',`category`='" . $request['edcategory'] . "',`address`='" . $request['edaddress'] . "',`email`='" . $request['edemail'] . "',`phone`='$phone',`mobile`=json_object('ind','$ind','mobile','" . $request['edmobile'] . "'),`point_of_contact`='" . $request['edptcontact'] . "',`city`='" . $request['edcity'] . "',`country`=json_object('currency','" . $request['edcurrency'] . "','country','" . $request['edcountry'] . "'),`delivery_range_unit`=json_object('unit','" . $request['edunit'] . "','range','$range'),`min_delivery_time`='$del_time',`min_prepration_time`='$pre_deltime',`speical_message`='" . $request['edmessage'] . "',`min_cart_value`='$cart_value',`cuisines`='" . $request['edcuisine'] . "',`license_numbers`='" . $request['edlic_cert'] . "',`extra_rate_percent`='$extra_rate',`google_location`='" . $edgeo_location . "',`registeration_date`='$date',`logo`='$logo',`banner`='$banner',`busy`='" . $request['busy'] . "',`status`='".$request['edstatus']."',`popular_display_order`='$order',`delivery_charge`='$eddel_charge',`packing_charge`='$edpack_charge',`expensive_rating`='" . $request['edexp_rate'] . "',`geo_cordinates` =  '". $geocordinates ."' WHERE `id` = '$rid'");

            $msg = 'success';
            return response::json(compact('msg'));
        }

        return redirect('manage_restaurant');

    }

    //Updation and Adding of Open Close Time
    public function openclose_time(Request $request)
    {
        $day_list = array();
        $i=0;
        $days= $request['day'];
        $from1= $request['from1'];
        $ampm1= $request['ampm1'];
        $to1= $request['to1'];
        $ampm2= $request['ampm2'];
        $edrid= $request['edrid'];
        $timing_option= $request['timing_option'];
		if($timing_option!='schedule'){
            $msg = 'success';
			foreach($days as $key=>$day)
			{
				$existdata = DB::select("SELECT operational_time FROM restaurant_master WHERE id='$edrid'");
				$length = DB::SELECT("SELECT IFNULL(JSON_LENGTH(operational_time->>'$.$day'),0) as count FROM `restaurant_master` WHERE id='$edrid'");
				if($length[0]->count<= 0) {
					$time = $length[0]->count + 1;
					$times = "time" . $time;
					if ($existdata[0]->operational_time == '')
					{
						$optime = '{"' . $day . '":{"' . $times . '":{"open":"' . $from1 . ' ' . $ampm1 . '","close":"' . $to1 . ' ' . $ampm2 . '"}}}';
						DB::SELECT("UPDATE restaurant_master SET operational_time='$optime' WHERE id ='$edrid'");
					}
					else if ($time == 1)
					{
						DB::SELECT("UPDATE restaurant_master SET operational_time=JSON_INSERT(operational_time,'$.$day',JSON_OBJECT('time1',JSON_OBJECT('open','$from1 $ampm1','close','$to1 $ampm2'))) WHERE id ='$edrid' ");
					}
					else
					{
						DB::SELECT("UPDATE restaurant_master SET operational_time=JSON_SET(operational_time,'$.$day.$times',JSON_OBJECT('open','$from1 $ampm1','close','$to1 $ampm2')) WHERE id ='$edrid'");
					}
					
				}
				else
				{
                    if($msg=='success'){$msg='  Already Exist ';}
					$msg .= $day.',';
				}
				$i++;
			}
        } else{
			$from_time = $from1;
			if($ampm1=='PM'){
				$from_time = $from_time+12;
			}
			$to_time   = $to1;
			if($ampm2=='PM'){
				$to_time = $to_time+12;
			}
			$msg="success";
			foreach($days as $key=>$day)
			{
				$is_exist = DB::SELECT("SELECT * FROM vendor_schedule_time WHERE vendor_id='$edrid' AND day='$day' AND ('$from_time' BETWEEN from_time AND to_time OR '$to_time' BETWEEN from_time AND to_time) ");	
				if(count($is_exist)==0){
					DB::INSERT("INSERT INTO vendor_schedule_time (vendor_id,sl_no,day,from_time,to_time) VALUES ('$edrid',0,'$day','$from_time','$to_time')");
					
				} else{
                    if($msg=='success'){$msg='  Already Exist ';}
					$msg .= $day.',';
				}
			}
		}
        return response::json(compact('msg','timing_option'));
    }

    //View Open Close Time in Restaurant
    public function view_time(Request $request)
    {

        $id = $request['edrid'];
        $timing_option = $request['timing_option'];$append = '';
		if($timing_option!='schedule') {
        $day = DB::SELECT('SELECT day FROM `day_setting`');
        $m=0;
        
        foreach($day as $mnth)
        {
            $mname = $mnth->day;
            $length = DB::SELECT("SELECT json_length(`operational_time`->>'$.$mname') as count FROM `restaurant_master` WHERE id='".$id."' ");
            $settings[$m] = $length[0]->count;
            $time_count =$length[0]->count;
            for($l=1;$l<=6;$l++)
            {
                $timelimit = 'time'.$l;
                $time_info = DB::SELECT("SELECT IFNULL(operational_time->>'$.$mname.$timelimit.open',0) as opening,IFNULL(operational_time->>'$.$mname.$timelimit.close',0) as closing FROM `restaurant_master` WHERE id='".$id."' ");
                if($time_info[0]->opening !=0 || $time_info[0]->closing !=0){
                    $append .=  "<tr>";
                    $append .=  "<td style='width:100px'>". $mname."</td>";
                    $append .=  "<td style='width:90px'>".$time_info[0]->opening."</td>";
                    $append .=  "<td style='width:90px'>".$time_info[0]->closing."</td>";
                    $append .=  "<td style='width:40px'> <a class='btn button_table' onclick=\"time_delete('$mname','$timelimit')\";><i class='fa fa-trash'></i></a></td>";
                    $append .=  "</tr>";
                }


            }
            $m++;
        }
       } else{
		   $schedule_info = DB::SELECT("SELECT * FROM `vendor_schedule_time` WHERE vendor_id='$id' ORDER BY FIELD(day, 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY')");
          if(count( $schedule_info)!=0){
            foreach($schedule_info as $info){
                $from_time = $info->from_time;
                 $fampm = 'AM';
                $to_time   = $info->to_time;
                $tampm = 'AM';
                if($from_time>12){
                   $from_time = $from_time-12;
                   $fampm = 'PM';
                } 
                if($to_time>12){
                   $to_time = $to_time-12;
                   $tampm = 'PM';
                }
                 $append .=  "<tr>";
                     $append .=  "<td style='width:100px'>".$info->day."</td>";
                     $append .=  "<td style='width:90px'>".$from_time.' '.$fampm."</td>";
                     $append .=  "<td style='width:90px'>".$to_time.' '.$tampm."</td>";
                     $append .=  "<td style='width:40px'> <a class='btn button_table' onclick=\"time_delete('$info->id','schedule')\";><i class='fa fa-trash'></i></a></td>";
                     $append .=  "</tr>";
            }
          } else{
            $append .= '<tr><td colspan="4" style="text-align:center"><strong>No Data Found</strong></td></tr>';
          }
           
	   }
        return $append ;
//      $restaurantdetail = DB::SELECT('SELECT operational_time FROM `restaurant_master` WHERE id = "'.$id.'"');
//      $optime =  array(json_decode($restaurantdetail[0]->operational_time));
//      return $optime;
    }

    //Delete Open Close Time in Restaurant
    public function delete_time(Request $request)
    {
        $id = $request['edrid'];
        $day = $request['day'];
        $time = $request['time'];
		if($time!='schedule'){
        $length = DB::SELECT("SELECT json_length(`operational_time`->>'$.$day') as count FROM `restaurant_master` WHERE id='".$id."' ");
        if ($length[0]->count == 1)
        {
            $datadel = DB::SELECT("UPDATE restaurant_master SET operational_time=JSON_REMOVE(operational_time,'$.$day') WHERE id ='".$id."'");
        }
        else
        {
            $datadel = DB::SELECT("UPDATE restaurant_master SET operational_time=JSON_REMOVE(operational_time,'$.$day.$time') WHERE id ='".$id."'");
        }
		} else{
			$datadel = DB::DELETE("delete from vendor_schedule_time WHERE id='$day' ");
		}
        return $datadel;
    }

    public function restaurant_status(Request $request)
    {
        $restaurant = Restaurant_Master::where('id','=',$request['ids'])
            ->where('busy','=','Y')
            ->get();
        if(count($restaurant)>0)
        {
            Restaurant_Master::where('id', $request['ids'])->update(
                [
                    'busy' => 'N'
                ]);
        }
        else
        {
            Restaurant_Master::where('id', $request['ids'])->update(
                [
                    'busy' => 'Y'
                ]);
        }
    }

    //restaurant list of particular location for mobile app
    public function restaurantlists($line1,$category,$term)
    {
        $typearr = array();
        $restaurantarr = array();
        $restaurants = array();
        $timezone = 'ASIA/KOLKATA';
        $date = new DateTime('now', new DateTimeZone($timezone));
        $datetime = $date->format('Y m d h:i:s a');
        $time = strtoupper($date->format('h:i a'));
        $day = strtoupper($date->format('l'));
        $status = '';
        if($term != 'null')
        {
            //  $restaurants = DB::SELECT('SELECT rt.id as res_id,p_exclusive,json_length(rt.operational_time->"$.' . $day . '")  as count,rt.operational_time as time,rt.busy,rt.pure_veg FROM `restaurant_master` rt join `restaurant_menu` rm on rt.id = rm.m_rest_id WHERE rt.status = "Y" and json_length(rt.operational_time->"$.' . $day . '")  > 0 and JSON_UNQUOTE(rm.m_name_type->"$.name") = "' . trim($term) . '" ORDER BY JSON_UNQUOTE(rt.name_tagline->"$.name")');
            $restaurant = DB::SELECT('SELECT rt.id as res_id,p_exclusive,json_length(rt.operational_time->"$.' . $day . '")  as count,rt.operational_time as time,rt.busy,rt.pure_veg,rt.google_location as rest_location,rt.geo_cordinates as cordinates,rt.delivery_range_unit->>"$.range" as range_unit FROM `restaurant_master` rt join `restaurant_menu` rm on rt.id = rm.m_rest_id WHERE rt.status = "Y" and json_length(rt.operational_time->"$.' . $day . '")  > 0 and JSON_UNQUOTE(rm.m_name_type->"$.name") = "' . trim($term) . '" ORDER BY popular_display_order asc');
        }
        else
        {
            /* $restaurants = DB::SELECT('SELECT rt.id as res_id,p_exclusive,json_length(rt.operational_time->"$.' . $day . '")  as count,rt.operational_time as time,rt.busy,rt.pure_veg FROM `restaurant_master` rt  WHERE rt.status = "Y" and json_length(rt.operational_time->"$.' . $day . '")  > 0  ORDER BY JSON_UNQUOTE(rt.name_tagline->"$.name")');*/
            $restaurant = DB::SELECT('SELECT rt.id as res_id,p_exclusive,json_length(rt.operational_time->"$.' . $day . '")  as count,rt.operational_time as time,rt.busy,rt.pure_veg,rt.google_location as rest_location,rt.geo_cordinates as cordinates,rt.delivery_range_unit->>"$.range" as range_unit FROM `restaurant_master` rt  WHERE rt.status = "Y" and json_length(rt.operational_time->"$.' . $day . '")  > 0 ORDER BY popular_display_order asc');
        }
        $deliverylocking_status = Commonsource::deliverylocking();
        if($deliverylocking_status == 'Y')
        {
            if($line1 == 'null')
            {
                $restaurants = $restaurant;
            }
            else
            {
                $typearr = explode('_',$line1);
                if(count($typearr)>0)
                {
                    if(strtoupper($typearr[0]) == 'LAT')
                    {
                        if (strpos($typearr[1], '$') !== false)
                        {
                            $cordinates = explode('$',$typearr[1]);
                        }
                        else
                        {
                            return response::json(['msg' => 'Latitude/Longitude should be separated by `$`']);
                        }
                    }
                    else if(strtoupper($typearr[0]) == 'ADD')
                    {
                        $cordnt = Commonsource::latitude_longitude($typearr[1]);
                        if($cordnt[2] == '0')
                        {
                            return response::json(['msg' => 'Not Exist']);
                        }
                        else
                        {
                            $cordinates = $cordnt;
                        }
                    }
                    else
                    {

                         return response::json(['msg' => 'line1 should have lat/add as prefix']);
                    }
                }

                $restaurantlist = Commonsource::locaterestaurant($restaurant,$cordinates);
                if(count($restaurantlist) >0)
                {
                    $restaurants = $restaurantlist[0];
                    $restarr = $restaurantlist[1];
                    $restlist = implode(',',$restaurantlist[1]);
                }
            }
        }
        else
        {
            $restaurants = $restaurant;
        }
        if($category == 'all')
        {
            if (count($restaurants) > 0) {

                foreach($restaurants as $key=>$item)
                {
                    if($item->busy == 'Y')
                    {
                        $detail =  DB::SELECT("SELECT id as res_id,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,IFNULL(expensive_rating,0) as expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Busy' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' ORDER BY popular_display_order asc");
                        if(count($detail)>0)
                        {
                            $restaurantarr[] = $detail[0];
                        }
                    }
                    else
                    {
                        for($i = 1; $i<=$item->count;$i++)
                        {
                            $json_data = json_decode($item->time,true);
                            $open      = strtoupper($json_data[$day]['time'.$i]['open']);
                            $close     = strtoupper($json_data[$day]['time'.$i]['close']);
                            if (strtotime($time) >= strtotime($open) && strtotime($time) <= strtotime($close))
                            {
                                $detail =  DB::SELECT("SELECT id as res_id,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,IFNULL(expensive_rating,0) as expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Open' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' ORDER BY popular_display_order asc");
                                if(count($detail)>0)
                                {
                                    $restaurantarr[] = $detail[0];
                                }
                                break;
                            }
                            else
                            {
                                $detail =  DB::SELECT("SELECT id as res_id,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,IFNULL(expensive_rating,0) as expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Closed' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' ORDER BY popular_display_order asc");
                                if(count($detail)>0)
                                {
                                    $restaurantarr[] = $detail[0];
                                }
                            }
                        }
                    }
                }
            }
        }
        else if($category=='veg')
        {
            if(isset($restarr) && count($restarr)>0)
            {
                $restaurants = DB::SELECT('SELECT id as res_id,p_exclusive,json_length(operational_time->"$.'.$day.'") as count,operational_time as time,busy,pure_veg FROM `restaurant_master` WHERE status = "Y" and pure_veg = "Y" and json_length(operational_time->"$.'.$day.'") > 0 and id in ('.$restlist.') ORDER BY JSON_UNQUOTE(name_tagline->"$.name")');
            }
            else{
                $restaurants = DB::SELECT('SELECT id as res_id,p_exclusive,json_length(operational_time->"$.'.$day.'") as count,operational_time as time,busy,pure_veg FROM `restaurant_master` WHERE status = "Y" and pure_veg = "Y" and json_length(operational_time->"$.'.$day.'") > 0 ORDER BY JSON_UNQUOTE(name_tagline->"$.name")');
            }
            if (count($restaurants) > 0)
            {
                foreach($restaurants as $key=>$item)
                {
                    if($item->busy == 'Y')
                    {
                        $detail =  DB::SELECT("SELECT id as res_id,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Busy' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' and pure_veg = 'Y' ORDER BY popular_display_order asc");
                        if(count($detail)>0)
                        {
                            $restaurantarr[] = $detail[0];
                        }
                    }
                    else
                    {
                        for($i = 1; $i<=$item->count;$i++)
                        {
                            $json_data = json_decode($item->time,true);
                            $open      = strtoupper($json_data[$day]['time'.$i]['open']);
                            $close     = strtoupper($json_data[$day]['time'.$i]['close']);
                            if (strtotime($time) >= strtotime($open) && strtotime($time) <= strtotime($close))
                            {
                                $detail =  DB::SELECT("SELECT id as res_id,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Open' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' and pure_veg = 'Y' ORDER BY popular_display_order asc");
                                if(count($detail)>0)
                                {
                                    $restaurantarr[] = $detail[0];
                                }
                                break;
                            }
                            else
                            {
                                $detail =  DB::SELECT("SELECT id as res_id,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Closed' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' and pure_veg = 'Y' ORDER BY popular_display_order asc");
                                if(count($detail)>0)
                                {
                                    $restaurantarr[] = $detail[0];
                                }
                            }
                        }
                    }
                }
            }
        }
        else if($category == 'exclusive')
        {
            if(isset($restarr) && count($restarr)>0)
            {
                $restaurants = DB::SELECT('SELECT id as res_id,json_length(operational_time->"$.' . $day . '") as count,operational_time as time,busy,pure_veg FROM `restaurant_master` WHERE status = "Y" and p_exclusive = "Y" and json_length(operational_time->"$.' . $day . '") > 0 and id in ('.$restlist.') ORDER BY JSON_UNQUOTE(name_tagline->"$.name")');
            }
            else
            {
                $restaurants = DB::SELECT('SELECT id as res_id,json_length(operational_time->"$.'.$day.'") as count,operational_time as time,busy,pure_veg FROM `restaurant_master` WHERE status = "Y" and p_exclusive = "Y" and json_length(operational_time->"$.'.$day.'") > 0 ORDER BY JSON_UNQUOTE(name_tagline->"$.name")');
            }
            if (count($restaurants) > 0)
            {
                foreach($restaurants as $key=>$item)
                {

                    if($item->busy == 'Y')
                    {
                        //return "SELECT id as res_id,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Busy' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' and p_exclusive = 'Y' ORDER BY popular_display_order asc";
                        $detail =  DB::SELECT("SELECT id as res_id,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Busy' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' and p_exclusive = 'Y' ORDER BY popular_display_order asc");
                        if(count($detail)>0)
                        {
                            $restaurantarr[] = $detail[0];
                        }
                    }
                    else
                    {
                        for($i = 1; $i<=$item->count;$i++)
                        {
                            $json_data = json_decode($item->time,true);
                            $open      = strtoupper($json_data[$day]['time'.$i]['open']);
                            $close     = strtoupper($json_data[$day]['time'.$i]['close']);
                            if (strtotime($time) >= strtotime($open) && strtotime($time) <= strtotime($close))
                            {
                                $detail =  DB::SELECT("SELECT id as res_id,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Open' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' and p_exclusive = 'Y' ORDER BY popular_display_order asc");
                                if(count($detail)>0)
                                {
                                    $restaurantarr[] = $detail[0];
                                }
                                break;
                            }
                            else
                            {
                                $detail =  DB::SELECT("SELECT id as res_id,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Closed' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' and p_exclusive = 'Y' ORDER BY popular_display_order asc");
                                if(count($detail)>0)
                                {
                                    $restaurantarr[] = $detail[0];
                                }
                            }
                        }
                    }
                }
            }
        }
        else if($category=='top_rated')
        {
            if(isset($restarr) && count($restarr)>0) {
                $restaurants = DB::SELECT('SELECT id as res_id,json_length(operational_time->"$.' . $day . '") as count,operational_time as time,busy,pure_veg FROM `restaurant_master` WHERE status = "Y" and JSON_UNQUOTE(star_rating->"$.value") !="" and json_length(operational_time->"$.' . $day . '") > 0 and id in (' . $restlist . ') ORDER BY popular_display_order asc limit 0,10');
            }
            else
            {
                $restaurants = DB::SELECT('SELECT id as res_id,json_length(operational_time->"$.'.$day.'") as count,operational_time as time,busy,pure_veg FROM `restaurant_master` WHERE status = "Y" and JSON_UNQUOTE(star_rating->"$.value") !="" and json_length(operational_time->"$.'.$day.'") > 0 ORDER BY popular_display_order asc limit 0,10');
            }
            if (count($restaurants) > 0) {
                foreach($restaurants as $key=>$item)
                {
                    if($item->busy == 'Y')
                    {
                        $detail =  DB::SELECT("SELECT id as res_id,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Busy' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' and JSON_UNQUOTE(star_rating->'$.value') !='' ORDER BY popular_display_order asc");
                        if(count($detail)>0)
                        {
                            $restaurantarr[] = $detail[0];
                        }
                    }
                    else
                    {
                        for($i = 1; $i<=$item->count;$i++)
                        {
                            $json_data = json_decode($item->time,true);
                            $open      = strtoupper($json_data[$day]['time'.$i]['open']);
                            $close     = strtoupper($json_data[$day]['time'.$i]['close']);
                            if (strtotime($time) >= strtotime($open) && strtotime($time) <= strtotime($close))
                            {
                                $detail =  DB::SELECT("SELECT id as res_id,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Open' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' and JSON_UNQUOTE(star_rating->'$.value') !='' ORDER BY popular_display_order asc");
                                if(count($detail)>0)
                                {
                                    $restaurantarr[] = $detail[0];
                                }
                                break;
                            }
                            else
                            {
                                $detail =  DB::SELECT("SELECT id as res_id,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Closed' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' and JSON_UNQUOTE(star_rating->'$.value') !='' ORDER BY popular_display_order asc");
                                if(count($detail)>0)
                                {
                                    $restaurantarr[] = $detail[0];
                                }
                            }
                        }
                    }
                }
            }
        }

        if(count($restaurantarr)>0)
        {
            $msg = 'Exist';
            return response::json(['msg' => $msg,'count'=>count($restaurantarr),'restaurants' => $restaurantarr]);
        }
        else
        {
            $msg = 'Not Exist';
            return response::json(['msg' => $msg]);
        }


//      return response::json(['msg' => $msg,'status' => $status,'restaurants' => $restaurantarr]);
    }

    //restaurant list of particular location for mobile app
    public function restaurantlists_new(Request $request)
    {
        $line1 = trim($request['line1']);
        if(trim($line1) == '')
        {
            return response::json(['msg' => 'Not Exist']);
        }
        $category = trim($request['category']);
        $extra_case=""; $extra_case1="";
        $sales_catid = trim($request['sales_catid']);
        if($sales_catid!=''){
            $extra_case = " AND rt.category='$sales_catid' ";
            $extra_case1 = " AND category='$sales_catid' ";
        }
        $term = trim($request['term']);
        $typearr = array();
        $restaurantarr = array();
        $restaurants = array();
        $timezone = 'ASIA/KOLKATA';
        $date = new DateTime('now', new DateTimeZone($timezone));
        $datetime = $date->format('Y m d h:i:s a');
        $time = strtoupper($date->format('h:i a'));
        $day = strtoupper($date->format('l'));
        $status = '';
        if($term != 'null')
        {
          //$restaurants = DB::SELECT('SELECT rt.id as res_id,p_exclusive,json_length(rt.operational_time->"$.' . $day . '")  as count,rt.operational_time as time,rt.busy,rt.pure_veg FROM `restaurant_master` rt join `restaurant_menu` rm on rt.id = rm.m_rest_id WHERE rt.status = "Y" and json_length(rt.operational_time->"$.' . $day . '")  > 0 and JSON_UNQUOTE(rm.m_name_type->"$.name") = "' . trim($term) . '" ORDER BY JSON_UNQUOTE(rt.name_tagline->"$.name")');
            $restaurant = DB::SELECT('SELECT rt.id as res_id,rt.is_scheduled,p_exclusive,json_length(rt.operational_time->"$.' . $day . '")  as count,rt.operational_time as time,rt.busy,rt.pure_veg,rt.google_location as rest_location,rt.geo_cordinates as cordinates,rt.delivery_range_unit->>"$.range" as range_unit FROM `restaurant_master` rt join `restaurant_menu` rm on rt.id = rm.m_rest_id WHERE rt.status = "Y" and json_length(rt.operational_time->"$.' . $day . '")  > 0 and JSON_UNQUOTE(rm.m_name_type->"$.name") = "' . trim($term) . '" '.$extra_case.' ORDER BY popular_display_order asc');
        }
        else
        {
         /* $restaurants = DB::SELECT('SELECT rt.id as res_id,p_exclusive,json_length(rt.operational_time->"$.' . $day . '")  as count,rt.operational_time as time,rt.busy,rt.pure_veg FROM `restaurant_master` rt  WHERE rt.status = "Y" and json_length(rt.operational_time->"$.' . $day . '")  > 0  ORDER BY JSON_UNQUOTE(rt.name_tagline->"$.name")');*/
            $restaurant = DB::SELECT('SELECT rt.id as res_id,rt.is_scheduled,p_exclusive,json_length(rt.operational_time->"$.' . $day . '")  as count,rt.operational_time as time,rt.busy,rt.pure_veg,rt.google_location as rest_location,rt.geo_cordinates as cordinates,rt.delivery_range_unit->>"$.range" as range_unit FROM `restaurant_master` rt  WHERE rt.status = "Y" and json_length(rt.operational_time->"$.' . $day . '")  > 0  '.$extra_case.' ORDER BY popular_display_order asc');
        }
        $deliverylocking_status = Commonsource::deliverylocking();
        if($deliverylocking_status == 'Y')
        {
            if($line1 == 'null')
            {
                $restaurants = $restaurant;
            }
            else
            {
                $typearr = explode('_',$line1);
                if(count($typearr)>0)
                {
                    if(strtoupper($typearr[0]) == 'LAT')
                    {
                        if (strpos($typearr[1], '$') !== false)
                        {
                            $cordinates = explode('$',$typearr[1]);
                        }
                        else
                        {
                            return response::json(['msg' => 'Latitude/Longitude should be separated by `$`']);
                        }
                    }
                    else if(strtoupper($typearr[0]) == 'ADD')
                    {
                        $cordnt = Commonsource::latitude_longitude($typearr[1]);
                        if($cordnt[2] == '0')
                        {
                            return response::json(['msg' => 'Not Exist']);
                        }
                        else
                        {
                            $cordinates = $cordnt;
                        }
                    }
                    else
                    {

                         return response::json(['msg' => 'line1 should have lat/add as prefix']);
                    }
                }

                $restaurantlist = Commonsource::locaterestaurant($restaurant,$cordinates);
                if(count($restaurantlist) >0)
                {
                    $restaurants = $restaurantlist[0];
                    $restarr = $restaurantlist[1];
                    $restlist = implode(',',$restaurantlist[1]);
                }
            }
        }
        else
        {
            $restaurants = $restaurant;
        }
        if($category == 'all')
        {
            if (count($restaurants) > 0) {

                foreach($restaurants as $key=>$item)
                {
                    if($item->busy == 'Y')
                    {
                        $detail =  DB::SELECT("SELECT id as res_id,is_scheduled,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,IFNULL(expensive_rating,0) as expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Busy' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' ORDER BY popular_display_order asc");
                        if(count($detail)>0)
                        {
                            $restaurantarr[] = $detail[0];
                        }
                    }
                    else
                    {
                        for($i = 1; $i<=$item->count;$i++)
                        {
                            $json_data = json_decode($item->time,true);
                            $open      = strtoupper($json_data[$day]['time'.$i]['open']);
                            $close     = strtoupper($json_data[$day]['time'.$i]['close']);
                            if (strtotime($time) >= strtotime($open) && strtotime($time) <= strtotime($close))
                            {
                                $detail =  DB::SELECT("SELECT id as res_id,is_scheduled,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,IFNULL(expensive_rating,0) as expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Open' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' ORDER BY popular_display_order asc");
                                if(count($detail)>0)
                                {
                                    $restaurantarr[] = $detail[0];
                                }
                                break;
                            }
                            else
                            {
                                $detail =  DB::SELECT("SELECT id as res_id,is_scheduled,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,IFNULL(expensive_rating,0) as expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Closed' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' ORDER BY popular_display_order asc");
                                if(count($detail)>0)
                                {
                                    $restaurantarr[] = $detail[0];
                                }
                            }
                        }
                    }
                }
            }
        }
        else if($category=='veg')
        {
            if(isset($restarr) && count($restarr)>0)
            {
                $restaurants = DB::SELECT('SELECT id as res_id,is_scheduled,p_exclusive,json_length(operational_time->"$.'.$day.'") as count,operational_time as time,busy,pure_veg FROM `restaurant_master` WHERE status = "Y" and pure_veg = "Y" and json_length(operational_time->"$.'.$day.'") > 0 and id in ('.$restlist.') '.$extra_case1.' ORDER BY JSON_UNQUOTE(name_tagline->"$.name")');
            }
            else{
                $restaurants = DB::SELECT('SELECT id as res_id,is_scheduled,p_exclusive,json_length(operational_time->"$.'.$day.'") as count,operational_time as time,busy,pure_veg FROM `restaurant_master` WHERE status = "Y" and pure_veg = "Y" and json_length(operational_time->"$.'.$day.'") > 0  '.$extra_case1.' ORDER BY JSON_UNQUOTE(name_tagline->"$.name")');
            }
            if (count($restaurants) > 0)
            {
                foreach($restaurants as $key=>$item)
                {
                    if($item->busy == 'Y')
                    {
                        $detail =  DB::SELECT("SELECT id as res_id,is_scheduled,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Busy' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' and pure_veg = 'Y' ORDER BY popular_display_order asc");
                        if(count($detail)>0)
                        {
                            $restaurantarr[] = $detail[0];
                        }
                    }
                    else
                    {
                        for($i = 1; $i<=$item->count;$i++)
                        {
                            $json_data = json_decode($item->time,true);
                            $open      = strtoupper($json_data[$day]['time'.$i]['open']);
                            $close     = strtoupper($json_data[$day]['time'.$i]['close']);
                            if (strtotime($time) >= strtotime($open) && strtotime($time) <= strtotime($close))
                            {
                                $detail =  DB::SELECT("SELECT id as res_id,is_scheduled,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Open' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' and pure_veg = 'Y' ORDER BY popular_display_order asc");
                                if(count($detail)>0)
                                {
                                    $restaurantarr[] = $detail[0];
                                }
                                break;
                            }
                            else
                            {
                                $detail =  DB::SELECT("SELECT id as res_id,is_scheduled,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Closed' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' and pure_veg = 'Y' ORDER BY popular_display_order asc");
                                if(count($detail)>0)
                                {
                                    $restaurantarr[] = $detail[0];
                                }
                            }
                        }
                    }
                }
            }
        }
        else if($category == 'premium')
        {
            if(isset($restarr) && count($restarr)>0)
            {
                $restaurants = DB::SELECT('SELECT id as res_id,is_scheduled,json_length(operational_time->"$.' . $day . '") as count,operational_time as time,busy,pure_veg FROM `restaurant_master` WHERE status = "Y" and p_exclusive = "Y" and json_length(operational_time->"$.' . $day . '") > 0 and id in ('.$restlist.') '.$extra_case1.'  ORDER BY JSON_UNQUOTE(name_tagline->"$.name")');
            }
            else
            {
                $restaurants = DB::SELECT('SELECT id as res_id,is_scheduled,json_length(operational_time->"$.'.$day.'") as count,operational_time as time,busy,pure_veg FROM `restaurant_master` WHERE status = "Y" and p_exclusive = "Y" and json_length(operational_time->"$.'.$day.'") > 0 '.$extra_case1.'  ORDER BY JSON_UNQUOTE(name_tagline->"$.name")');
            }
            if (count($restaurants) > 0)
            {
                foreach($restaurants as $key=>$item)
                {

                    if($item->busy == 'Y')
                    {
                        //return "SELECT id as res_id,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Busy' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' and p_exclusive = 'Y' ORDER BY popular_display_order asc";
                        $detail =  DB::SELECT("SELECT id as res_id,is_scheduled,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Busy' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' and p_exclusive = 'Y' ORDER BY popular_display_order asc");
                        if(count($detail)>0)
                        {
                            $restaurantarr[] = $detail[0];
                        }
                    }
                    else
                    {
                        for($i = 1; $i<=$item->count;$i++)
                        {
                            $json_data = json_decode($item->time,true);
                            $open      = strtoupper($json_data[$day]['time'.$i]['open']);
                            $close     = strtoupper($json_data[$day]['time'.$i]['close']);
                            if (strtotime($time) >= strtotime($open) && strtotime($time) <= strtotime($close))
                            {
                                $detail =  DB::SELECT("SELECT id as res_id,is_scheduled,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Open' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' and p_exclusive = 'Y' ORDER BY popular_display_order asc");
                                if(count($detail)>0)
                                {
                                    $restaurantarr[] = $detail[0];
                                }
                                break;
                            }
                            else
                            {
                                $detail =  DB::SELECT("SELECT id as res_id,is_scheduled,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Closed' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' and p_exclusive = 'Y' ORDER BY popular_display_order asc");
                                if(count($detail)>0)
                                {
                                    $restaurantarr[] = $detail[0];
                                }
                            }
                        }
                    }
                }
            }
        }
        else if($category=='top_rated')
        {
            if(isset($restarr) && count($restarr)>0) {
                $restaurants = DB::SELECT('SELECT id as res_id,is_scheduled,json_length(operational_time->"$.' . $day . '") as count,operational_time as time,busy,pure_veg FROM `restaurant_master` WHERE status = "Y" and JSON_UNQUOTE(star_rating->"$.value") !="" and json_length(operational_time->"$.' . $day . '") > 0 and id in (' . $restlist . ') '.$extra_case1.'  ORDER BY popular_display_order asc limit 0,10');
            }
            else
            {
                $restaurants = DB::SELECT('SELECT id as res_id,is_scheduled,json_length(operational_time->"$.'.$day.'") as count,operational_time as time,busy,pure_veg FROM `restaurant_master` WHERE status = "Y" and JSON_UNQUOTE(star_rating->"$.value") !="" and json_length(operational_time->"$.'.$day.'") > 0 '.$extra_case1.'  ORDER BY popular_display_order asc limit 0,10');
            }
            if (count($restaurants) > 0) {
                foreach($restaurants as $key=>$item)
                {
                    if($item->busy == 'Y')
                    {
                        $detail =  DB::SELECT("SELECT id as res_id,is_scheduled,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Busy' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' and JSON_UNQUOTE(star_rating->'$.value') !='' ORDER BY popular_display_order asc");
                        if(count($detail)>0)
                        {
                            $restaurantarr[] = $detail[0];
                        }
                    }
                    else
                    {
                        for($i = 1; $i<=$item->count;$i++)
                        {
                            $json_data = json_decode($item->time,true);
                            $open      = strtoupper($json_data[$day]['time'.$i]['open']);
                            $close     = strtoupper($json_data[$day]['time'.$i]['close']);
                            if (strtotime($time) >= strtotime($open) && strtotime($time) <= strtotime($close))
                            {
                                $detail =  DB::SELECT("SELECT id as res_id,is_scheduled,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Open' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' and JSON_UNQUOTE(star_rating->'$.value') !='' ORDER BY popular_display_order asc");
                                if(count($detail)>0)
                                {
                                    $restaurantarr[] = $detail[0];
                                }
                                break;
                            }
                            else
                            {
                                $detail =  DB::SELECT("SELECT id as res_id,is_scheduled,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Closed' as status  FROM `restaurant_master` WHERE  id = '".$item->res_id."' and JSON_UNQUOTE(star_rating->'$.value') !='' ORDER BY popular_display_order asc");
                                if(count($detail)>0)
                                {
                                    $restaurantarr[] = $detail[0];
                                }
                            }
                        }
                    }
                }
            }
        } 

        if(count($restaurantarr)>0) 
        {
            $msg = 'Exist';
            return response::json(['msg' => $msg,'count'=>count($restaurantarr),'restaurants' => $restaurantarr]);
        }
        else
        {
            $msg = 'Not Exist';
            return response::json(['msg' => $msg]);
        }


//      return response::json(['msg' => $msg,'status' => $status,'restaurants' => $restaurantarr]);
    }


    //most popular restaurants are listed
    public function mostpopular_restaurants()
    {
        $restarr = array();
        $timezone = 'ASIA/KOLKATA';
        $date = new DateTime('now', new DateTimeZone($timezone));
        $datetime = $date->format('Y m d h:i:s a');
        $time = strtoupper($date->format('h:i a'));
        $day = strtoupper($date->format('l'));
        $restaurants = DB::SELECT("SELECT id as res_id,JSON_UNQUOTE(name_tagline->'$.name') as name,logo,JSON_UNQUOTE(star_rating->'$.value') as rating FROM `restaurant_master` WHERE id!= '' and status = 'Y' and logo != '' ORDER BY popular_display_order asc limit 0,8");
        if(count($restaurants)>0)
        {
            foreach($restaurants as $item)
            {
                $arr = array();
                $id = $item->res_id ;
                $detail = DB::SELECT("SELECT min_delivery_time,min_prepration_time,operational_time as time,json_length(operational_time->'$." . $day . "')  as count,pure_veg,busy,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_value,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,JSON_UNQUOTE(name_tagline->'$.name') as name FROM `restaurant_master` WHERE  id = '".$id."'");
                if(count( $detail)>0)
                {
                    if (isset($detail[0]))
                    {
                        $count = $detail[0]->count;
                        if ($detail[0]->busy == 'Y')
                        {
                            $status = 'Busy';
                        }
                        else
                        {
                            for ($i = 1; $i <= $count; $i++)
                            {
                                $json_data = json_decode($detail[0]->time, true);
                                $open = strtoupper($json_data[$day]['time' . $i]['open']);
                                $close = strtoupper($json_data[$day]['time' . $i]['close']);
                                if (strtotime($time) >= strtotime($open) && strtotime($time) <= strtotime($close))
                                {
                                    $status = 'Open';
                                    break;
                                }
                                else
                                {
                                    $status = 'Closed';
                                }
                            }
                        }

                    }
                    $arr['res_id'] = $id;
                    $arr['name']   = $item->name;
                    $arr['logo']   = $item->logo;
                    $arr['rating']   = $item->rating;
                    $arr['status'] = $status;
                }
                $restarr[] = $arr;
            }
            if(count($restarr)>0)
            {
                $msg = 'Exist';
                return response::json(['msg' => $msg,'restaurants' => $restarr]);
            }
            else
            {
                $msg = 'Not Exist';
                return response::json(['msg' => $msg]);
            }
        }
        else
        {
            $msg = 'Not Exist';
            return response::json(['msg' => $msg]);
        }
    }



    //restaurant offers
    public function restaurant_offers(Request $request)
    {
        $details =  DB::SELECT("SELECT `image` as image from `restaurant_offers` where `active` = 'Y'");
        if(count($details)>0)
        {
            $msg = 'Exist';
            return response::json(['msg' => $msg,'offers' => $details]);
        }
        else
        {
            $msg = 'Not Exist';
            return response::json(['msg' => $msg]);
        }

    }

    //About restaurants(Restaurant Detaisl)
    public function about_restaurants($id)
    {
        $timezone = 'ASIA/KOLKATA';
        $date = new DateTime('now', new DateTimeZone($timezone));
        $array = array();
        $datetime = $date->format('Y m d h:i:s a');
        $time = strtoupper($date->format('h:i a'));
        $day = strtoupper($date->format('l'));
        $details = Restaurant_Master::where('id',$id)->select("id","name_tagline->name as name","address","star_rating->count as review_count","star_rating->value as star_vaue","delivery_charge","operational_time->$day as time")->first();
        if(count($details)>0)
        {

            $id=$details->id;
            $name=strtoupper($details->name);
            $address=$details->address;
            $charge=$details->delivery_charge;
            $time=$details->time;
            if($details->star_vaue == '')
            {
                $star_vaue = '0';
            }
            else
            {
                $star_vaue = $details->star_vaue;
            }
            if($details->review_count == '')
            {
                $review_count = '0';
            }
            else
            {
                $review_count = $details->review_count;
            }
            $array['id'] = $id;
            $array['name'] = json_decode($name);
            $array['address'] = $address;
            $array['delivery_charge'] = $charge;
            $array['star_rate'] = json_decode($star_vaue);
            $array['review_count'] = json_decode($review_count);
            $array['time'] = json_decode($time);
            $msg = 'Exist';
            return response::json(['msg' => $msg,'details' => $array]);
        }
        else
        {
            $msg = 'Not Exist';
            return response::json(['msg' => $msg]);
        }


    }
    //Review of restaurants(Restaurant Detaisl)
    public function review_restaurants($id)
    {
        //$review = DB::SELECT("SELECT a.id,a.rest_id as res_id,UPPER(JSON_UNQUOTE(name_tagline->'$.name')) as restaurant,JSON_UNQUOTE(entry_by->'$.name') as name,IFNULL(JSON_UNQUOTE(review->'$.star'),0) as star,IFNULL(JSON_UNQUOTE(review->'$.text'),0) as text,DATE_FORMAT(entry_date,'%d %b %Y') as date FROM `restaurant_reviews` a LEFT join restaurant_master b on b.id=a.rest_id  WHERE a.id!= '' and a.status = 'Y' and a.rest_id ='$id' ORDER BY a.entry_date desc limit 0,8");
        $review = DB::SELECT("SELECT rest_id,rest_details->>'$.name' as restaurant,customer_details->>'$.name' as name,review_star as star,review_details->>'$.review' as text,review_details->>'$.date' as date FROM order_master WHERE review_details->>'$.status' = 'Y' and rest_id ='$id' ORDER BY review_details->>'$.date' desc");
        if(count($review)>0)
        {
            $msg = 'Exist';
            return response::json(['msg' => $msg,'review' => $review]);
        }
        else
        {
            $msg = 'Not Exist';
            return response::json(['msg' => $msg]);
        }

    }

//details of particular restaurant
    public function restaurant_web($id)
    {
        $timearr = array();
        $timezone = 'ASIA/KOLKATA';
        $date = new DateTime('now', new DateTimeZone($timezone));
        $detailarray = array();
        $datetime = $date->format('Y m d h:i:s a');
        $time = strtoupper($date->format('h:i a'));
        $day = strtoupper($date->format('l'));
        $detail =  DB::SELECT("SELECT id as res_id,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,address,delivery_charge,pure_veg,cuisines,min_delivery_time,min_prepration_time,JSON_UNQUOTE(star_rating->'$.count') as review_count,
                   JSON_UNQUOTE(star_rating->'$.value') as star_value,logo,operational_time->>'$.$day' as time  FROM `restaurant_master` WHERE  id = '".$id."'");
        foreach($detail as $key=>$item)
        {
            $detailarray['res_id']   =   $item->res_id;
            $detailarray['name']     =   $item->name;
            $detailarray['tag_line'] =   $item->tag_line;
            $detailarray['address']  =   $item->address;
            $detailarray['delivery_charge']  =   $item->delivery_charge;
            $detailarray['pure_veg']  =   $item->pure_veg;
            $detailarray['cuisines']  =   $item->cuisines;
            $detailarray['min_delivery_time']  =   $item->min_delivery_time;
            $detailarray['min_prepration_time']  =   $item->min_prepration_time;
            $detailarray['review_count']  =   $item->review_count;
            $detailarray['star_value']  =   $item->star_value;
            $detailarray['logo']  =   $item->logo;
            $decodetim = json_decode($item->time,true);
            foreach($decodetim as $key=>$time)
            {
                $str_time  =    $time['open'].' - '.  $time['close'];
                $timearr[] =    $str_time;
            }
            $detailarray['time']  = implode (" , ", $timearr);
        }
        if(count($detailarray)>0)
        {
            $msg = "Exist";
            return response::json(['msg' => $msg,'details' => $detailarray]);

        }
        else
        {
            $msg = 'Not Exist';
            return response::json(['msg' => $msg]);
        }
    }

    //restaurant category
    public function restaurant_category($id)
    {
        $categoryarr =array();
        $category = DB::SELECT("select name,image_view  from category where restaurant_id = '".$id."' and status = 'Y' order by order_no asc");
        if(count($category)>0)
        {
            $msg = "Exist";
            foreach($category as $ky=>$val)
            {
                $cat = $val->name;
                $image_view = $val->image_view;
                $menu = DB::SELECT("SELECT m_menu_id as res_id,JSON_UNQUOTE(m_name_type->'$.name') as menu,m_most_selling as most_selling,JSON_UNQUOTE(m_name_type->'$.type') as type,m_por_rate as portion,m_subcategory,m_description,m_tax,m_image,m_diet,JSON_UNQUOTE(m_time->'$.from') as open,JSON_UNQUOTE(m_time->'$.to') as close,m_offer_exists,JSON_UNQUOTE(m_present_offers->'$.type') as offer_type,JSON_UNQUOTE(m_present_offers->'$.offer_rate') as offer_rate,JSON_UNQUOTE(m_present_offers->'$.desc') as description FROM `restaurant_menu` WHERE  m_rest_id = '" . $id . "' and JSON_CONTAINS(m_category, '[\"" . $cat . "\"]')");
                if(count($menu) > 0 )
                {
                    $categoryarr = $category;
                }
            }
        }
        else
        {
            $msg = 'Not Exist';
        }
        return response::json(['msg' => $msg,'category' => $categoryarr]);
    }
    public function search_restaurent_menu($value)
    {
        $timezone = 'ASIA/KOLKATA';
        $date = new DateTime('now', new DateTimeZone($timezone));
        $datetime = $date->format('Y m d h:i:s a');
        $time = strtoupper($date->format('h:i a'));
        $day = strtoupper($date->format('l'));
        $restaurentarr= array();
        $resttarr= array();
        $menu_list= array();
        $timeformat = strtoupper($date->format('h:i:s'));
        $restaurent = DB::SELECT("SELECT DISTINCT(name_tagline->>'$.name') as name,id,operational_time as time,json_length(operational_time->'$." . $day . "')  as count,busy FROM restaurant_master WHERE status='Y' AND upper(name_tagline->>'$.name') LIKE UPPER('".$value."%') order by name");
        $menu_list = DB::SELECT("SELECT DISTINCT(m_name_type->>'$.name') as menu FROM restaurant_menu WHERE m_status='Y' AND upper(m_name_type->>'$.name') LIKE UPPER('".$value."%') and '".$timeformat."' >= m_time->>'$.from' and '".$timeformat."' <= m_time->>'$.to' group by menu");
        foreach($restaurent as $item)
        {
            $restaurentarr['name'] = $item->name;
            $restaurentarr['id']   = $item->id;
            $count                 = $item->count;

            if($item->busy == 'Y')
            {
                $status = 'Busy';
            }
            else
            {
                for($i = 1; $i<=$count;$i++)
                {
                    $json_data = json_decode( $item->time,true);
                    $open      = strtoupper($json_data[$day]['time'.$i]['open']);
                    $close     = strtoupper($json_data[$day]['time'.$i]['close']);
                    if (strtotime($time) >= strtotime($open) && strtotime($time) <= strtotime($close))
                    {
                        $status = 'Open';
                        break;
                    }
                    else
                    {
                        $status = 'Closed';
                    }
                }
            }
            $restaurentarr['status']   =$status;
            $resttarr[] =$restaurentarr;
        }
        return ['restaurant'=>$resttarr,'menu_list'=>$menu_list,'restaurant_count'=>count($resttarr)];
    }
    public function search_restaurent_menu_new(Request $request)
    {
        $value = $request['term'];
        $timezone = 'ASIA/KOLKATA';
        $date = new DateTime('now', new DateTimeZone($timezone));
        $datetime = $date->format('Y m d h:i:s a');
        $time = strtoupper($date->format('h:i a'));
        $day = strtoupper($date->format('l'));
        $restaurentarr= array();
        $resttarr= array();
        $menu_list= array();
        $restaurent = DB::SELECT("SELECT DISTINCT(name_tagline->>'$.name') as name,id,operational_time as time,json_length(operational_time->'$." . $day . "')  as count,busy FROM restaurant_master WHERE status='Y' AND upper(name_tagline->>'$.name') LIKE UPPER('".$value."%') order by name");
        $menu_list = DB::SELECT("SELECT DISTINCT(m_name_type->>'$.name') as menu FROM restaurant_menu WHERE m_status='Y' AND upper(m_name_type->>'$.name') LIKE UPPER('".$value."%') group by menu");
        foreach($restaurent as $item)
        {
            $restaurentarr['name'] = $item->name;
            $restaurentarr['id']   = $item->id;
            $count                 = $item->count;

            if($item->busy == 'Y')
            {
                $status = 'Busy';
            }
            else
            {
                for($i = 1; $i<=$count;$i++)
                {
                    $json_data = json_decode( $item->time,true);
                    $open      = strtoupper($json_data[$day]['time'.$i]['open']);
                    $close     = strtoupper($json_data[$day]['time'.$i]['close']);
                    if (strtotime($time) >= strtotime($open) && strtotime($time) <= strtotime($close))
                    {
                        $status = 'Open';
                        break;
                    }
                    else
                    {
                        $status = 'Closed';
                    }
                }
            }
            $restaurentarr['status']   =$status;
            $resttarr[] =$restaurentarr;
        }
        return ['restaurant'=>$resttarr,'menu_list'=>$menu_list,'restaurant_count'=>count($resttarr)];
    }

    public function search_restaurent_menu_new_location(Request $request)
    {
        $restaurent =array();
        $value = $request['term'];
        $location = $request['location'];
         if($location=='' || $location == 'NULL' || $location=='null') {
            $location = 'lat_11.2528194$75.7710131';
         }
        $cordinates = explode('_', trim($location));
        $location = $cordinates[1];
        $timezone = 'ASIA/KOLKATA';
        $date = new DateTime('now', new DateTimeZone($timezone));
        $datetime = $date->format('Y m d h:i:s a');
        $time = strtoupper($date->format('h:i a'));
        $day = strtoupper($date->format('l'));
        $times = strtoupper($date->format('H:i:s'));
        $days = strtoupper($date->format('D'));
        $restaurentarr= array();
        $resttarr= array();
        $menu_list= array();
        $restaurents = DB::SELECT("SELECT DISTINCT(name_tagline->>'$.name') as name,id as res_id,id,operational_time as time,json_length(operational_time->'$." . $day . "')  as count,busy,google_location as rest_location,geo_cordinates as cordinates,delivery_range_unit->>'$.range' as range_unit FROM restaurant_master WHERE status='Y' AND upper(name_tagline->>'$.name') LIKE UPPER('".$value."%') order by name");
        if(isset($location) && $location!= 'null')
        {
            if (strpos($location, '$') !== false)
            {
                $cordinates = explode('$', $location);
                $restaurantlist = Commonsource::locaterestaurant($restaurents, $cordinates);
                if (count($restaurantlist) > 0)
                {
                    $restaurent = $restaurantlist[0];
                    $restarr = $restaurantlist[1];
                    $restlist = implode(',', $restaurantlist[1]);
                }
            }
            else
            {
                return response::json(['msg' => 'Latitude/Longitude should be separated by `$`']);
            }
        }
        else
        {
            return response::json(['msg' => 'Latitude/Longitude should be given']);
        }
        $restaurant = DB::SELECT("SELECT DISTINCT(name_tagline->>'$.name') as name,id as res_id,id,operational_time as time,json_length(operational_time->'$." . $day . "')  as count,busy,google_location as rest_location,geo_cordinates as cordinates,delivery_range_unit->>'$.range' as range_unit FROM restaurant_master WHERE status='Y' order by name");
        if(isset($location) && $location!= 'null')
        {
            if (strpos($location, '$') !== false)
            {
                $cordnt = explode('$', $location);
                $restaurantlists = Commonsource::locaterestaurant($restaurant, $cordnt);
                if (count($restaurantlists) > 0)
                {
                    $restlists = implode(',', $restaurantlists[1]);
                }
            }
        }
        $menu_list = DB::SELECT("SELECT DISTINCT(m_name_type->>'$.name') as menu FROM restaurant_menu left Join restaurant_master on restaurant_menu.m_rest_id = restaurant_master.id  WHERE m_status='Y' AND JSON_SEARCH(UPPER(m_days), 'one','".$days."') is not null  and upper(m_name_type->>'$.name') LIKE UPPER('".$value."%') and  restaurant_master.id in (".$restlists.") and '$times' >= m_time->>'$.from' AND '$times' <= m_time->>'$.to' group by menu");
//      $menu_list = DB::SELECT("SELECT DISTINCT(m_name_type->>'$.name') as menu FROM restaurant_menu left Join restaurant_master on restaurant_menu.m_rest_id = restaurant_master.id  WHERE m_status='Y' AND JSON_SEARCH(UPPER(m_days), 'one','".$days."') is not null  and upper(m_name_type->>'$.name') LIKE UPPER('".$value."%') and '$times' >= m_time->>'$.from' AND '$times' <= m_time->>'$.to' and  restaurant_master.id in (".$restlist.") group by menu");
        foreach($restaurent as $item)
        {
            $restaurentarr['name'] = $item->name;
            $restaurentarr['id']   = $item->id;
            $count                 = $item->count;

            if($item->busy == 'Y')
            {
                $status = 'Busy';
            }
            else
            {
                for($i = 1; $i<=$count;$i++)
                {
                    $json_data = json_decode( $item->time,true);
                    $open      = strtoupper($json_data[$day]['time'.$i]['open']);
                    $close     = strtoupper($json_data[$day]['time'.$i]['close']);
                    if (strtotime($time) >= strtotime($open) && strtotime($time) <= strtotime($close))
                    {
                        $status = 'Open';
                        break;
                    }
                    else
                    {
                        $status = 'Closed';
                    }
                }
            }
            $restaurentarr['status']   =$status;
            $resttarr[] =$restaurentarr;
        }
        return ['restaurant'=>$resttarr,'menu_list'=>$menu_list,'restaurant_count'=>count($resttarr)];
    }

    public function tax_apply_to_all_menu(Request $request)
    {
        $rest_id = $request['id'];
        $edextra_rate = $request['edextra_rate'];



        $sel_all_category  = DB::SELECT("SELECT m_rest_id,m_tax,m_menu_id,json_length(m_por_rate) as len,m_menu_id  FROM `restaurant_menu` WHERE m_rest_id='".$rest_id."'");

        $extra_rate_percent  = DB::UPDATE("UPDATE `restaurant_master` SET `extra_rate_percent`='$edextra_rate' WHERE `id`='$rest_id'");
        foreach ($sel_all_category as $value) {
            $len       = $value->len;
            $m_menu_id = $value->m_menu_id;
            $res_id    = $rest_id;
            $i=0;
            for($k = 0; $k<$len;$k++)
            {
                $i++;

                $update   = DB::UPDATE("UPDATE `restaurant_menu` SET `m_por_rate` = JSON_SET(`m_por_rate`,'$.portion$i.extra_percent',$edextra_rate) WHERE `m_rest_id`='$res_id' ");
                $update   = DB::UPDATE("UPDATE `restaurant_menu` SET `m_por_rate` = JSON_SET(`m_por_rate`,'$.portion$i.extra_val',(`m_por_rate`->>'$.portion$i.exc_rate' * `m_por_rate`->>'$.portion$i.extra_percent') / 100) WHERE `m_rest_id`='$res_id' AND `m_menu_id`='$m_menu_id'");

                $update3  = DB::UPDATE("UPDATE `restaurant_menu` SET `m_por_rate` = JSON_SET(`m_por_rate`,'$.portion$i.final_rate',(`m_por_rate`->>'$.portion$i.inc_rate' + `m_por_rate`->>'$.portion$i.extra_val')) WHERE `m_rest_id`='$res_id' AND `m_menu_id`='$m_menu_id'");
            }


        }

        return 'succ';
    }

    //update favourites of restaurant
    public function favourite_update($userid,$restid,$status)
    {
        $msg = "";
        $restarr = array();
        $detail = DB::SELECT('select count(*) as count,favourites from customer_list where  id="' . $userid . '"');
        if (count($detail) > 0) {
            $fav = json_decode($detail[0]->favourites, true);
            if(strtoupper($status) == 'U')
            {
                if (count($fav) <= 0)
                {
                    $fav[] = $restid;
                    $msg = 'updated';
                }
                else
                {
                    if (in_array($restid, $fav))
                    {
                        unset($fav[array_search($restid, $fav)]);
                    }
                    else
                    {
                        array_push($fav, $restid);
                    }
                }

                DB::SELECT('update customer_list set favourites = \'' . json_encode($fav) . '\' where id="' . $userid . '"');
                $msg = 'updated';
                return response::json(['msg' => $msg]);
            }
            else if(strtoupper($status) == 'S')
            {
                //$status = '';
                $msg = 'Exist';
                if(count($fav)>0)
                {
                    if (in_array($restid, $fav))
                    {
                        $status = 'Y';
                    } else
                    {
                        $status = 'N';
                    }
                }
                else
                {
                    $status = 'N';
                }
                return response::json(['msg' => $msg,'status' => $status]);

            }
        }
        else
        {
            $msg = "User Doesn't Exist";
            return response::json(['msg' => $msg]);
        }
    }

    public function fav_list($uesrid)
    {
        $detail = DB::SELECT('select count(*) as count,favourites from customer_list where  id="' . $uesrid . '"');
        if (count($detail) > 0)
        {
            if(isset($detail[0]->favourites)) {
                $fav = json_decode($detail[0]->favourites, true);
                $fav = array_filter($fav, 'strlen');
                if (count($fav) > 0) {
                    $count = count($fav);
                    for ($i = 0; $i < count($fav); $i++) {
                        if ($fav[$i] != '') {
                            $detail = DB::SELECT("SELECT id as res_id,p_exclusive,JSON_UNQUOTE(name_tagline->'$.name') as name,JSON_UNQUOTE(name_tagline->'$.tag_line') as tag_line,pure_veg,category,cuisines,min_delivery_time,min_prepration_time,speical_message,expensive_rating,JSON_UNQUOTE(delivery_range_unit->'$.range')  as delivery_range,JSON_UNQUOTE(delivery_range_unit->'$.unit')  as delivery_unit,IFNULL(JSON_UNQUOTE(star_rating->'$.count'),0) as review_count,IFNULL(JSON_UNQUOTE(star_rating->'$.value'),0) as star_vaue,logo,'Busy' as status  FROM `restaurant_master` WHERE  id = '" . $fav[$i] . "'  ORDER BY popular_display_order asc");
                            if (count($detail[0]) > 0) {
                                $restaurantarr[] = $detail[0];
                                $msg = 'Exist';
                                return response::json(['msg' => $msg, 'count' => $count, 'favlist' => $restaurantarr]);
                            }
                        } else {
                            $msg = 'Not Exist';
                            return response::json(['msg' => $msg, 'count' => $count]);
                        }
                    }
                } else {
                    return response::json(['msg' => 'Not Exist']);
                }
            }
            else{
                return response::json(['msg' => 'Not Exist']);
            }
        }
        else
        {
            return response::json(['msg' => 'User Not Exist']);

        }

    }

//Restaurant order edit
    public function rest_order($id,$val)
    {
        Restaurant_Master::where('id',trim($id))->update(['popular_display_order' =>$val]);
        $details = $this->view_restaurant();
        $msg = 'editted';
        return response::json(['msg' => $msg,'details' => $details]);
    }

    //android_version
    public function android_version()
    {
        $setting = GeneralSetting::where('id','1')->select('android_present_version','android_custom_message','force_update')->first();
        if(count($setting)>0)
        {
            if(strtoupper($setting['force_update']) == 'Y')
            {
                $msg = 'Exist';
                return response::json(['msg' => $msg,'version' => $setting['android_present_version'],'message' => $setting['android_custom_message']]);
            }
            else if(strtoupper($setting['force_update']) == 'N')
            {
                $msg = 'no_force_update';
                return response::json(['msg' => $msg,'version' => $setting['android_present_version'],'message' => $setting['android_custom_message']]);
            }
        }
        else
        {
            $msg = 'Not Exist';
            return response::json(['msg' => $msg]);
        }

    }

    //ios_version
    public function ios_version()
    {
        $setting = GeneralSetting::where('id','1')->select('ios_present_version','ios_custom_message','ios_force_update')->first();
        if(count($setting)>0)
        {
            if(strtoupper($setting['ios_force_update']) == 'Y')
            {
                $msg = 'Exist';
                return response::json(['msg' => $msg,'version' => $setting['ios_present_version'],'message' => $setting['ios_custom_message']]);
            }
            else if(strtoupper($setting['ios_force_update']) == 'N')
            {
                $msg = 'no_force_update';
                return response::json(['msg' => $msg,'version' => $setting['ios_present_version'],'message' => $setting['ios_custom_message']]);
            }
        }
        else
        {
            $msg = 'Not Exist';
            return response::json(['msg' => $msg]);
        }
    } //ios_version
    public function ios_version_new()
    {
        $setting = GeneralSetting::where('id','1')->select('ios_present_version','ios_custom_message','ios_force_update')->first();
        if(count($setting)>0)
        {
            if(strtoupper($setting['ios_force_update']) == 'Y')
            {
                $msg = 'Exist';
                return response::json(['msg' => $msg,'version' => $setting['ios_present_version'],'message' => $setting['ios_custom_message']]);
            }
            else if(strtoupper($setting['ios_force_update']) == 'N')
            {
                $msg = 'no_force_update';
                return response::json(['msg' => $msg,'version' => $setting['ios_present_version'],'message' => $setting['ios_custom_message']]);
            }
        }
        else
        {
            $msg = 'Not Exist';
            return response::json(['msg' => $msg]);
        }
    }
    public function login_restaurant($restid)
    {
        $password='';
        $name = '';
        $encr_method = Datasource::encr_method();
        $detail = DB::SELECT("select name,password FROM users where restaurant_id = '$restid'");
        if(count($detail)!=0){
            $rowkey = DB::SELECT("SELECT `explore`,`explore2` FROM `general_settings`");
            $key1 = hash('sha256', $rowkey[0]->explore);
            $iv1 = substr(hash('sha256', $rowkey[0]->explore2), 0, 16);
            $key = hash('sha256', $key1);
            $iv = substr(hash('sha256', $iv1), 0, 16);
            $password = openssl_decrypt(base64_decode($detail[0]->password), $encr_method, $key, 0, $iv);
            $name = $detail[0]->name;
        }
        return ['name'=>$name,'password'=>$password];

    }
    public function update_rest_auth(Request $request) {
        $restname = $request['restname'];
        $restpasw = $request['restpasw'];
        $restid   = $request['restid'];

        $encr_method = Datasource::encr_method();
        $rowkey = DB::SELECT("SELECT `explore`,`explore2` FROM `general_settings`");
        $key1 = hash('sha256', $rowkey[0]->explore);
        $iv1 = substr(hash('sha256', $rowkey[0]->explore2), 0, 16);
        $key = hash('sha256', $key1);
        $iv = substr(hash('sha256', $iv1), 0, 16);
        $password = openssl_encrypt($restpasw, $encr_method, $key, 0, $iv);
        $password = base64_encode($password);
        $timezone = 'ASIA/KOLKATA';
        $date = new DateTime('now', new DateTimeZone($timezone));
        $datetime = $date->format('Y-m-d h:i:s');

        $exist = DB::SELECT("select staffid from users where restaurant_id = '$restid'");
        if(count($exist)>0)
        {
            DB::SELECT("UPDATE users SET name = '$restname',password='$password',updated_at='$datetime' WHERE restaurant_id='$restid' ");
            $msg = 'update';
        }
        else
        {
            DB::INSERT("INSERT INTO `users`(`login_group`, `name`, `password`, `restaurant_id`,`created_at`,modules) VALUES ('V','" . trim($restname) . "','".$password."','".$restid."','".$datetime."',json_object('RestauranrReports','Y','staff','N','banner','N','offers','N','orders','N','reports','N','customer','N','restaurant','N','designation','N','staff_report','N'))");
            $msg = 'insert';
        }
        return $msg;
    }

    public function radius_calculate(Request $request)
    {
        $lat1 = $request['lat1'];
        $lat2 = $request['lat2'];
        $long1 = $request['long1'];
        $long2 = $request['long2'];/*return $lat1.' '.$lat2.' '.$long1.' '.$long2;*/
        $radius = Commonsource::distance_calculate($lat2,$lat1,$long2,$long1);
        return $radius;
    }
}

