<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Review;
use Session;
class ReviewController extends Controller
{
    public function menu_review($id)
    {
		$logingroup = Session::get('logingroup');
		 $staffid = Session::get('staffid');
		 if($logingroup=='V' && $staffid!=$id){
			 return view('404');
		 } else{
        $resid = $id;
        $restaurant_name = DB::SELECT('SELECT name_tagline->>"$.name" as name FROM `restaurant_master` WHERE `id` = "'.$resid.'"' );
        $details = DB::SELECT("SELECT order_number as id,rest_id,rest_details->>'$.name' as restaurant,customer_details->>'$.name' as name,review_star as star,review_details->>'$.review' as review,review_details->>'$.status' as status,review_details->>'$.date' as entry_date FROM order_master WHERE rest_id ='".$resid."' ORDER BY review_details->>'$.date' desc");
        return view('review.review',compact('details','resid','details','restaurant_name'));
		 }
    }
    
    //Filtering of Review
    public function filter_review(Request $request)
    {
        $search = '';
        $flt_from = $request['flt_from'];
        $resid = $request['resid'];
        $flt_to = $request['flt_to'];
        $flt_name = $request['flt_name'];
       
        if((isset($flt_from) && $flt_from != '') && (isset($flt_to) && $flt_to != ''))
        {
            if($search == "")
            {
                $search.="  entry_date BETWEEN '".date('Y-m-d', strtotime(($flt_from)))."' AND '".date('Y-m-d', strtotime(($flt_to)))."'";
            }
            else
            {
                $search.=" and  entry_date  BETWEEN '".date('Y-m-d', strtotime(($flt_from)))."' AND '".date('Y-m-d', strtotime(($flt_to)))."'";
            }
        }
       
        if(isset($flt_name) && $flt_name != '')
        {
            if($search == "")
            {
                  $search.="  LOWER(entry_by->>'$.name')   LIKE '%".strtolower($flt_name)."%'";
            }
            else
            {
                 $search.=" and  LOWER(entry_by->>'$.name')   LIKE '%".strtolower($flt_name)."%'";
            }
        }
       
        if($search!="")
        {
            $search="where $search and ";
        }
        else
        {
            $search ="where ";
        }
        $details = DB::SELECT('SELECT status,entry_by->>"$.name" as name,entry_by->>"$.ids" as ids,id,rest_id,entry_date,review->>"$.star" as star,review->>"$.text" as review FROM `restaurant_reviews` '.$search.' `restaurant_reviews`.`id` != " " and `rest_id` ="'.$resid.'" ORDER BY id');
        return $details;
    }
    
    public function review_status(Request $request)
    {
        $resid = $request['resid'];
        if($request['status'] =='N')
        {
            $status = 'Y';
        }
        else
        {
           $status = 'N'; 
        }
        $update = DB::UPDATE("UPDATE order_master SET review_details=json_set(review_details,'$.status','".$status."') WHERE order_number='".$request['ids']."' ");
        $detail = DB::SELECT("SELECT count(order_number) AS count FROM order_master WHERE rest_id ='".$resid."' and review_details->>'$.status' = 'Y'");
        if(count($detail[0]->count)>0)
        {
            DB::SELECT("update `restaurant_master` set `star_rating` = JSON_SET(`star_rating`,'$.count','" . $detail[0]->count . "') where id='" . $resid . "'");
        }
      $takeavg_rating =  DB::SELECT("SELECT CAST(AVG(`review_star`) AS UNSIGNED) AS avg_rating FROM order_master where `rest_id` = $resid and `current_status` = 'D' AND review_star>0");
      $avg_rating = $takeavg_rating[0]->avg_rating;
      if($avg_rating<3){
          $avg_rating=3;
      }
      DB::UPDATE("UPDATE restaurant_master SET star_rating=json_set(star_rating,'$.value',$avg_rating) WHERE id=$resid");
      return $update;
    }

}
