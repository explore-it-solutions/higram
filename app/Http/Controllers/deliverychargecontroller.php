<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class  deliverychargecontroller extends Controller
{
    public function dc_management(){
	   
	   $filterarr = array();
        $rows=DB::SELECT("SELECT * FROM tbl_delivery_charges WHERE 1");
        return view('delivery_charge.dc_management',compact('rows','filterarr'));
   }
   public function update_delivery_charge(Request $request){
	   $from_range = $request['from_range'];
	   $to_range   = $request['to_range'];
	   $del_charge = $request['del_charge'];
	   $status     = $request['status'];
	   $del_id = $request['del_id'];
	   $operation = $request['operation'];
	   if($operation!='delete'){
	   $condition = "";
	   if($del_id!=''){
		   $condition = " AND id!='$del_id' ";
	   }
	   $is_exist = DB::SELECT("SELECT * FROM tbl_delivery_charges WHERE ('$to_range' BETWEEN from_range AND to_range OR $from_range BETWEEN from_range AND to_range)  $condition ");
	   if(count($is_exist)==0 && $del_id==''){
		   DB::INSERT("INSERT INTO `tbl_delivery_charges`(`from_range`, `to_range`, `charge`) VALUES ('$from_range','$to_range','$del_charge')");
		   $msg="inserted";
	   } else if(count($is_exist)==0 && $del_id!=''){
		   DB::UPDATE("UPDATE `tbl_delivery_charges` SET `from_range`='$from_range', `to_range`='$to_range', `charge`='$del_charge',active='$status' WHERE id= $del_id  ");
		    $msg="updated";
	   } else{
		   $msg="exist"; 
	   }
	   } else{
		   DB::DELETE("DELETE FROM tbl_delivery_charges WHERE id='$del_id' "); $msg="removed";
	   }
	   return $msg;
   }
}
