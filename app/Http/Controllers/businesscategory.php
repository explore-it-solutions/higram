<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Image;

class businesscategory extends Controller
{
   public function view_business_category(){
	   
	   $filterarr = array();
        $rows=DB::SELECT("SELECT id,URL_DECODE(category) as category,disp_order,active,cat_image,pre_scheduled FROM business_category WHERE 1");
        return view('business_cat.business_cat',compact('rows','filterarr'));
   }
   public function update_business_cat(Request $request){
		   $cat_name = urlencode($request['cat_name']);
           $catid    = $request['catid'];
           $status   = $request['status']; 
           $cate_img =$request['cate_img']; 
           $disp_order =$request['disp_order']; 
           $scheduled_status =$request['scheduled_status']; 
		   $chekc_case = "";
		   $pic_url= "";		   
		   if($catid!=''){
			   $chekc_case = " AND id!='$catid' ";
		   }
		   if($cate_img!=''){
			   $uploadfile =$cat_name.'-image-'. date("jmYhis") .'.'.strtolower($cate_img->getClientOriginalExtension());
					Image::make($cate_img)->fit(270,408)->save(base_path() . '/uploads/salescategory/' . $uploadfile);
					$pic_url =  'uploads/salescategory/' . $uploadfile;
		   }
		$is_exist=DB::SELECT("SELECT * FROM business_category WHERE category='$cat_name' $chekc_case ");
		if(count($is_exist)==0){		
				if($catid==''){
					DB::INSERT("INSERT INTO business_category(category,cat_image,pre_scheduled,disp_order) VALUES ('".$cat_name."','".$pic_url."','".$scheduled_status."','".$disp_order."')");
					$msg='success';
				}		
				else if($catid!=''){
					$exist_info=DB::SELECT("SELECT cat_image FROM business_category WHERE id='$catid' ");
					if($exist_info[0]->cat_image!='' && $pic_url!=''){
						 unlink(base_path().'/'.$exist_info[0]->cat_image);
					} else if($pic_url==''){
						$pic_url = $exist_info[0]->cat_image;
					}
					DB::UPDATE("UPDATE business_category SET category='".$cat_name."',active='$status',cat_image='".$pic_url."',pre_scheduled='".$scheduled_status."',disp_order='".$disp_order."' WHERE id='$catid' ");
					$msg='success';
				} 
		} else{
			$msg='exist';
		}
		return $msg;	
   }
   public function sales_category(Request $request){
	$msg='Not Exist';
		$cat_info=DB::SELECT("SELECT URL_DECODE(a.category) as cat_name,a.id as cat_id,a.cat_image,count(b.id) as total_outlets FROM business_category a LEFT JOIN restaurant_master b ON a.id=b.category WHERE  a.active='Y' GROUP BY a.id ORDER BY a.disp_order");
		if(count($cat_info)!=0){
			$msg='Exist';
		}
		return ['msg'=>$msg,'cat_info'=>$cat_info];
   }
}
