<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class StaffReportController extends Controller
{
    public function view_staff_report()
    {
        $staffid = Session::get('staffid');
        $staffs       = DB::SELECT("select concat_WS(' ',`first_name`,`last_name`) as name,`id` from internal_staffs s,internal_staffs_area a WHERE s.`id` = a.staff_id and a.area_id in ( SELECT  a1.area_id from users u1, internal_staffs s1, internal_staffs_area a1 where u1.staffid =s1.id and s1.id = a1.staff_id and a1.staff_id = '$staffid') and `designation`='Delivery staff'");
        $reports_all  = DB::SELECT("SELECT * FROM `report_master` WHERE `category`='staff'"); 
        $g_settings   = DB::SELECT("SELECT `decimal_digit` FROM `general_settings`");
        $num_format   = $g_settings[0]->decimal_digit; 
        $filter_query = array();
        $i=0;
        $all_array = array();
        
        $asnd_stf = DB::SELECT("SELECT i.id,date(om.`order_date`) as date,om.`delivery_assigned_details`->>'$.name' as name,om.`delivery_assigned_to` FROM `order_master` as om LEFT JOIN `internal_staffs` as i ON om.`delivery_assigned_to`= i.id WHERE om.`current_status` NOT IN('T','CA','P') and date(`order_date`) = CURRENT_DATE() group by name,date,om.delivery_assigned_to");
        foreach ($asnd_stf as $value) {
                    $staff_id     = $value->id;
                    if($staff_id!='') {
                        $filter_query[$i] = DB::SELECT("SELECT `order_number`,date(`order_date`) as date,`delivery_assigned_details`->>'$.name' as stf_name,`delivery_assigned_details`->>'$.phone' as stf_phone,`sub_total` as amount,`total_details`->'$.packing_charge' as pck_chrg,`total_details`->'$.delivery_charge' as del_chrg,`final_total`,`status_details`->>'$.OP' as picked,`status_details`->>'$.C' as cnfrm,`status_details`->>'$.D' as delivery,`delivery_assigned_details`->'$.review' as review,`delivery_assigned_details`->'$.star_rate' as rating,`customer_details`->>'$.name' as cus_name,`customer_details`->>'$.mobile' as cus_mobile FROM `order_master` WHERE delivery_assigned_to = '$staff_id' and `current_status` NOT IN('T','CA','P') and date(`order_date`) = CURRENT_DATE()");
                        $i++;
                    }

                    
        }
        
        return view('reports.staff.deliver_staff_report',compact('staffs','reports_all','filter_query','staffs','asnd_stf','num_format'));
    }
    
    
    public function filter_staff_reports(Request $request)
    {
        $staffid   =   $request['staff_id'];
        $reports_name   =   $request['reports_name'];
        $replaced = str_replace(' ', '_', $reports_name);
        $string = "";
        $append = '';
        $g_settings   = DB::SELECT("SELECT `decimal_digit` FROM `general_settings`");
        $num_format   = $g_settings[0]->decimal_digit; 
       
        if($reports_name == 'Delivery_staff_report')
        {
            $staffs         =   $request['staff'];
            $paymode        = $request['payment_mode'];
            if($request['date_from'] != '' && $request['date_to'] != '')
            {
                $from_date      =   date('Y-m-d',  strtotime($request['date_from'])); 
                $to_date        =   date('Y-m-d',  strtotime($request['date_to'])); 

                $string .= " and date(`order_date`) between '$from_date' and '$to_date' ";
            }
            else
            {
                      $string .= " ";
            }
        
        if($staffs == 'all')
        {
            $string .= "";
        }
        else
        {
            $string .= " and delivery_assigned_to = '$staffs'";
        }
            if($paymode !='all')
            {
                $string .=  " and payment_method = '$paymode'";
            }
            if($staffs == 'all')
            {
                $i=0;
                $k=0;
                $staffs = DB::SELECT("SELECT  date(`order_date`) as date FROM order_master o, restaurant_master r where o.`rest_id` = r.id  and r.city in (SELECT  a.area_id from users u, internal_staffs s, internal_staffs_area a where u.staffid =s.id and s.id = a.staff_id and a.staff_id = '$staffid') $string  group by date");
//              $staffs = DB::SELECT("SELECT date(`order_date`) as date FROM `order_master` as om LEFT JOIN `internal_staffs` as i ON om.`delivery_assigned_to`= i.id WHERE om.`current_status` NOT IN('T','CA','P') $string group by date");
                foreach ($staffs as $values)
                {
                    $date = $values->date;
                    $asnd_stf[$k] = DB::SELECT("SELECT i.id,om.`delivery_assigned_details`->>'$.name' as name,date(`order_date`) as date FROM order_master om, restaurant_master r, `internal_staffs`  i where om.`rest_id` = r.id and om.`delivery_assigned_to`= i.id and  r.city in (SELECT  a.area_id from users u, internal_staffs s, internal_staffs_area a where u.staffid =s.id and s.id = a.staff_id and a.staff_id = '$staffid') and om.`current_status` NOT IN('T','CA','P') and  date(`order_date`) = '$date' group by id,name,date");
//                    $asnd_stf[$k] = DB::SELECT("SELECT i.id,om.`delivery_assigned_details`->>'$.name' as name,date(`order_date`) as date FROM `order_master` as om LEFT JOIN `internal_staffs` as i ON om.`delivery_assigned_to`= i.id WHERE om.`current_status` NOT IN('T','CA','P') and date(`order_date`) = '$date' group by id,name,date");
                    foreach ($asnd_stf[$k] as $value)
                    {
                        $staff_id     = $value->id;
                        $date         = $value->date;
                        if($paymode !='all')
                        {
                              $filter_query[$i] = DB::SELECT("SELECT `order_number`,date(`order_date`) as date,`delivery_assigned_details`->>'$.name' as stf_name,`delivery_assigned_details`->>'$.phone' as stf_phone,`sub_total` as amount,JSON_UNQUOTE(`total_details`->'$.packing_charge') as pck_chrg,JSON_UNQUOTE(`total_details`->'$.delivery_charge') as del_chrg,`final_total`,`status_details`->>'$.OP' as picked,`status_details`->>'$.C' as cnfrm,`status_details`->>'$.D' as delivery,`delivery_assigned_details`->'$.review' as review,`delivery_assigned_details`->'$.star_rate' as rating,`customer_details`->>'$.name' as cus_name,`customer_details`->>'$.mobile' as cus_mobile,payment_method,IFNULL(`payment_details`->>'$.razorpay_payment_id',0) as paymentid FROM `order_master`, restaurant_master r WHERE order_master.`rest_id` = r.id and `current_status` NOT IN('T','CA','P') and r.city in (SELECT  a.area_id from users u, internal_staffs s, internal_staffs_area a where u.staffid =s.id and s.id = a.staff_id and a.staff_id = '$staffid') and delivery_assigned_to = '$staff_id' and date(`order_date`) = '$date' and payment_method = '$paymode'");
//                            $filter_query[$i] = DB::SELECT("SELECT `order_number`,date(`order_date`) as date,`delivery_assigned_details`->>'$.name' as stf_name,`delivery_assigned_details`->>'$.phone' as stf_phone,`sub_total` as amount,JSON_UNQUOTE(`total_details`->'$.packing_charge') as pck_chrg,JSON_UNQUOTE(`total_details`->'$.delivery_charge') as del_chrg,`final_total`,`status_details`->>'$.OP' as picked,`status_details`->>'$.C' as cnfrm,`status_details`->>'$.D' as delivery,`delivery_assigned_details`->'$.review' as review,`delivery_assigned_details`->'$.star_rate' as rating,`customer_details`->>'$.name' as cus_name,`customer_details`->>'$.mobile' as cus_mobile,payment_method,IFNULL(`payment_details`->>'$.razorpay_payment_id',0) as paymentid FROM `order_master` WHERE `current_status` NOT IN('T','CA','P') and delivery_assigned_to = '$staff_id' and date(`order_date`) = '$date' and payment_method = '$paymode'");
                        }
                        else
                        {
                            $filter_query[$i] = DB::SELECT("SELECT `order_number`,date(`order_date`) as date,`delivery_assigned_details`->>'$.name' as stf_name,`delivery_assigned_details`->>'$.phone' as stf_phone,`sub_total` as amount,JSON_UNQUOTE(`total_details`->'$.packing_charge') as pck_chrg,JSON_UNQUOTE(`total_details`->'$.delivery_charge') as del_chrg,`final_total`,`status_details`->>'$.OP' as picked,`status_details`->>'$.C' as cnfrm,`status_details`->>'$.D' as delivery,`delivery_assigned_details`->'$.review' as review,`delivery_assigned_details`->'$.star_rate' as rating,`customer_details`->>'$.name' as cus_name,`customer_details`->>'$.mobile' as cus_mobile,payment_method,IFNULL(`payment_details`->>'$.razorpay_payment_id',0) as paymentid FROM `order_master`, restaurant_master r    WHERE  order_master.`rest_id` = r.id and `current_status` NOT IN('T','CA','P') and delivery_assigned_to = '$staff_id' and date(`order_date`) = '$date' and r.city in (SELECT  a.area_id from users u, internal_staffs s, internal_staffs_area a where u.staffid =s.id and s.id = a.staff_id and a.staff_id  = '$staffid')");
//                          $filter_query[$i] = DB::SELECT("SELECT `order_number`,date(`order_date`) as date,`delivery_assigned_details`->>'$.name' as stf_name,`delivery_assigned_details`->>'$.phone' as stf_phone,`sub_total` as amount,JSON_UNQUOTE(`total_details`->'$.packing_charge') as pck_chrg,JSON_UNQUOTE(`total_details`->'$.delivery_charge') as del_chrg,`final_total`,`status_details`->>'$.OP' as picked,`status_details`->>'$.C' as cnfrm,`status_details`->>'$.D' as delivery,`delivery_assigned_details`->'$.review' as review,`delivery_assigned_details`->'$.star_rate' as rating,`customer_details`->>'$.name' as cus_name,`customer_details`->>'$.mobile' as cus_mobile,payment_method,IFNULL(`payment_details`->>'$.razorpay_payment_id',0) as paymentid FROM `order_master` WHERE `current_status` NOT IN('T','CA','P') and delivery_assigned_to = '$staff_id' and date(`order_date`) = '$date'");
                        }
                        $i++;
                    }
                    $k++;
                }
                    
                                $append .=  '<table id="'.$replaced.'" class="table table-striped table-bordered">';
                                $append .=    '<thead>';
                                $append .=    '<tr>';
                                $append .=        '<th style="min-width:40px">Sl No</th>';
                                $append .=        '<th style="min-width:70px">Order No</th>';
                                $append .=        '<th style="min-width:100px">Date</th>';
                                $append .=        '<th style="min-width:100px">Amount</th>';
                                $append .=        '<th style="min-width:100px">Pack Chrg</th>';
                                $append .=        '<th style="min-width:100px">Delv chrg</th>';
                                $append .=        '<th style="min-width:100px">Final Amount</th>';
                                $append .=        '<th style="min-width:100px">Pay Mode</th>';
                                $append .=        '<th style="min-width:100px">Payment Id</th>';
                                $append .=        '<th style="min-width:100px">Conf Time</th>';
                                $append .=        '<th style="min-width:100px">Pick Time</th>';
                                $append .=        '<th style="min-width:100px">Delv Time</th>';
                                $append .=        '<th style="min-width:100px">Staff Rating</th>';
                                $append .=        '<th style="min-width:100px">Staff Review</th>';
                                $append .=        '<th style="min-width:100px">Cust Name</th>';
                                $append .=        '<th style="min-width:100px">Phone</th>';
                                $append .=    '</tr>';
                                $append .=    '</thead>';

                                $append .=    '<tbody>';

                   
                                    $j=0;
                                    $k=0;

                    
                                            $tfoot_amount_total = 0;
                                            $tfoot_pck_chrg_total = 0;
                                            $tfoot_chrg_total = 0;
                                            $tfoot_total = 0;
											//return $staffs;
                        foreach ($staffs as $values)
                        {                 
                                 $append .=            '<tr role="row" class="odd"  style="background-color: beige !important;">';
                                 $append .=                '<td style="min-width:30px;"></td>';
                                 $append .=                '<td style="min-width:30px;"></td>';
                                 $append .=                '<td style="min-width:30px;"><strong style="color:#000">'.$values->date.'<strong></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=            '</tr>';

                 foreach($asnd_stf[$k] as $value)
                 {
                             $tot_cash_tot=0; 
                             $tot_amount_total = 0;
                             $tot_pck_chrg_total = 0;
                             $tot_del_chrg_total = 0;
                             $tot_final_total = 0;

if($from_date== $to_date)
{
    $id_staff = $value->id;
    $orddate  = $value->date;
    $time = DB::SELECT("select DATE_Format(in_time,'%H:%i') as in_time,DATE_Format(out_time,'%H:%i') as out_time from delivery_staff_attendence where staff_id = '$id_staff' and entry_date= '$orddate'");
       $tt ='';
       //return "select DATE_Format(in_time,'%H:%i') as in_time,DATE_Format(out_time,'%H:%i') as out_time from delivery_staff_attendence where staff_id = '$id_staff' and entry_date= '$orddate'" ;
    foreach($time as $timeout)
          {
            $intime = $timeout->in_time;
            $outtime = $timeout->out_time;
            if($outtime == '')
            {
                $outtime == '';
            }
            else
            {
                $outtime == $outtime;
            }
            $tt .= $intime.'-'.$outtime.', ';
                                
          }
          $tt = substr($tt,0,-2);
           $append .=            '<tr role="row" class="odd">';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td>'.$value->name.'</td>';
                                 $append .=                '<td >'.$tt.'</td>';
                                 $append .=                '<td ></td>';
                                 $append .=                '<td ></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=            '</tr>';
}
else
{
                                 $append .=            '<tr role="row" class="odd">';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td>'.$value->name.'</td>';
                                 $append .=                '<td ></td>';
                                 $append .=                '<td ></td>';
                                 $append .=                '<td ></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=            '</tr>';
}

                 $u=0;
                 foreach($filter_query[$j] as $val)
                     {

                                 $u++; 
                                 $tot_amount = $val->amount;
                                 $tot_pck_chrg = $val->pck_chrg;
                                 $tot_del_chrg = $val->del_chrg;
                                 $tot_final = $val->final_total;

                                 $tot_amount_total += $tot_amount;
                                 $tot_pck_chrg_total += $tot_pck_chrg;
                                 $tot_del_chrg_total += $tot_del_chrg;
                                 $tot_final_total += $tot_final;


                                 $append .=            '<tr role="row" class="odd">';
                                 $append .=                '<td style="min-width:30px;">'.$u.'</td>';
                                 $append .=                '<td>'.$val->order_number.'</td>';
                                 $append .=                '<td> </td>';
                                 $append .=                '<td>'.number_format((float)$val->amount, $num_format).'</td>';
                                                 $amt = $val->pck_chrg; 
                                 $append .=                '<td>'.number_format((float)$amt, $num_format).'</td>';
                                                 $fnl = $val->del_chrg;
                                 $append .=                '<td>'.  number_format((float)$fnl, $num_format).'</td>';
                                 $append .=                '<td>'.number_format((float)$val->final_total,$num_format).'</td>';
                                 $append .=                '<td>'.$val->payment_method.'</td>';
                                 $append .=                '<td>'.$val->paymentid.'</td>';
                                 $append .=                '<td>'.$val->cnfrm.'</td>';
                                 $append .=                '<td>'.$val->picked.'</td>';
                                 $append .=                '<td>'.$val->delivery.'</td>';
                                                 $rating = substr($val->rating,0,-1);
                                 $append .=                '<td>'.   substr($rating,1).'</td>';
                                                 $review = substr($val->review,0,-1);
                                 $append .=                '<td>'.  substr($review,1).'</td>';
                                 $append .=                '<td>'.$val->cus_name.'</td>';
                                 $append .=                '<td>'.$val->cus_mobile.'</td>';
                                 $append .=              '</tr>';
                                             }
                                     $j++; 
                                 $append .=              '<tr role="row" class="odd" style="    background-color: #e2e2e2 !important;">';
                                 $append .=                '<td style="min-width:40px;"></td>';
                                 $append .=                '<td style="min-width:70px;"><strong style="color:#000">Total</strong></td>';
                                 $append .=                '<td style="min-width:100px;"></td>';
                                 $append .=                '<td style="font-weight:bold;color:#000">'.number_format((float)$tot_amount_total, $num_format).'</td>';
                                 $append .=                '<td style="font-weight:bold;color:#000">'.number_format((float)$tot_pck_chrg_total, $num_format).'</td>';
                                 $append .=                '<td style="font-weight:bold;color:#000">'.number_format((float)$tot_del_chrg_total, $num_format).'</td>';
                                 $append .=                '<td style="font-weight:bold;color:#000">'.number_format((float)$tot_final_total, $num_format).'</td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=              '</tr>';

                                                 $tfoot_amount_total += $tot_amount_total;
                                                 $tfoot_pck_chrg_total += $tot_pck_chrg_total;
                                                 $tfoot_chrg_total += $tot_del_chrg_total;
                                                 $tfoot_total += $tot_final_total;                             

                                     }
                                 $k++;    
                         }            
                    

                                $append .=    '</tbody>';
                                $append .=    '<tfoot>';
                                $append .=               '<tr role="row" class="odd">';
                                $append .=                '<td style="min-width:30px;"></td>';
                                $append .=                '<td style="min-width:30px;"></td>';
                                $append .=                '<td style="min-width:30px;"></td>';
                                $append .=                '<td style="min-width:100px;font-weight:bold;color:#000">'.$tfoot_amount_total.'</td>';
                                $append .=                '<td style="min-width:100px;font-weight:bold;color:#000">'.$tfoot_pck_chrg_total.'</td>';
                                $append .=                '<td style="min-width:100px;font-weight:bold;color:#000">'.$tfoot_chrg_total.'</td>';
                                $append .=                '<td style="min-width:100px;font-weight:bold;color:#000">'.$tfoot_total.'</td>';
                                $append .=                '<td style="min-width:100px;"></td>';
                                $append .=                '<td style="min-width:100px;"></td>';
                                $append .=                '<td style="min-width:100px;"></td>';
                                $append .=                '<td style="min-width:100px;"></td>';
                                $append .=                '<td style="min-width:100px;"></td>';
                                $append .=                '<td style="min-width:100px;"></td>';
                                $append .=                '<td style="min-width:100px;"></td>';
                                $append .=                '<td style="min-width:100px;"></td>';
                                $append .=                '<td style="min-width:100px;"></td>';
                                $append .=              '</tr>';
                                $append .=  '</tfoot>';                
                                $append .=  '</table>';
                    
                   
                }
                else
                {
//                    $asnd_stf = DB::SELECT("SELECT i.id,date(`order_date`) as date FROM `order_master` as om LEFT JOIN `internal_staffs` as i ON om.`delivery_assigned_to`= i.id WHERE om.`current_status` NOT IN('T','CA','P')  $string group by id,date");
                    $i=0;
//                    foreach ($asnd_stf as $value) 
//                    {
//                        $id = $value->id;
                        $asnd = DB::SELECT("SELECT i.id,date(`order_date`) as date FROM `order_master` as om LEFT JOIN `internal_staffs` as i ON om.`delivery_assigned_to`= i.id WHERE om.`current_status` NOT IN('T','CA','P')  $string group by id,date");
                        $filter_query = DB::SELECT("SELECT `order_number`,date(`order_date`) as date,`delivery_assigned_details`->>'$.name' as stf_name,`delivery_assigned_details`->>'$.phone' as stf_phone,`sub_total` as amount,JSON_UNQUOTE(`total_details`->'$.packing_charge') as pck_chrg,JSON_UNQUOTE(`total_details`->'$.delivery_charge') as del_chrg,`final_total`,`status_details`->>'$.OP' as picked,`status_details`->>'$.C' as cnfrm,`status_details`->>'$.D' as delivery,`delivery_assigned_details`->'$.review' as review,`delivery_assigned_details`->'$.star_rate' as rating,`customer_details`->>'$.name' as cus_name,`customer_details`->>'$.mobile' as cus_mobile,payment_method,IFNULL(`payment_details`->>'$.razorpay_payment_id',0) as paymentid FROM `order_master` WHERE `current_status` NOT IN('T','CA','P') $string Order BY date");
                        $asnd_stf = DB::SELECT("SELECT i.id,date(`order_date`) as date FROM `order_master` as om LEFT JOIN `internal_staffs` as i ON om.`delivery_assigned_to`= i.id WHERE om.`current_status` NOT IN('T','CA','P')  $string group by id,date");
                  foreach ($asnd_stf as $value) 
                    {
                        if($from_date== $to_date)
                     {
                        $id_staff = $value->id;
                        $orddate  = $value->date;
                        $time = DB::SELECT("select DATE_Format(in_time,'%H:%i') as in_time,DATE_Format(out_time,'%H:%i') as out_time from delivery_staff_attendence where staff_id = '$id_staff' and entry_date= '$orddate'");
                        $tt ='';
                        foreach($time as $timeout)
                        {
                            $intime = $timeout->in_time;
                            $outtime = $timeout->out_time;
                            if($outtime == '')
                            {
                                $outtime == '';
                            }
                            else
                            {
                               $outtime == $outtime;
                            }
                            $tt .= $intime.'-'.$outtime.', ';
                                
                        }
                        $tt = substr($tt,0,-2);
                     }
                        $i++;
                    }
                  
                    $append .=  '<table id="'.$replaced.'" class="table table-striped table-bordered">';
                                $append .=    '<thead>';
                                $append .=    '<tr>';
                                $append .=        '<th style="min-width:40px">Sl No</th>';
                                $append .=        '<th style="min-width:70px">Order No</th>';
                                $append .=        '<th style="min-width:100px">Date</th>';
                                $append .=        '<th style="min-width:100px">Amount</th>';
                                $append .=        '<th style="min-width:100px">Pack Chrg</th>';
                                $append .=        '<th style="min-width:100px">Delv chrg</th>';
                                $append .=        '<th style="min-width:100px">Final Amount</th>';
                                $append .=        '<th style="min-width:100px">Pay Mode</th>';
                                $append .=        '<th style="min-width:100px">Payment Id</th>';
                                $append .=        '<th style="min-width:100px">Conf Time</th>';
                                $append .=        '<th style="min-width:100px">Pick Time</th>';
                                $append .=        '<th style="min-width:100px">Delv Time</th>';
                                $append .=        '<th style="min-width:100px">Staff Rating</th>';
                                $append .=        '<th style="min-width:100px">Staff Review</th>';
                                $append .=        '<th style="min-width:100px">Cust Name</th>';
                                $append .=        '<th style="min-width:100px">Phone</th>';
                                $append .=    '</tr>';
                                $append .=    '</thead>';

                                $append .=    '<tbody>';

                   
                                    $j=0;
                              

                    
                                            $tfoot_amount_total = 0;
                                            $tfoot_pck_chrg_total = 0;
                                            $tfoot_chrg_total = 0;
                                            $tfoot_total = 0;
                            

//                            foreach($asnd_stf as $value)
//                            {
//                             $tot_cash_tot=0; 
//                             $tot_amount_total = 0;
//                             $tot_pck_chrg_total = 0;
//                             $tot_del_chrg_total = 0;
//                             $tot_final_total = 0;


                 $u=0;
                        if(count($filter_query) != 0){
                            
                            if($from_date == $to_date)
                            {
                                 $append .=            '<tr role="row" class="odd" style="background-color: beige !important;">';
                                 $append .=                '<td style="min-width:40px;"></td>';
                                 $append .=                '<td style="min-width:70px;"></td>';
                                 $append .=                '<td><strong style="color:#000">'.$filter_query[0]->stf_name.'</strong></td>';
                                 $append .=                '<td style="min-width:30px;">'.$tt.'</td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=            '</tr>';
                            }
                            else{
                            
                                 $append .=            '<tr role="row" class="odd" style="background-color: beige !important;">';
                                 $append .=                '<td style="min-width:40px;"></td>';
                                 $append .=                '<td style="min-width:70px;"></td>';
                                 $append .=                '<td><strong style="color:#000">'.$filter_query[0]->stf_name.'</strong></td>';
                                 $append .=                '<td style="min-width:30px;"></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=                '<td></td>';
                                 $append .=            '</tr>';
                            }
                            
                            
                            
                             foreach($asnd_stf as $value)
                            {
                             $tot_cash_tot=0; 
                             $tot_amount_total = 0;
                             $tot_pck_chrg_total = 0;
                             $tot_del_chrg_total = 0;
                             $tot_final_total = 0;
                            
                            } 
                            
                 foreach($filter_query as $val)
                     {
                            
                                 $u++; 
                                 $tot_amount = $val->amount;
                                 $tot_pck_chrg = $val->pck_chrg;
                                 $tot_del_chrg = $val->del_chrg;
                                 $tot_final = $val->final_total;

                                 $tot_amount_total += $tot_amount;
                                 $tot_pck_chrg_total += $tot_pck_chrg;
                                 $tot_del_chrg_total += $tot_del_chrg;
                                 $tot_final_total += $tot_final;


                                 
                                 $append .=            '<tr role="row" class="odd">';
                                 $append .=                '<td style="min-width:30px;">'.$u.'</td>';
                                 $append .=                '<td>'.$val->order_number.'</td>';
                                 $append .=                '<td>'.$val->date.'</td>';
                                 $append .=                '<td>'.number_format((float)$val->amount, $num_format).'</td>';
                                                 $amt = $val->pck_chrg; 
                                 $append .=                '<td>'.number_format((float)$amt, $num_format).'</td>';
                                                 $fnl = $val->del_chrg;
                                 $append .=                '<td>'.  number_format((float)$fnl, $num_format).'</td>';
                                 $append .=                '<td>'.number_format((float)$val->final_total,$num_format).'</td>';
                                 $append .=                '<td>'.$val->payment_method.'</td>';
                                 $append .=                '<td>'.$val->paymentid.'</td>';
                                 $append .=                '<td>'.$val->cnfrm.'</td>';
                                 $append .=                '<td>'.$val->picked.'</td>';
                                 $append .=                '<td>'.$val->delivery.'</td>';
                                                 $rating = substr($val->rating,0,-1);
                                 $append .=                '<td>'.   substr($rating,1).'</td>';
                                                 $review = substr($val->review,0,-1);
                                 $append .=                '<td>'.  substr($review,1).'</td>';
                                 $append .=                '<td>'.$val->cus_name.'</td>';
                                 $append .=                '<td>'.$val->cus_mobile.'</td>';
                                 $append .=              '</tr>';
                                             }
                                     $j++; 
                               
                                $tfoot_amount_total += $tot_amount_total;
                                $tfoot_pck_chrg_total += $tot_pck_chrg_total;
                                $tfoot_chrg_total += $tot_del_chrg_total;
                                $tfoot_total += $tot_final_total;                             

                            }
                                 
                                    
                    

                                $append .=    '</tbody>';
                                $append .=    '<tfoot>';
                                $append .=               '<tr role="row" class="odd">';
                                $append .=                '<td style="min-width:30px;"></td>';
                                $append .=                '<td style="min-width:30px;"><strong style="color:#000">Total</strong></td>';
                                $append .=                '<td style="min-width:30px;"></td>';
                                $append .=                '<td style="min-width:100px;">'.$tfoot_amount_total.'</td>';
                                $append .=                '<td style="min-width:100px;">'.$tfoot_pck_chrg_total.'</td>';
                                $append .=                '<td style="min-width:100px;">'.$tfoot_chrg_total.'</td>';
                                $append .=                '<td style="min-width:100px;">'.$tfoot_total.'</td>';
                                $append .=                '<td style="min-width:100px;"></td>';
                                $append .=                '<td style="min-width:100px;"></td>';
                                $append .=                '<td style="min-width:100px;"></td>';
                                $append .=                '<td style="min-width:100px;"></td>';
                                $append .=                '<td style="min-width:100px;"></td>';
                                $append .=                '<td style="min-width:100px;"></td>';
                                $append .=                '<td style="min-width:100px;"></td>';
                                $append .=                '<td style="min-width:100px;"></td>';
                                $append .=                '<td style="min-width:100px;"></td>';
                                $append .=              '</tr>';
                                $append .=  '</tfoot>';                
                                $append .=  '</table>';
                    
                }
                
                 return $append;    
                
            }
        else if($reports_name == 'Staff_change_report')
        {
            if($request['date_from'] != '' && $request['date_to'] != '')
            {
                $from_date      =   date('Y-m-d',  strtotime($request['date_from'])); 
                $to_date        =   date('Y-m-d',  strtotime($request['date_to'])); 

                $string .= " and date(`order_date`) between '$from_date' and '$to_date' ";
            }
            else
            {
                      $string .= " ";
            }
                $staffs = DB::SELECT("SELECT `order_number`,json_length(`staff_change_details`) as length, date(`order_date`) as date FROM `order_master`, restaurant_master r WHERE `delivery_staff_change`='Y' $string and order_master.`rest_id` = r.id and r.city in (SELECT  a.area_id from users u, internal_staffs s, internal_staffs_area a where u.staffid =s.id and s.id = a.staff_id and a.staff_id= '$staffid') group by order_number,length,date order by date desc");
//              $staffs = DB::SELECT("SELECT `order_number`,json_length(`staff_change_details`) as length, date(`order_date`) as date FROM `order_master` WHERE `delivery_staff_change`='Y' $string group by order_number,length,date order by date desc");

                $append .=        '<table id="'.$replaced.'" class="table table-striped table-bordered">';
                $append .=        '<thead>';
                $append .=        '<tr style="font-size: 10px !important">';
                $append .=            '<th style="min-width:40px">Sl No</th>';
                $append .=            '<th style="min-width:200px">Order Number</th>';
                $append .=            '<th style="min-width:200px">Date</th>';
                $append .=            '<th style="min-width:140px">Area</th>';
                $append .=            '<th style="min-width:400px">Staff Change Details</th>';
                $append .=        '</tr>';
                $append .=        '</thead>';
                $append .=        '<tbody>';
                $t=0;
                $j=0;
                $p=0;
                foreach ($staffs as $value)
                {
                    $length[$t] = $value->length;
                    $order  = $value->order_number;
                    $k=0;
                    for($i=0;$i<$length[$t];$i++)
                    {
                        $k++;
                        $details[$j] = DB::SELECT("SELECT concat_ws(' ',sf1.`first_name`,sf1.`last_name`) as from_name,concat_ws(' ',sf2.`first_name`,sf2.`last_name`) as to_name,om.`order_number`,date(om.`order_date`)as date,om.`staff_change_details`->>'$.staff$k.time' as time,om.`customer_details`->>'$.addressline2' as addr FROM `order_master` as om LEFT JOIN `internal_staffs` as sf1 ON om.`staff_change_details`->>'$.staff$k.fromstaff' = sf1.id LEFT JOIN `internal_staffs` as sf2 ON om.`staff_change_details`->>'$.staff$k.tostaff' = sf2.id  WHERE `delivery_staff_change`='Y' AND `order_number`= '$order' order by date desc");
//                      $details[$j] = DB::SELECT("SELECT concat_ws(' ',sf1.`first_name`,sf1.`last_name`) as from_name,concat_ws(' ',sf2.`first_name`,sf2.`last_name`) as to_name,om.`order_number`,date(om.`order_date`)as date,om.`staff_change_details`->>'$.staff$k.time' as time,om.`customer_details`->>'$.addressline2' as addr FROM `order_master` as om LEFT JOIN `internal_staffs` as sf1 ON om.`staff_change_details`->>'$.staff$k.fromstaff' = sf1.id LEFT JOIN `internal_staffs` as sf2 ON om.`staff_change_details`->>'$.staff$k.tostaff' = sf2.id  WHERE `delivery_staff_change`='Y' AND `order_number`= '$order' order by date desc");
                        $p++;
                        foreach ($details[$j] as $val)
                        {
                            $append .=                '<tr role="row" class="odd" style="background-color: #fff !important;">';
                            $append .=                    '<td>'.$p.'</td>';
                            $append .=                    '<td>'.$val->order_number.'</td>';
                            $append .=                    '<td>'.$val->date.'</td>';
                            $append .=                    '<td>'.$val->addr.'</td>';
                            $append .=                    '<td>From   '.$val->from_name.'   Changed to   '.$val->to_name.'   at   '.$val->time.'</td>';
                            $append .=                '</tr>';   
                        }
                        $j++;   
                    }
                        $t++;
                }
                    $append .=        '</tbody>';
                    $append .=    '</table>';
                    return $append;
            }    
    }
}
