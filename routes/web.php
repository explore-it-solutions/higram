<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('userlogin.login');
});
Route::get('logout', function () {
    return view('userlogin.login');
});
Route::get('manage_user', function () {
    return view('user.manage_user');
});
Route::get('welcome_restaurant', function () {
    return view('restaurant_login.home');
});
Route::get('restaurant_report', function () {
    return view('restaurant_login.restaurant_report');
});
//Route::get('index', function () {
//    return view('index');
//});

Route::get('userexistcheck','UserLoginController@userexistcheck');
Route::get('session/set', 'UserLoginController@setsession');

Route::group(['middleware' => 'usersession'], function () {
Route::get('index','DashboardController@index_function');
Route::get("manage_designation","DesignationController@view_designation");
Route::get("area","AreaController@view_area");
Route::get("business_category","businesscategory@view_business_category");
Route::get("dc_management","deliverychargecontroller@dc_management");
Route::get("manage_restaurant","RestaurantController@view_restaurant");
Route::get("restaurant_details","RestaurantController@view_restaurantdetails");
Route::get("manage_staff","StaffController@view_staff");
Route::get('groupautosearch', 'RestaurantController@groupautosearch');
Route::get("designation_status","DesignationController@designation_status");//Change Designation Status
Route::get("manage_customer","CustomerController@view_customer");
Route::get("manage_order","OrderController@view_order");
Route::get('restaurant_edit/{id}', 'RestaurantController@restaurant_edit');
Route::get("ratings","RatingController@ratings");
Route::get("staff_permission/{id}","StaffPermissionController@view_staffpermission");
//Menu
Route::get("menu/list/{id}","MenuController@menu_list");
Route::get("menu/add/{id}","MenuController@menu_add");
Route::get("most_selling","MenuController@most_selling");//Change most_selling status
Route::get("menu/review/{id}","ReviewController@menu_review");
Route::get("menu/tax/{id}","TaxController@menu_tax");
Route::get("menu/edit/{rid}/{mid}","MenuController@menu_edit");
Route::get("menu/download/{id}","MenuController@menu_excel_download");
Route::get("tax_status","TaxController@tax_status");
Route::get("review_status","ReviewController@review_status");//Change Review Status
Route::get("restaurant_status","RestaurantController@restaurant_status");//Change Review Status

//Menu Category
Route::get("category/list/{id}","CategoryController@category_list");
Route::get("category_imgview","CategoryController@category_imgview");//Change Category Image View

//Restaurant offer
Route::get("restaurant/offer/{id}","RestaurantOfferController@restaurantoffer");
Route::get('offeritem/search','RestaurantOfferController@offeritem_search');
Route::get("rest_offer_status","RestaurantOfferController@rest_offer_status");//Change Restaurant Offer Status

//Banners
Route::get('manage/banners','BannerController@banners_view');
Route::get('banner/add','BannerController@banners_add');
Route::get('banner/appadd/{id}','BannerController@banners_appadd');


Route::get("general_offers","GeneralOfferController@generaloffer");
Route::get("genoffer_status","GeneralOfferController@genoffer_status");//Change General offer Status

// general reports view
Route::get("general_reports","GeneralReportController@view_general_report");

// staff reports view
Route::get("staff_reports","StaffReportController@view_staff_report");
Route::get("test","PaymentController@test");

Route::get("notification","NotificationController@notification_view");
Route::get("notification_group","NotificationGroupController@notificationgroup_view");

Route::get('orderitem/search','OrderController@orderitem_search');


//EXCEL DOWNLOAD
Route::get("excel_download","CustomerController@excel_download");
//ORDER HISTORY
Route::get('order_history','OrderHistoryController@order_history');
});



Route::group(['middleware' => 'prevent-back-history'],function(){
Auth::routes();
});