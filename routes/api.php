<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
   
});
    Route::group(['middleware' => ['api','cors']], function () {
   });


  //USER LOGIN SECTION
Route::get('emailcheck','UserLoginController@emailcheck');
Route::post('logintest','UserLoginController@logintest');
Route::post('add_area', 'AreaController@add_area');//Add and Edit Designation
Route::post('update_business_cat', 'businesscategory@update_business_cat');
Route::post('update_delivery_charge', 'deliverychargecontroller@update_delivery_charge');
Route::post('add_designation', 'DesignationController@add_designation');//Add and Edit Designation
Route::post('add_staff', 'StaffController@add_staff');//Add and Edit Staff
Route::get('vendor_list_under_type', 'StaffController@vendor_list_under_type');
Route::post('add_restaurant', 'RestaurantController@add_restaurant');//Add Restaurant
Route::post('filter/restaurant', 'RestaurantController@filter_restaurant');//Filter Restaurant
Route::get('vendor_privileges/{vid}', 'RestaurantController@vendor_privileges');
Route::post('update_privilege', 'RestaurantController@update_privilege');
Route::post('filter/menu', 'MenuController@filter_menu');//Filter Restaurant
Route::post('menu/add', 'MenuController@submit_menu');//Add menu
Route::post('menu/edit', 'MenuController@menu_editsubmit');//Add menu
Route::post('category/add', 'MenuController@category_add');//Add menu
Route::post('subcategory/add', 'MenuController@subcategory_add');//Add menu
Route::get('category/{id}', 'MenuController@category');//List actegory per  Restaurant
Route::get('subcategory/{id}', 'MenuController@subcategory');//List actegory per  Restaurant
Route::post('edit_restaurant', 'RestaurantController@edit_restaurant');//Edit Restaurant
Route::post('openclose_time', 'RestaurantController@openclose_time');//Open Close Time Add and update
Route::post('get_taxvalue', 'MenuController@get_taxvalue');//get tax value for particular tax
Route::get('view_time', 'RestaurantController@view_time');//View Open Close Time in Restaurant
Route::get('delete_time', 'RestaurantController@delete_time');//Delete Open Close Time in Restaurant
Route::get('rest_order/{id}/{val}','RestaurantController@rest_order');
Route::get('login_restaurant/{id}','RestaurantController@login_restaurant');
Route::post('update_rest_auth','RestaurantController@update_rest_auth');
Route::post('radius_calculate','RestaurantController@radius_calculate');
Route::post('filter/staff_list', 'StaffController@filter_staff_list');//Filter Staff List
Route::post('staff_area', 'StaffController@staff_area');//Staff Area Add
Route::post('staffarea_list', 'StaffController@staffarea_list');//Staff Area Add
Route::post('staff_area_delete', 'StaffController@staff_area_delete');//Staff Area Add

//Route::get('rest_order/{id}/{val}','RestaurantController@rest_order');
Route::post('add_tax', 'TaxController@add_tax');//Add and Edit Tax
Route::post('menu/upload', 'MenuController@menu_upload');//get tax value for particular tax
Route::post('filter/review', 'ReviewController@filter_review');//Filter Review
Route::post('filter/customer_list', 'CustomerController@filter_customer_list');//Filter Customer List

//Banner add
Route::post('banner/add','BannerController@banner_submit');
Route::post('banner/appadd','BannerController@banner_appsubmit');
Route::get('banner_delete/{id}','BannerController@banner_delete');
Route::get('banner_order/{id}/{val}','BannerController@banner_order');

// General Offers
Route::post('add_gen_offers', 'GeneralOfferController@add_gen_offers');//Add General Offers
Route::post('edit_gen_offers', 'GeneralOfferController@edit_gen_offers');//Edit General Offers
Route::post('filter/genoffer', 'GeneralOfferController@filter_genoffer');//Filter General Offers
Route::post('remove_gen_offer/{offerid}', 'GeneralOfferController@remove_gen_offer');//Filter General Offers
Route::post('apply_gen_offer/{userid}/{couponcode}/{optn}', 'GeneralOfferController@add_coupon_discount');//Filter General Offers

//Restaurant Offer
Route::post('restaurant/add_offer','RestaurantOfferController@restaurant_offer');
Route::post('edit_rest_offers', 'RestaurantOfferController@edit_rest_offers');//Edit General Offers
Route::post('remove_offer/{restid}/{slno}', 'RestaurantOfferController@remove_rest_offers');//Edit General Offers


//Category
Route::get('category_order/{id}/{slno}/{val}','CategoryController@category_order');
Route::get('category_order/{id}/{slno}/{val}','CategoryController@category_order');

Route::get('manage_order_filter_tables','OrderController@manage_order_filter_tables');//Returns the filter data of orders
Route::get('manage_order_filter_div','OrderController@manage_order_filter_div');//Returns the filter data of orders
Route::get('view_order_details_list','OrderController@view_order_details_list');//Returns the data of orders details
Route::get('view_order_address_list','OrderController@view_order_address_list');
Route::get('delete_menuorder','OrderController@delete_menuorder');//Deletes the menuorder
Route::get('assign_staff_list','OrderController@take_assign_staff_list');


//ORDER HISTORY
Route::get('order_history_filter_tables','OrderHistoryController@order_history_filter_tables');//Returns the filter data of orders
Route::get('view_order_history_details','OrderHistoryController@view_order_history_details');
Route::post('check_paymentstatus','OrderController@check_paymentstatus');
//summary of orders in layout page
Route::get('view_order_details_all','DashboardController@total_summary_orders');
//tax aplly to all menu
Route::post('tax_apply_to_all_menu','RestaurantController@tax_apply_to_all_menu');

//General Reports Filter
Route::get('filter_general_reports','GeneralReportController@filter_general_reports');
Route::get('search_item_name','GeneralReportController@orderitem_search');
Route::get('filter_staff_reports','StaffReportController@filter_staff_reports');

//Notification
Route::post('add_notification', 'NotificationGroupController@add_notification');
Route::post('update_notification','NotificationGroupController@update_notification');
Route::post('group_delete','NotificationGroupController@group_delete');
Route::post('customer_list','NotificationGroupController@customer_list');
Route::post('filter/customer', 'NotificationGroupController@filter_customer');
Route::get('restaurantlist','NotificationGroupController@restaurantlist' );

//Manage order
Route::get('manage_order_filter_tables','OrderController@manage_order_filter_tables');//Returns the filter data of orders
Route::get('manage_order_filter_div','OrderController@manage_order_filter_div');//Returns the filter data of orders
Route::get('view_order_details_list','OrderController@view_order_details_list');//Returns the data of orders details
Route::get('addmenuorder','OrderController@addmenuorder');
Route::get('addnewmenuorder','OrderController@addnewmenuorder');
Route::get('edit_order_details','OrderController@edit_order_details');
Route::get('saveedit_order','OrderController@saveedit_order');
Route::get('confirm_order','OrderController@confirm_order');
Route::get('cancel_order','OrderController@cancel_order');//
Route::get('autocomplete_can_reason','OrderController@autocomplete_reason');//
Route::get('savepassword','StaffPermissionController@savepassword');//Save and update password for staff login
Route::get('savepermission','StaffPermissionController@savepermission');
Route::get('get_restraurent_category','TaxController@get_restraurent_category');//Returns categories under restraurent
Route::post('insert_restraurent_category','TaxController@update_restraurent_category_taxes_values');//insert tax to menu

//API FOR FRONT END SERVICES
Route::get('restaurants/{id}/{category}/{search}','RestaurantController@restaurantlists'); //returns restaurants not busy nad open at the time in particular location
Route::get('mobile_reg/{no}','CustomerController@mobile_registration');//mobile number registration and otp send
Route::get('verify_otp/{no}/{otp}','CustomerController@otp_verification');//otp ver
Route::get('verify_otp/profile/{no}/{otp}','CustomerController@profileotp_verification');//otp ver
Route::get('forgot_otp/{no}','CustomerController@forgot_otp');
Route::get('forgot_password/{no}/{password}','CustomerController@forgot_password');
Route::post('customer_reg/{frst}/{lst}/{eml}/{pswd}/{mbl}','CustomerController@customer_registration');
Route::get('customer_login/{phone}/{password}','CustomerController@customer_login');
Route::post('restaurant_offers','RestaurantOfferController@restaurant_offerslist');//returns the restaurant and general offer details
Route::get('bannerapp','BannerController@bannerapplist');//returns list of App Banner
Route::post('bannerapp_new','BannerController@bannerapplist_new');//returns list of App Banner
Route::get('bannerweb','BannerController@bannerweblist'); //returns list of Web Banner
Route::post('bannerweb_new','BannerController@bannerweblist_new'); //returns list of Web Banner
Route::get('popular/restaurants','RestaurantController@mostpopular_restaurants');
Route::get('restaurant/about/{id}','RestaurantController@about_restaurants');//Returns the details of Restaurant
Route::get('restaurant/review/{id}','RestaurantController@review_restaurants');//Returns the review of particular restaurant
Route::post('addresses','CustomerController@addresslist');//Returns the Address of Customer
Route::get('address_add/{userid}/{type}/{line1}/{line2}/{default}/{landmark}/{pincode}','CustomerController@address_add');//Add address of the customer
Route::get('address_edit/{userid}/{addressid}/{type}/{line1}/{line2}/{default}/{landmark}/{pincode}','CustomerController@address_edit');//Edit address of the customer
Route::post('cart_order_web','OrderController@cart_order_web');//Adding menu to cart
Route::post('cart_order/{item_id}/{rate}/{qty}/{prefrnce}/{userid}/{rest_id}/{portion}','OrderController@cart_order');
Route::get('cart_list/{userid}','OrderController@cart_list');//Cart List
Route::post('cart_clear','OrderController@cart_clear');//Clear Cart
Route::post('cart_edit/{userid}/{slno}/{qty}/{prefrnce}','OrderController@cart_edit');//Cart Edit
Route::get('cartitem_delete/{userid}/{slno}','OrderController@cartitem_delete');//Delete Cart Item
Route::get('order_confirmation/{userid}/{type}/{line1}/{line2}/{landmark}/{pincode}/{paymethod}','OrderController@order_confirmation');//Confirm the order
Route::get('menu/{id}/{user_id}','MenuController@menulist');//Returns the menu List
Route::get('cart_total/{userid}','OrderController@cart_total');//Returns the menu count and final total
Route::get('restaurant/web/{id}','RestaurantController@restaurant_web');//Returns the menu List
Route::get('restaurant/category/{id}','RestaurantController@restaurant_category');//Returns the menu List
Route::post('deliverystaff_login','StaffController@deliverystaff_login');//Lists the staff name and number if code matches
Route::get('deliverystaff_details/{staffid}','StaffController@deliverystaff_details');//List the entry details of the staff
Route::post('deliverystaff_addtime/{staffid}','StaffController@deliverystaff_addtime');//Add entry of a particular staff
Route::get('deliverycount_list/{staffid}/{frmdate}/{todate}','StaffController@deliverycount_list');//List the delivery count of particular staff in a date range
Route::get('delivery_orders/{staffid}','StaffController@delivery_orders');//List of delivery orders.
Route::get('delivery_order_details/{order_number}','StaffController@delivery_order_details');//List the delivery order details
Route::post('order_status/{order_number}/{status}','StaffController@order_status');//Change the order status of particular order
Route::get('home_search_list/{search_term}','RestaurantController@search_restaurent_menu');
Route::post('payment_mode','PaymentController@payment_mode');//Returns the payment mode
Route::post('user_orderlist','OrderController@user_orderlist');
Route::post('user_info','OrderController@user_info');
Route::get('order_review/{order_number}','OrderController@order_review');
Route::get('order_review_add/{order_number}/{star_rating}/{review}/{staffrate}/{staffreview}','MailerController@order_review_add');
Route::get('user_orderdetails/{order_number}','OrderController@user_orderdetails');
Route::get('home_orderstatus/{userid}','OrderController@home_orderstatus');
Route::get('sendotp/profile/{mob}/{oldno}','CustomerController@sendotp');
Route::post('updateprofile','CustomerController@updateprofile');
Route::get('repeat_order/{userid}/{order_number}','OrderController@repeat_order');//To repeat the order
Route::get('new_order_check/{staffid}','StaffController@new_order_check');
Route::get('favourite_restaurant/update/{userid}/{restid}/{status}','RestaurantController@favourite_update');
Route::get('new_order_status/{staffid}','StaffController@new_order_status');
Route::get('fav_list/{userid}','RestaurantController@fav_list');
Route::get('android_version','RestaurantController@android_version');
Route::get('ios_version','RestaurantController@ios_version');
Route::post('user_details','CustomerController@user_details');
Route::get('address_remove/{userid}/{id}','CustomerController@address_remove');
Route::get('payment/initialize/{id}','PaymentController@payment_initialize');
Route::post('payment/complete/{id}/{refid}','PaymentController@payment_complete');
Route::post('payment/order','PaymentController@create_order');
Route::post('payment/order/new','PaymentController@create_order_new');
Route::post('payment/orderconfirmation_new','PaymentController@orderconfirmation_new');
Route::post('delivery_range_check','StaffController@delivery_range_check');
Route::get('default_location/{id}','CustomerController@default_location');
Route::get('minimum_cartvalue_check/{userid}','StaffController@minimumcartcheck');
Route::get('test','StaffController@testing');
Route::post('ftoken_check','CustomerController@ftoken_check');
Route::post('send_notification','CustomerController@notification_send');
Route::post('order_notification','OrderController@order_notification');
Route::post('ftoken_staff_check','StaffController@ftoken_staff_check');
Route::post('notificationsubmit','StaffController@notificationsubmit');
Route::post('notification_check','CustomerController@notification_check');
Route::post('filter/notification_list','CustomerController@notification_list');
Route::post('ftoken_delete','CustomerController@ftoken_delete');
Route::get('notification_delete/{id}','NotificationController@notification_delete');


//API UPDATED
Route::post('mobile_reg/new','CustomerController@mobile_registration_new');//mobile number registration and otp send
Route::post('custid_updation_test','CustomerController@custid_updation_test');//mobile number registration and otp send
Route::post('verify_otp/new','CustomerController@otp_verification_new');//otp ver
Route::post('customer_reg_new','CustomerController@customer_registration_new');
//Route::post('customer_reg_new','CustomerController@customer_registration_new');
Route::post('customer_login_new','CustomerController@customer_login_new');
Route::post('forgot_otp_new','CustomerController@forgot_otp_new');
Route::post('force_psw_update','CustomerController@force_psw_update');
Route::post('forgot_password_new','CustomerController@forgot_password_new');
Route::post('home_search_list_new','RestaurantController@search_restaurent_menu_new');
Route::post('home_search_list_new_location','RestaurantController@search_restaurent_menu_new_location');
Route::post('restaurants_new','RestaurantController@restaurantlists_new'); //returns restaurants not busy nad open at the time in particular location
Route::post('cart_order_new','OrderController@cart_order_new');
Route::post('cart_edit_new','OrderController@cart_edit_new');//Cart Edit
Route::post('cartitem_delete_new','OrderController@cartitem_delete_new');//Delete Cart Item
Route::post('address_add_new','CustomerController@address_add_new');//Add address of the customer
Route::post('menu_new','MenuController@menulist_new');//Returns the menu List
Route::post('order_review_add_new','MailerController@order_review_add_new');
Route::post('address_edit_new','CustomerController@address_edit_new');//Edit address of the customer
Route::post('address_remove_new','CustomerController@address_remove_new');
Route::post('ios_version_new','RestaurantController@ios_version_new');
Route::post('sendotp/profile_new','CustomerController@sendotp_new');
Route::post('verify_otp/profile_new','CustomerController@profileotp_verification_new');//otp ver
Route::post('apply_gen_offer_new', 'GeneralOfferController@add_coupon_discount_new');//Filter General Offers

//NEW API
Route::get('sales_category','businesscategory@sales_category');
